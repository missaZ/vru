﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDuplicate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PER_FAMNO = New System.Windows.Forms.TextBox()
        Me.PER_DOB = New System.Windows.Forms.TextBox()
        Me.PER_GRAND = New System.Windows.Forms.TextBox()
        Me.PER_FATHER = New System.Windows.Forms.TextBox()
        Me.PER_FIRST = New System.Windows.Forms.TextBox()
        Me.gvDuplicates = New System.Windows.Forms.DataGridView()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnCorrect = New System.Windows.Forms.Button()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.VRC_NAME_AR = New System.Windows.Forms.TextBox()
        Me.VRC_OID = New System.Windows.Forms.TextBox()
        Me.GOV_NAME_AR = New System.Windows.Forms.TextBox()
        Me.GOV_MOT_ID = New System.Windows.Forms.TextBox()
        Me.PC_NAME = New System.Windows.Forms.TextBox()
        Me.PC_ID = New System.Windows.Forms.TextBox()
        Me.PER_PUID = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox25 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnInsert = New System.Windows.Forms.Button()
        CType(Me.gvDuplicates, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PER_FAMNO
        '
        Me.PER_FAMNO.Enabled = False
        Me.PER_FAMNO.Font = New System.Drawing.Font("Times New Roman", 20.25!)
        Me.PER_FAMNO.Location = New System.Drawing.Point(10, 169)
        Me.PER_FAMNO.Multiline = True
        Me.PER_FAMNO.Name = "PER_FAMNO"
        Me.PER_FAMNO.ReadOnly = True
        Me.PER_FAMNO.Size = New System.Drawing.Size(279, 34)
        Me.PER_FAMNO.TabIndex = 100008
        Me.PER_FAMNO.Tag = "addition"
        '
        'PER_DOB
        '
        Me.PER_DOB.Enabled = False
        Me.PER_DOB.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOB.ForeColor = System.Drawing.Color.Black
        Me.PER_DOB.Location = New System.Drawing.Point(13, 252)
        Me.PER_DOB.Multiline = True
        Me.PER_DOB.Name = "PER_DOB"
        Me.PER_DOB.ReadOnly = True
        Me.PER_DOB.Size = New System.Drawing.Size(276, 34)
        Me.PER_DOB.TabIndex = 100020
        Me.PER_DOB.Tag = "addition"
        '
        'PER_GRAND
        '
        Me.PER_GRAND.Enabled = False
        Me.PER_GRAND.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_GRAND.ForeColor = System.Drawing.Color.Black
        Me.PER_GRAND.Location = New System.Drawing.Point(13, 370)
        Me.PER_GRAND.Multiline = True
        Me.PER_GRAND.Name = "PER_GRAND"
        Me.PER_GRAND.ReadOnly = True
        Me.PER_GRAND.Size = New System.Drawing.Size(499, 35)
        Me.PER_GRAND.TabIndex = 100013
        Me.PER_GRAND.Tag = "addition"
        '
        'PER_FATHER
        '
        Me.PER_FATHER.Enabled = False
        Me.PER_FATHER.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_FATHER.ForeColor = System.Drawing.Color.Black
        Me.PER_FATHER.Location = New System.Drawing.Point(13, 332)
        Me.PER_FATHER.Multiline = True
        Me.PER_FATHER.Name = "PER_FATHER"
        Me.PER_FATHER.ReadOnly = True
        Me.PER_FATHER.Size = New System.Drawing.Size(499, 32)
        Me.PER_FATHER.TabIndex = 100012
        Me.PER_FATHER.Tag = "addition"
        '
        'PER_FIRST
        '
        Me.PER_FIRST.Enabled = False
        Me.PER_FIRST.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_FIRST.ForeColor = System.Drawing.Color.Black
        Me.PER_FIRST.Location = New System.Drawing.Point(13, 292)
        Me.PER_FIRST.Multiline = True
        Me.PER_FIRST.Name = "PER_FIRST"
        Me.PER_FIRST.ReadOnly = True
        Me.PER_FIRST.Size = New System.Drawing.Size(499, 34)
        Me.PER_FIRST.TabIndex = 100011
        Me.PER_FIRST.Tag = "addition"
        '
        'gvDuplicates
        '
        Me.gvDuplicates.AllowUserToAddRows = False
        Me.gvDuplicates.AllowUserToDeleteRows = False
        Me.gvDuplicates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvDuplicates.Location = New System.Drawing.Point(10, 414)
        Me.gvDuplicates.MultiSelect = False
        Me.gvDuplicates.Name = "gvDuplicates"
        Me.gvDuplicates.ReadOnly = True
        Me.gvDuplicates.Size = New System.Drawing.Size(923, 151)
        Me.gvDuplicates.TabIndex = 100043
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(603, 571)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(106, 37)
        Me.btnCancel.TabIndex = 100046
        Me.btnCancel.Text = "الغاء التحديث"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnCorrect
        '
        Me.btnCorrect.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCorrect.Location = New System.Drawing.Point(715, 571)
        Me.btnCorrect.Name = "btnCorrect"
        Me.btnCorrect.Size = New System.Drawing.Size(106, 37)
        Me.btnCorrect.TabIndex = 100047
        Me.btnCorrect.Text = "تصحيح"
        Me.btnCorrect.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBack.Location = New System.Drawing.Point(10, 571)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(106, 37)
        Me.btnBack.TabIndex = 100048
        Me.btnBack.Text = "رجوع"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(747, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(190, 22)
        Me.Label1.TabIndex = 100049
        Me.Label1.Text = "تم العثور على بيانات متشابهة"
        '
        'VRC_NAME_AR
        '
        Me.VRC_NAME_AR.Enabled = False
        Me.VRC_NAME_AR.Font = New System.Drawing.Font("Times New Roman", 20.25!)
        Me.VRC_NAME_AR.Location = New System.Drawing.Point(10, 90)
        Me.VRC_NAME_AR.Multiline = True
        Me.VRC_NAME_AR.Name = "VRC_NAME_AR"
        Me.VRC_NAME_AR.ReadOnly = True
        Me.VRC_NAME_AR.Size = New System.Drawing.Size(279, 34)
        Me.VRC_NAME_AR.TabIndex = 100052
        Me.VRC_NAME_AR.Tag = "addition"
        '
        'VRC_OID
        '
        Me.VRC_OID.Enabled = False
        Me.VRC_OID.Font = New System.Drawing.Font("Times New Roman", 20.25!)
        Me.VRC_OID.Location = New System.Drawing.Point(10, 40)
        Me.VRC_OID.Multiline = True
        Me.VRC_OID.Name = "VRC_OID"
        Me.VRC_OID.ReadOnly = True
        Me.VRC_OID.Size = New System.Drawing.Size(279, 34)
        Me.VRC_OID.TabIndex = 100050
        Me.VRC_OID.Tag = "addition"
        '
        'GOV_NAME_AR
        '
        Me.GOV_NAME_AR.BackColor = System.Drawing.SystemColors.Control
        Me.GOV_NAME_AR.Enabled = False
        Me.GOV_NAME_AR.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GOV_NAME_AR.ForeColor = System.Drawing.Color.Black
        Me.GOV_NAME_AR.Location = New System.Drawing.Point(518, 90)
        Me.GOV_NAME_AR.Multiline = True
        Me.GOV_NAME_AR.Name = "GOV_NAME_AR"
        Me.GOV_NAME_AR.ReadOnly = True
        Me.GOV_NAME_AR.Size = New System.Drawing.Size(273, 34)
        Me.GOV_NAME_AR.TabIndex = 100051
        Me.GOV_NAME_AR.Tag = "const"
        '
        'GOV_MOT_ID
        '
        Me.GOV_MOT_ID.BackColor = System.Drawing.SystemColors.Control
        Me.GOV_MOT_ID.Enabled = False
        Me.GOV_MOT_ID.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GOV_MOT_ID.ForeColor = System.Drawing.Color.Black
        Me.GOV_MOT_ID.Location = New System.Drawing.Point(518, 40)
        Me.GOV_MOT_ID.Multiline = True
        Me.GOV_MOT_ID.Name = "GOV_MOT_ID"
        Me.GOV_MOT_ID.ReadOnly = True
        Me.GOV_MOT_ID.Size = New System.Drawing.Size(273, 34)
        Me.GOV_MOT_ID.TabIndex = 100055
        Me.GOV_MOT_ID.Tag = "const"
        '
        'PC_NAME
        '
        Me.PC_NAME.Enabled = False
        Me.PC_NAME.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PC_NAME.ForeColor = System.Drawing.Color.Black
        Me.PC_NAME.Location = New System.Drawing.Point(518, 169)
        Me.PC_NAME.Multiline = True
        Me.PC_NAME.Name = "PC_NAME"
        Me.PC_NAME.ReadOnly = True
        Me.PC_NAME.Size = New System.Drawing.Size(273, 34)
        Me.PC_NAME.TabIndex = 100057
        Me.PC_NAME.Tag = "addition"
        '
        'PC_ID
        '
        Me.PC_ID.Enabled = False
        Me.PC_ID.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PC_ID.ForeColor = System.Drawing.Color.Black
        Me.PC_ID.Location = New System.Drawing.Point(518, 130)
        Me.PC_ID.Multiline = True
        Me.PC_ID.Name = "PC_ID"
        Me.PC_ID.ReadOnly = True
        Me.PC_ID.Size = New System.Drawing.Size(273, 34)
        Me.PC_ID.TabIndex = 100056
        Me.PC_ID.Tag = "addition"
        '
        'PER_PUID
        '
        Me.PER_PUID.Enabled = False
        Me.PER_PUID.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_PUID.ForeColor = System.Drawing.Color.Black
        Me.PER_PUID.Location = New System.Drawing.Point(518, 252)
        Me.PER_PUID.Multiline = True
        Me.PER_PUID.Name = "PER_PUID"
        Me.PER_PUID.ReadOnly = True
        Me.PER_PUID.Size = New System.Drawing.Size(224, 34)
        Me.PER_PUID.TabIndex = 100058
        Me.PER_PUID.Tag = "addition"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.VRU.My.Resources.Resources.s1_p4_label21
        Me.PictureBox1.Location = New System.Drawing.Point(806, 128)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(127, 75)
        Me.PictureBox1.TabIndex = 100060
        Me.PictureBox1.TabStop = False
        '
        'PictureBox25
        '
        Me.PictureBox25.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox25.Image = Global.VRU.My.Resources.Resources.s1_p3_label3
        Me.PictureBox25.Location = New System.Drawing.Point(748, 212)
        Me.PictureBox25.Name = "PictureBox25"
        Me.PictureBox25.Size = New System.Drawing.Size(185, 74)
        Me.PictureBox25.TabIndex = 100059
        Me.PictureBox25.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.Image = Global.VRU.My.Resources.Resources.s1_p1_label2
        Me.PictureBox6.Location = New System.Drawing.Point(308, 40)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(196, 84)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox6.TabIndex = 100054
        Me.PictureBox6.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.Image = Global.VRU.My.Resources.Resources.s1_p1_label
        Me.PictureBox3.Location = New System.Drawing.Point(806, 40)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(127, 84)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox3.TabIndex = 100053
        Me.PictureBox3.TabStop = False
        '
        'PictureBox20
        '
        Me.PictureBox20.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox20.Image = Global.VRU.My.Resources.Resources.s1_p3_label21
        Me.PictureBox20.Location = New System.Drawing.Point(308, 212)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(196, 74)
        Me.PictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox20.TabIndex = 100034
        Me.PictureBox20.TabStop = False
        '
        'PictureBox10
        '
        Me.PictureBox10.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox10.Image = Global.VRU.My.Resources.Resources.s1_p2_right_line4_text1
        Me.PictureBox10.Location = New System.Drawing.Point(518, 370)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(415, 35)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox10.TabIndex = 100045
        Me.PictureBox10.TabStop = False
        '
        'PictureBox16
        '
        Me.PictureBox16.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox16.Image = Global.VRU.My.Resources.Resources.s1_p2_left_line1_label11
        Me.PictureBox16.Location = New System.Drawing.Point(308, 130)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(196, 73)
        Me.PictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox16.TabIndex = 100030
        Me.PictureBox16.TabStop = False
        '
        'PictureBox12
        '
        Me.PictureBox12.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox12.Image = Global.VRU.My.Resources.Resources.s1_p2_right_line3_text1
        Me.PictureBox12.Location = New System.Drawing.Point(518, 332)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(415, 32)
        Me.PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox12.TabIndex = 100027
        Me.PictureBox12.TabStop = False
        '
        'PictureBox11
        '
        Me.PictureBox11.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox11.Image = Global.VRU.My.Resources.Resources.s1_p2_right_line2_text1
        Me.PictureBox11.Location = New System.Drawing.Point(518, 292)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(415, 34)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox11.TabIndex = 100025
        Me.PictureBox11.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(13, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 100061
        Me.Label2.Text = "الاسباب"
        '
        'btnInsert
        '
        Me.btnInsert.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInsert.Location = New System.Drawing.Point(827, 571)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(106, 37)
        Me.btnInsert.TabIndex = 100062
        Me.btnInsert.Text = "اضافة"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'frmDuplicate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.LightGray
        Me.ClientSize = New System.Drawing.Size(942, 615)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PER_PUID)
        Me.Controls.Add(Me.PictureBox25)
        Me.Controls.Add(Me.PC_NAME)
        Me.Controls.Add(Me.PC_ID)
        Me.Controls.Add(Me.VRC_NAME_AR)
        Me.Controls.Add(Me.VRC_OID)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.GOV_NAME_AR)
        Me.Controls.Add(Me.GOV_MOT_ID)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.btnCorrect)
        Me.Controls.Add(Me.PictureBox20)
        Me.Controls.Add(Me.PER_DOB)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.PictureBox10)
        Me.Controls.Add(Me.gvDuplicates)
        Me.Controls.Add(Me.PER_FAMNO)
        Me.Controls.Add(Me.PictureBox16)
        Me.Controls.Add(Me.PER_GRAND)
        Me.Controls.Add(Me.PER_FATHER)
        Me.Controls.Add(Me.PER_FIRST)
        Me.Controls.Add(Me.PictureBox12)
        Me.Controls.Add(Me.PictureBox11)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(958, 653)
        Me.MinimumSize = New System.Drawing.Size(958, 653)
        Me.Name = "frmDuplicate"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ظهور مكرر"
        CType(Me.gvDuplicates, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PER_FAMNO As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOB As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox20 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox16 As System.Windows.Forms.PictureBox
    Friend WithEvents PER_GRAND As System.Windows.Forms.TextBox
    Friend WithEvents PER_FATHER As System.Windows.Forms.TextBox
    Friend WithEvents PER_FIRST As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents gvDuplicates As System.Windows.Forms.DataGridView
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnCorrect As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents VRC_NAME_AR As System.Windows.Forms.TextBox
    Friend WithEvents VRC_OID As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents GOV_NAME_AR As System.Windows.Forms.TextBox
    Friend WithEvents GOV_MOT_ID As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PC_NAME As System.Windows.Forms.TextBox
    Friend WithEvents PC_ID As System.Windows.Forms.TextBox
    Friend WithEvents PER_PUID As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox25 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnInsert As System.Windows.Forms.Button
End Class
