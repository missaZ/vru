﻿Imports iTextSharp.text.pdf
Imports System.IO

Module modHelper
    Public VRC As String
    Public VRCNAME As String
    Public userId As Integer

    Function PDFcreator(PerId As Integer) As String
        Try
            Dim pdfn As String
            Dim Voterid2 As String
            Dim lockpdf As Boolean = False
            Dim sql As String = ""
            Dim sqlHelp As New DataAccessLayer
            Dim per As New person
            Dim finder As New PersonDAL

            per.PER_ID = PerId
            finder = New PersonDAL
            finder.GetPerson(per)

            Voterid2 = ""

            pdfn = finder.person("PER_ID")
            pdfn = finder.person("PER_FIRST").ToString() + "_" + pdfn '.Substring(pdfn.Length - 4)
            Dim pdfTemplate As String = Directory.GetCurrentDirectory() & "\Files\Form222_final.pdf"
            Dim newFile As String = Directory.GetCurrentDirectory() & "\Files\comp_" + pdfn + "_" + String.Format("{0:d_MM_yyyy}", DateTime.Now) + ".pdf"
            Dim pdfReader As PdfReader = New PdfReader(pdfTemplate)
            Dim pdfStamper As PdfStamper = New PdfStamper(pdfReader, New FileStream(newFile, FileMode.Create))
            Dim pdfFormFields As AcroFields = pdfStamper.AcroFields

            If (finder.person("PER_ID").ToString() <> "") Then

                pdfFormFields.SetField("Voter_ID", finder.person("PER_ID").ToString())
                If (finder.person("PER_ID").ToString() <> "") Then
                    Voterid2 = "*" + finder.person("PER_ID").ToString() + "*"
                End If
            End If

            pdfFormFields.SetField("barc1", Voterid2)
            pdfFormFields.SetField("Voter_idN", Voterid2)
            pdfFormFields.SetField("Gov_id", finder.person("GOV_MOT_ID").ToString())
            pdfFormFields.SetField("Gov_name", finder.person("GOV_NAME_AR").ToString())
            pdfFormFields.SetField("VRC", finder.person("VRC_OID").ToString())
            pdfFormFields.SetField("VRC_Name", finder.person("VRC_NAME_AR").ToString())

            pdfFormFields.SetField("Fam_ID", finder.person("PER_FAMNO").ToString())

            pdfFormFields.SetField("FirstN", finder.person("PER_FIRST").ToString())
            pdfFormFields.SetField("FatherName", finder.person("PER_FATHER").ToString())
            pdfFormFields.SetField("GrandName", finder.person("PER_GRAND").ToString())

            Dim names() As String
            ReDim names(2)
            names(0) = ""
            names(1) = ""
            names(2) = ""

            Dim full As String = finder.person("PER_FHFULLNAME").ToString
            names = full.Split(" ")
            ReDim Preserve names(2)
            pdfFormFields.SetField("FamFirst", names(0))
            pdfFormFields.SetField("FamFather", names(1))
            pdfFormFields.SetField("FamGrand", names(2))

            pdfFormFields.SetField("Gender", IIf(finder.person("PER_GENDER") = 1, "M", "F"))


            If finder.person("PER_DOB").ToString() <> "" Then
                Dim myDate As Date
                myDate = Date.Parse(finder.person("PER_DOB"))
                pdfFormFields.SetField("Day", myDate.Day)
                pdfFormFields.SetField("Month", myDate.Month)
                pdfFormFields.SetField("Year", myDate.Year)
            End If



            pdfFormFields.SetField("PU", IIf(finder.person("PER_PUID") = 0, "", finder.person("PER_PUID")))

            pdfFormFields.SetField("PCID", finder.person("PC_ID").ToString())
            pdfFormFields.SetField("PC_Name", finder.person("PC_NAME").ToString())

            pdfFormFields.SetField("DocT1", finder.person("PER_DOCUMENTTYPE1").ToString())
            pdfFormFields.SetField("Doc1No", finder.person("PER_DOCUMENTNO1").ToString())
            pdfFormFields.SetField("Doc1issu", finder.person("PER_DOCUMENTISSUEPLACE1").ToString())
            pdfFormFields.SetField("Doc1date", finder.person("PER_DOCUMENTISSUEDATE1").ToString())

            pdfFormFields.SetField("DocT2", finder.person("PER_DOCUMENTTYPE2").ToString())
            pdfFormFields.SetField("Doc2No", finder.person("PER_DOCUMENTNO2").ToString())
            pdfFormFields.SetField("Doc2issu", finder.person("PER_DOCUMENTISSUEPLACE2").ToString())
            pdfFormFields.SetField("Doc2date", finder.person("PER_DOCUMENTISSUEDATE2").ToString())

            pdfFormFields.SetField("DocT3", finder.person("PER_DELETEDOCUMENTTYPE").ToString())
            pdfFormFields.SetField("Doc3No", finder.person("PER_DELETEDOCUMENTNO").ToString())
            pdfFormFields.SetField("Doc3issu", finder.person("PER_DELETEDOCUMENTISSUEPALCE").ToString())
            pdfFormFields.SetField("Doc3date", finder.person("PER_DELETEDOCUMENTISSUEDATE").ToString())
            pdfFormFields.SetFieldProperty("DocT3", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("Doc3No", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("Doc3issu", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("Doc3date", "flags", PdfFormField.FLAGS_PRINT, Nothing)

            pdfFormFields.SetField("DocT4", finder.person("PER_IDPDOCUMENTTYPE").ToString())
            pdfFormFields.SetField("Doc4No", IIf(finder.person("PER_IDPDOCUMENTNO") = "0", "", finder.person("PER_IDPDOCUMENTNO")))
            pdfFormFields.SetField("Doc4issu", finder.person("PER_IDPDOCUMENTISSUEPLACE").ToString())
            pdfFormFields.SetField("Doc4date", finder.person("PER_IDPDOCUMENTISSUEDATE").ToString())
            pdfFormFields.SetFieldProperty("DocT4", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("Doc4No", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("Doc4issu", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("Doc4date", "flags", PdfFormField.FLAGS_PRINT, Nothing)


            pdfFormFields.SetField("vrc_OID", IIf(finder.person("PER_OLDVRC") = 0, "", finder.person("PER_OLDVRC")))
            pdfFormFields.SetField("vrc_OID2", IIf(finder.person("PER_OLDFRC") = 0, "", finder.person("PER_OLDFRC")))
            pdfFormFields.SetFieldProperty("vrc_OID", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("vrc_OID2", "flags", PdfFormField.FLAGS_PRINT, Nothing)

            pdfFormFields.SetField("OGOVIDP", finder.person("PER_ORGGOVNAME"))
            pdfFormFields.SetField("IDPNO", IIf(finder.person("PER_ORGGOVID") = 0, "", finder.person("PER_ORGGOVID")))

            If finder.person("PER_IDPDATE") <> "" Then
                Dim myDate As Date
                myDate = Date.Parse(finder.person("PER_IDPDATE"))
                pdfFormFields.SetField("IDPDay", myDate.Day)
                pdfFormFields.SetField("IDPMonth", myDate.Month)
                pdfFormFields.SetField("IDPYear", myDate.Year)
            End If



            pdfFormFields.SetField("IDPVRC", IIf(finder.person("PER_IDPVRCID") = 0, "", finder.person("PER_IDPVRCID")))
            pdfFormFields.SetField("IDPvrc_OID", IIf(finder.person("PER_IDPFRCID") = 0, "", finder.person("PER_IDPFRCID")))
            pdfFormFields.SetFieldProperty("OGOVIDP", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("IDPNO", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("IDPDay", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("IDPMonth", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("IDPYear", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("IDPVRC", "flags", PdfFormField.FLAGS_PRINT, Nothing)
            pdfFormFields.SetFieldProperty("IDPvrc_OID", "flags", PdfFormField.FLAGS_PRINT, Nothing)

            If finder.person("CHC") Then
                pdfFormFields.SetField("COR", "COR")
                pdfFormFields.SetField("CHG", "CHG")
            Else
                pdfFormFields.SetField("COR", IIf(finder.person("COR"), "COR", ""))
                pdfFormFields.SetField("CHG", IIf(finder.person("CHG"), "CHG", ""))
            End If
            pdfFormFields.SetField("ADD", IIf(finder.person("ADD"), "ADD", ""))
            pdfFormFields.SetField("DEL", IIf(finder.person("DEL"), "DEL", ""))
            pdfFormFields.SetField("IDP", IIf(finder.person("IDP"), "IDP", ""))

            pdfStamper.FormFlattening = lockpdf

            If finder.person("PER_IMAGE") IsNot Nothing Then
                pdfFormFields.SetField("img", finder.person("PER_IMAGE").ToString)
                Dim chartImg As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(finder.person("PER_IMAGE"))
                chartImg.ScalePercent(55)
                chartImg.Border = 1
                Dim underContent As iTextSharp.text.pdf.PdfContentByte
                chartImg.SetAbsolutePosition(20, 755)
                underContent = pdfStamper.GetOverContent(1)
                underContent.AddImage(chartImg)
            End If

            'Dim gB As New CheckBox
            'gB = Application.OpenForms("frmSearch").Controls("chkLock")

            'If (gB.Checked) Then
            '    lockpdf = True
            'End If

            'pdfStamper.FormFlattening = lockpdf
            pdfStamper.Close()

            Return newFile
        Catch ex As Exception
            MsgBox(ex.Message)
            Return ""
        End Try
    End Function
End Module
