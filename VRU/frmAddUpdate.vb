﻿Imports System.Configuration
Imports System.IO

Public Class frmAddUpdate
    Public finder As PersonDAL
    Private fileName As String
    Public photoChanged As Boolean = False
    Public myPER_ID As Integer = 0
    Public addPerson As Boolean = False

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Try
            Dim newFile As String
            If myPER_ID = 0 Then
                myPER_ID = Integer.Parse(PER_ID.Text)
            End If

            newFile = PDFcreator(Integer.Parse(myPER_ID))
            frmPDF.filename = newfile
            frmPDF.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub frmAddUpdate_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If frmSearch.isSearch Then
            Dim er As New EventArgs
            frmSearch.txtVoterId.Text = Me.PER_ID.Text
            frmSearch.btnSearch_Click(sender, er)
        End If
    End Sub

    Private Sub frmAddUpdate_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            Dim myform As New Form
            Dim id As Integer
            Dim pId As String
            Dim name As String
            Dim type As String

            If finder.perList.Count > 0 Then
                id = finder.person("PER_ID")
                pId = barcode(id)

                Dim names(2) As String
                Dim full As String = finder.person("PER_FHFULLNAME").ToString
                names = full.Split(" ")

                For Each cntrl As Control In Me.Controls
                    name = cntrl.Name
                    type = cntrl.GetType.ToString

                    Select Case type
                        Case "System.Windows.Forms.TextBox"

                            Try
                                Select Case cntrl.Name
                                    Case "PER_HEAD"
                                        cntrl.Text = names(0) 'first
                                    Case "PER_HEADFATHER"
                                        cntrl.Text = names(1) 'second
                                    Case "PER_HEADGRAND"
                                        cntrl.Text = names(2) 'third
                                    Case Else
                                        cntrl.Text = finder.person(cntrl.Name).ToString
                                End Select

                                If IsNumeric(finder.person(cntrl.Name)) Then
                                    cntrl.Text = IIf(finder.person(cntrl.Name) = 0, "", finder.person(cntrl.Name).ToString)
                                End If

                            Catch ex As Exception
                                Continue For
                            End Try
                        Case "System.Windows.Forms.CheckBox"
                            Try
                                Dim c As CheckBox = cntrl
                                c.Checked = finder.person(c.Name)
                                If finder.person("CHC") Then
                                    CHG.Checked = True
                                    COR.Checked = True
                                End If
                            Catch ex As Exception
                                Continue For
                            End Try
                        Case "System.Windows.Forms.GroupBox"
                            For Each c As RadioButton In cntrl.Controls
                                Try
                                    c.Checked = finder.person(c.Name)
                                Catch ex As Exception
                                    Continue For
                                End Try
                            Next
                        Case "System.Windows.Forms.ComboBox"
                            Try
                                Dim a As String = finder.person(cntrl.Name).ToString
                                Dim cb As ComboBox = cntrl
                                cb.Text = finder.person(cntrl.Name).ToString
                            Catch ex As Exception
                                Continue For
                            End Try
                    End Select
                Next

                Try

                    PER_GENDERM.Checked = finder.person(PER_GENDER.Name)
                    PER_GENDER.Checked = Not PER_GENDERM.Checked

                    If finder.person("PER_IMAGE") IsNot Nothing Then
                        Dim imageData As Byte() = finder.person("PER_IMAGE")

                        Using ms As New MemoryStream(imageData, 0, imageData.Length)
                            ms.Write(imageData, 0, imageData.Length)
                            PER_IMAGE.Image = Image.FromStream(ms, True)
                        End Using
                        PER_IMAGE.Tag = "exist"
                    End If
                Catch ex As Exception
                    'do nothing
                End Try
            Else
                id = finder.getLastPerId()
                PER_ID.Text = id
                VRC_OID.Text = VRC
                VRC_NAME_AR.Text = VRCNAME
            End If

            PER_IMAGE.Enabled = True
            GOV_MOT_ID.Text = ConfigurationManager.AppSettings("govId")
            GOV_NAME_AR.Text = ConfigurationManager.AppSettings("govName")

            ToolTip1.SetToolTip(PER_DELETEDOCUMENTISSUEDATE, "YYYY-mm-dd")
            ToolTip1.SetToolTip(PER_DOCUMENTISSUEDATE1, "YYYY-mm-dd")
            ToolTip1.SetToolTip(PER_DOCUMENTISSUEDATE2, "YYYY-mm-dd")
            ToolTip1.SetToolTip(PER_IDPDATE, "YYYY-mm-dd")
            ToolTip1.SetToolTip(PER_IDPDOCUMENTISSUEDATE, "YYYY-mm-dd")
            Me.ScrollToControl(PER_IMAGE)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Dim myform As New PersonDAL
        Dim cntrl As Control
        Dim id As Integer

        myPER_ID = 0
        For Each cntrl In Me.Controls
            If TypeOf (cntrl) Is TextBox And cntrl.Tag <> "const" Then
                cntrl.Text = ""
            ElseIf TypeOf (cntrl) Is GroupBox Then
                Dim c As RadioButton
                For Each c In cntrl.Controls
                    c.Checked = False
                Next

            ElseIf TypeOf (cntrl) Is ComboBox Then
                Dim c As ComboBox = cntrl
                c.SelectedIndex = -1
            ElseIf TypeOf (cntrl) Is CheckBox Then
                Dim c As CheckBox = cntrl
                c.Checked = False
           

            End If

            If cntrl.Tag <> "other" Then
                cntrl.Enabled = False
            End If
        Next

        ADD.Checked = True
        frmSearch.isSearch = False
        PER_IMAGE.Enabled = True
        PER_IMAGE.ImageLocation = ""
        PER_IMAGE.Image = Nothing
        id = myform.getLastPerId()
        PER_ID.Text = id
        VRC_OID.Text = VRC
    End Sub

    Private Function barcode(num As Integer) As String
        Try
            Dim barcodeNo As String = ""
            Dim digit As Integer
            Dim division As Integer = 10000000
            Dim divResult As Double
            Dim i As Integer

            For i = 0 To 7
                divResult = num / division
                digit = Math.Truncate(divResult)

                barcodeNo = barcodeNo + digit.ToString + "     "
                num = num - (digit * division)
                division = division / 10
            Next

            Return barcodeNo

        Catch ex As Exception
            MsgBox(ex.Message)
            Return ""
        End Try
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim myPerson As New person
            Dim processPerson As New PersonDAL
            Dim result As Integer = 0
            Dim addFlag As Boolean = False
            Dim ms As New MemoryStream()
            Dim required As Boolean = False
            Dim perList As New List(Of person)
            Dim dtResult As New DataTable
            Dim r As Object


            addPerson = False
            GOV_MOT_ID.BackColor = Color.White

            If REGULAR.Checked Then
                myPerson.PER_REGTYPE = 1
            ElseIf SPECIAL.Checked Then
                myPerson.PER_REGTYPE = 2
            ElseIf ABS.Checked Then
                myPerson.PER_REGTYPE = 3
            Else
                MsgBox("الرجاء اختيار نوع التصويت")
                required = True
            End If

            If PER_IMAGE.Image Is Nothing Then
                MsgBox("الرجاءاختيار صورة")
                required = True
            End If

            For Each cntrl As Control In Me.Controls
                If (TypeOf (cntrl) Is TextBox And cntrl.Tag = IIf(ADD.Checked, "addition", IIf(CHG.Checked, "change", IIf(COR.Checked, "correct", IIf(DEL.Checked, "delete", "idp"))))) And (cntrl.Text = "") Then
                    cntrl.BackColor = Color.Red
                    cntrl.Focus()
                    required = True
                End If
            Next

            If required Then
                Exit Sub
            End If

            myPerson.PER_ID = Integer.Parse(PER_ID.Text)
            myPerson.GOV_MOT_ID = Integer.Parse(GOV_MOT_ID.Text)
            myPerson.PER_FAMID = PER_FAMNO.Text
            myPerson.PER_FIRST = PER_FIRST.Text
            myPerson.PER_FATHER = PER_FATHER.Text
            myPerson.PER_GRAND = PER_GRAND.Text
            myPerson.PC_ID = Integer.Parse(PC_ID.Text)
            myPerson.PER_DOB = Date.Parse(PER_DOB.Text)
            myPerson.userId = userId
            myPerson.PER_PUID = Integer.Parse(PER_PUID.Text)
            myPerson.PER_TYPE = "ADD"
            myPerson.PER_FHFULLNAME = PER_HEAD.Text + " " + PER_HEADFATHER.Text + " " + PER_HEADGRAND.Text
            myPerson.PER_DOCTYPE = PER_DOCUMENTTYPE1.Text
            myPerson.PER_DOCNO = PER_DOCUMENTNO1.Text
            myPerson.PER_DOCPLACE = PER_DOCUMENTISSUEPLACE1.Text
            myPerson.PER_DOCDATE = PER_DOCUMENTISSUEDATE1.Text
            myPerson.PER_DOCTYPE1 = PER_DOCUMENTTYPE2.Text
            myPerson.PER_DOCNO1 = PER_DOCUMENTNO2.Text
            myPerson.PER_DOCPLACE1 = PER_DOCUMENTISSUEPLACE2.Text
            myPerson.PER_DOCDATE1 = PER_DOCUMENTISSUEDATE2.Text
            myPerson.VRC_OID = Integer.Parse(VRC_OID.Text)
            myPerson.PER_OLDGOVNAME = PER_ORGGOVNAME.Text
            myPerson.PER_IDPDATE = PER_IDPDATE.Text

            If photoChanged Then
                Dim fs As New FileStream(PER_IMAGE.ImageLocation, FileMode.Open, FileAccess.Read)
                Dim img As Byte()
                ReDim img(fs.Length)

                fs.Read(img, 0, Convert.ToInt32(fs.Length))

                myPerson.PER_IMAGE = img
            Else
                If finder.person("PER_IMAGE") IsNot Nothing Then
                    myPerson.PER_IMAGE = finder.person("PER_IMAGE")
                End If

            End If

            r = processPerson.check_duplicate(myPerson)

            If TypeOf (r) Is Integer Then
                If r = 3 Then
                    PC_ID.BackColor = Color.Red
                    GOV_MOT_ID.BackColor = Color.Red
                    VRC_OID.BackColor = Color.Red
                End If
                Exit Sub
            Else
                dtResult = r
            End If

            If dtResult IsNot Nothing And dtResult.Rows.Count <> 0 Then

                Dim ans As New MsgBoxResult
                ans = MsgBox("لقد تم العثور على تكرار هل تريد معالجة الامر؟", MsgBoxStyle.YesNo)
                If ans = MsgBoxResult.Yes Then
                    frmDuplicate.GOV_MOT_ID.Text = GOV_MOT_ID.Text
                    frmDuplicate.GOV_NAME_AR.Text = GOV_NAME_AR.Text
                    frmDuplicate.VRC_OID.Text = VRC_OID.Text
                    frmDuplicate.VRC_NAME_AR.Text = VRC_NAME_AR.Text
                    frmDuplicate.PER_FAMNO.Text = PER_FAMNO.Text
                    frmDuplicate.PER_FIRST.Text = PER_FIRST.Text
                    frmDuplicate.PER_FATHER.Text = PER_FATHER.Text
                    frmDuplicate.PER_GRAND.Text = PER_GRAND.Text
                    frmDuplicate.PER_DOB.Text = PER_DOB.Text
                    frmDuplicate.PC_ID.Text = PC_ID.Text
                    frmDuplicate.PC_NAME.Text = PC_NAME.Text
                    frmDuplicate.gvDuplicates.DataSource = dtResult
                    frmDuplicate.PER_PUID.Text = PER_PUID.Text
                    'frmDuplicate.Label2.Text = dtResult.Rows(0)("نوع التكرار")
                    'frmDuplicate.Per_Id2 = dtResult.Rows(0)("رقم الناخب المكرر")
                    frmDuplicate.ShowDialog()
                    If addPerson Then
                        GoTo addPerson
                    Else
                        Exit Sub
                    End If

                Else
                    Dim duplicate As New PersonDAL
                    duplicate.DeleteTemp(dtResult.Rows(0)("رقم الناخب المكرر"))

                    Exit Sub
                End If
            End If

addPerson:
            frmSearch.isSearch = True
            If ADD.Checked Then
                addFlag = True
                frmSearch.isSearch = False
                If PER_GENDER.Checked Then
                    myPerson.PER_GENDER = 0
                ElseIf PER_GENDERM.Checked Then
                    myPerson.PER_GENDER = 1
                Else
                    MsgBox("الرجاءاختيار الجنس")
                    Exit Sub
                End If

                result = processPerson.AddPerson(myPerson)
                If result = 3 Then
                    PC_ID.BackColor = Color.Red
                    GOV_MOT_ID.BackColor = Color.Red
                    VRC_OID.BackColor = Color.Red
                End If
            ElseIf DEL.Checked Then

                myPerson.PER_DOCTYPE = PER_DELETEDOCUMENTTYPE.Text
                myPerson.PER_DOCNO = PER_DELETEDOCUMENTNO.Text
                myPerson.PER_DOCPLACE = PER_DELETEDOCUMENTISSUEPALCE.Text
                myPerson.PER_DOCDATE = Date.Parse(PER_DELETEDOCUMENTISSUEDATE.Text)
                myPerson.PER_TYPE = "DEL"
                result = processPerson.UpdatePerson(myPerson)
            ElseIf IDP.Checked Then
                myPerson.PER_OLDGOVID = Integer.Parse(PER_ORGGOVID.Text)
                myPerson.PER_OLDVRC = Integer.Parse(PER_IDPVRCID.Text)
                myPerson.PER_OLDFRC = Integer.Parse(PER_IDPFRCID.Text)
                myPerson.PER_DOCTYPE = PER_IDPDOCUMENTTYPE.Text
                myPerson.PER_DOCNO = PER_IDPDOCUMENTNO.Text
                myPerson.PER_DOCPLACE = PER_IDPDOCUMENTISSUEPLACE.Text
                myPerson.PER_DOCDATE = Date.Parse(PER_IDPDOCUMENTISSUEDATE.Text)
                myPerson.PER_TYPE = "IDP"
                result = processPerson.UpdatePerson(myPerson)
            Else
                If CHG.Checked Then
                    myPerson.PER_TYPE = "CHG"
                    myPerson.PER_OLDVRC = Integer.Parse(PER_OLDVRC.Text)
                    myPerson.PER_OLDFRC = Integer.Parse(PER_OLDFRC.Text)
                    myPerson.PER_DOCTYPE = CHGDocumentType.Text
                    myPerson.PER_DOCNO = CHGDocumentNumber.Text
                    myPerson.PER_DOCPLACE = CHGDocumentIssuePlace.Text
                    myPerson.PER_DOCDATE = Date.Parse(CHGDocumentIssueDate.Text)
                    myPerson.PC_ID = Integer.Parse(PC_ID.Text)
                    myPerson.VRC_OID = Integer.Parse(VRC_OID.Text)
                    myPerson.GOV_MOT_ID = Integer.Parse(GOV_MOT_ID.Text)
                End If

                If COR.Checked Then
                    myPerson.PER_TYPE = "COR"
                    If CHK_DOB.Checked Then
                        If PER_DOB.Text = "" Then
                            PER_DOB.Focus()
                            Exit Sub
                        End If

                    End If
                    If CHK_FATHERNAME.Checked Then
                        If PER_FATHER.Text = "" Then
                            PER_FATHER.Focus()
                            Exit Sub
                        End If

                    End If
                    If CHK_GRANDNAME.Checked Then
                        If PER_GRAND.Text = "" Then
                            PER_GRAND.Focus()
                            Exit Sub
                        End If

                    End If
                    If CHK_VOTERNAME.Checked Then
                        If PER_FIRST.Text = "" Then
                            PER_FIRST.Focus()
                            Exit Sub
                        End If

                    End If
                End If

                myPerson.PER_DOB = Date.Parse(PER_DOB.Text)
                myPerson.PER_FIRST = PER_FIRST.Text
                myPerson.PER_FATHER = PER_FATHER.Text
                myPerson.PER_GRAND = PER_GRAND.Text

                If CHG.Checked And COR.Checked Then
                    myPerson.PER_TYPE = "CHC"
                End If

                result = processPerson.UpdatePerson(myPerson)
                If result = 3 Then
                    PC_ID.BackColor = Color.Red
                    GOV_MOT_ID.BackColor = Color.Red
                    VRC_OID.BackColor = Color.Red
                    Exit Sub
                End If
            End If

            If result < 0 Then
                MsgBox("تم الخزن بنجاج")
            Else
                MsgBox("لم تتم عملية الخزن")
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GOV_MOT_ID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles GOV_MOT_ID.KeyPress

        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_ID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_ID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub VRC_OID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles VRC_OID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_FAMID_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_PUID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_PUID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PC_OID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PC_ID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_OLDVRC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_OLDVRC.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_OLDFRC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_OLDFRC.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_ORGGOVID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_ORGGOVID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPVRCID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPVRCID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPFRCID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPFRCID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub GOV_NAME_AR_KeyPress(sender As Object, e As KeyPressEventArgs) Handles GOV_NAME_AR.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_FIRST_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_FIRST.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_FATHER_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_FATHER.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_GRAND_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_GRAND.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub VRC_NAME_AR_KeyPress(sender As Object, e As KeyPressEventArgs) Handles VRC_NAME_AR.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_HEAD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_HEAD.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_HEADFATHER_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_HEADFATHER.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_HEADGRAND_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_HEADGRAND.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PC_NAME_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PC_NAME.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DOCUMENTTYPE2_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DOCUMENTISSUEPLACE1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOCUMENTISSUEPLACE1.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DOCUMENTISSUEPLACE2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOCUMENTISSUEPLACE2.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DELETEDOCUMENTTYPE_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DELETEDOCUMENTISSUEPALCE_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DELETEDOCUMENTISSUEPALCE.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_ORGGOVNAME_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_ORGGOVNAME.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPDOCUMENTTYPE_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPDOCUMENTISSUEPLACE_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPDOCUMENTISSUEPLACE.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub ADD_CheckedChanged(sender As Object, e As EventArgs) Handles ADD.CheckedChanged
        If ADD.Checked Then
            DEL.Checked = Not ADD.Checked
            COR.Checked = Not ADD.Checked
            CHG.Checked = Not ADD.Checked
            IDP.Checked = Not ADD.Checked
        End If

        For Each cntrl As Control In Me.Controls
            If cntrl.Tag = "addition" Then
                cntrl.Enabled = ADD.Checked
            ElseIf cntrl.Tag <> "other" And cntrl.Name <> "PER_IMAGE" Then
                cntrl.Enabled = False 'Not ADD.Checked
            End If
        Next
    End Sub

    Private Sub CHG_CheckedChanged(sender As Object, e As EventArgs) Handles CHG.CheckedChanged
        If CHG.Checked Then

            ADD.Checked = Not CHG.Checked
            DEL.Checked = Not CHG.Checked
            IDP.Checked = Not CHG.Checked
        End If

        For Each cntrl As Control In Me.Controls
            If cntrl.Tag = "change" Then
                cntrl.Visible = True
                cntrl.Enabled = CHG.Checked
            ElseIf cntrl.Tag <> "other" Then
                If cntrl.Tag = "correction" And COR.Checked Then
                    cntrl.Enabled = COR.Checked
                Else
                    cntrl.Enabled = False 'Not CHG.Checked
                End If

            End If
        Next
        VRC_NAME_AR.Enabled = CHG.Checked
        VRC_OID.Enabled = CHG.Checked
        PC_ID.Enabled = CHG.Checked
        PC_NAME.Enabled = CHG.Checked
    End Sub

    Private Sub COR_CheckedChanged(sender As Object, e As EventArgs) Handles COR.CheckedChanged
        If COR.Checked Then
            ADD.Checked = Not COR.Checked
            DEL.Checked = Not COR.Checked
            IDP.Checked = Not COR.Checked
        End If

        For Each cntrl As Control In Me.Controls
            If cntrl.Tag = "correction" Then
                cntrl.Enabled = COR.Checked
            ElseIf cntrl.Tag <> "other" Then
                If cntrl.Tag = "change" And CHG.Checked Then
                    cntrl.Enabled = CHG.Checked
                Else
                    cntrl.Enabled = False ' Not COR.Checked
                End If

            End If
        Next
    End Sub

    Private Sub DEL_CheckedChanged(sender As Object, e As EventArgs) Handles DEL.CheckedChanged
        If DEL.Checked Then

            ADD.Checked = Not DEL.Checked
            COR.Checked = Not DEL.Checked
            CHG.Checked = Not DEL.Checked
            IDP.Checked = Not DEL.Checked
        End If

        For Each cntrl As Control In Me.Controls
            If cntrl.Tag = "delete" Then
                cntrl.Enabled = DEL.Checked
            ElseIf cntrl.Tag <> "other" Then
                cntrl.Enabled = False 'Not DEL.Checked
            End If
        Next
    End Sub

    Private Sub IDP_CheckedChanged(sender As Object, e As EventArgs) Handles IDP.CheckedChanged
        If IDP.Checked Then

            ADD.Checked = Not IDP.Checked
            COR.Checked = Not IDP.Checked
            CHG.Checked = Not IDP.Checked
            DEL.Checked = Not IDP.Checked
        End If

        For Each cntrl As Control In Me.Controls
            If cntrl.Tag = "idp" Then
                cntrl.Enabled = IDP.Checked
            ElseIf cntrl.Tag <> "other" Then
                cntrl.Enabled = False 'Not IDP.Checked
            End If
        Next

        VRC_NAME_AR.Enabled = IDP.Checked
        VRC_OID.Enabled = IDP.Checked
        PC_ID.Enabled = IDP.Checked
        PC_NAME.Enabled = IDP.Checked
    End Sub

    Private Sub CHK_VOTERNAME_CheckedChanged(sender As Object, e As EventArgs) Handles CHK_VOTERNAME.CheckedChanged
        'Label11.Enabled = CHK_VOTERNAME.Checked And COR.Checked
        PER_FIRST.Enabled = CHK_VOTERNAME.Checked And COR.Checked
    End Sub

    Private Sub CHK_FATHERNAME_CheckedChanged(sender As Object, e As EventArgs) Handles CHK_FATHERNAME.CheckedChanged
        ' Label9.Enabled = CHK_FATHERNAME.Checked And COR.Checked
        PER_FATHER.Enabled = CHK_FATHERNAME.Checked And COR.Checked
    End Sub

    Private Sub CHK_GRANDNAME_CheckedChanged(sender As Object, e As EventArgs) Handles CHK_GRANDNAME.CheckedChanged
        'Label10.Enabled = CHK_GRANDNAME.Checked And COR.Checked
        PER_GRAND.Enabled = CHK_GRANDNAME.Checked And COR.Checked
    End Sub

    Private Sub CHK_DOB_CheckedChanged(sender As Object, e As EventArgs) Handles CHK_DOB.CheckedChanged
        'Label21.Enabled = CHK_DOB.Checked And COR.Checked
        PER_DOB.Enabled = CHK_DOB.Checked And COR.Checked
    End Sub

    Private Sub PER_DOCUMENTNO1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOCUMENTNO1.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DOCUMENTNO2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOCUMENTNO2.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPDOCUMENTNO_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPDOCUMENTNO.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PC_ID_TextChanged(sender As Object, e As EventArgs) Handles PC_ID.TextChanged
        PC_ID.BackColor = Color.White
        PC_ID.ForeColor = Color.Black
    End Sub

    Private Sub VRC_OID_TextChanged(sender As Object, e As EventArgs) Handles VRC_OID.TextChanged

        VRC_OID.BackColor = Color.White
        VRC_OID.ForeColor = Color.Black
        VRC = VRC_OID.Text
    End Sub

    Private Sub PER_DOCUMENTTYPE1_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

  
    Private Sub PER_IMAGE_Click(sender As Object, e As EventArgs) Handles PER_IMAGE.Click
        Form1.Show()
    End Sub

    Private Sub PER_DOCUMENTISSUEDATE1_Validated(sender As Object, e As EventArgs) Handles PER_DOCUMENTISSUEDATE1.Validated
        Dim mydate As Date
        If Not Date.TryParseExact(PER_DOCUMENTISSUEDATE1.Text, "yyyy-MM-dd", Globalization.CultureInfo.CurrentUICulture, Globalization.DateTimeStyles.None, mydate) Then
            PER_DOCUMENTISSUEDATE1.BackColor = Color.Red
        ElseIf Date.Parse(PER_DOCUMENTISSUEDATE1.Text) > Date.Now.Date Then
            PER_DOCUMENTISSUEDATE1.BackColor = Color.Red
        Else
            PER_DOCUMENTISSUEDATE1.BackColor = Color.White
        End If
    End Sub

    Private Sub PER_DOCUMENTISSUEDATE2_Validated(sender As Object, e As EventArgs) Handles PER_DOCUMENTISSUEDATE2.Validated
        Dim mydate As Date
        If Not Date.TryParseExact(PER_DOCUMENTISSUEDATE2.Text, "yyyy-MM-dd", Globalization.CultureInfo.CurrentUICulture, Globalization.DateTimeStyles.None, mydate) Then
            PER_DOCUMENTISSUEDATE2.BackColor = Color.Red
        ElseIf Date.Parse(PER_DOCUMENTISSUEDATE2.Text) > Date.Now.Date Then
            PER_DOCUMENTISSUEDATE2.BackColor = Color.Red
        Else
            PER_DOCUMENTISSUEDATE2.BackColor = Color.White
        End If
    End Sub

    Private Sub PER_DELETEDOCUMENTISSUEDATE_Validated(sender As Object, e As EventArgs) Handles PER_DELETEDOCUMENTISSUEDATE.Validated
        Dim mydate As Date
        If Not Date.TryParseExact(PER_DELETEDOCUMENTISSUEDATE.Text, "yyyy-MM-dd", Globalization.CultureInfo.CurrentUICulture, Globalization.DateTimeStyles.None, mydate) Then
            PER_DELETEDOCUMENTISSUEDATE.BackColor = Color.Red
        ElseIf Date.Parse(PER_DELETEDOCUMENTISSUEDATE.Text) > Date.Now.Date Then
            PER_DELETEDOCUMENTISSUEDATE.BackColor = Color.Red
        Else
            PER_DELETEDOCUMENTISSUEDATE.BackColor = Color.White
        End If
    End Sub

    Private Sub PER_IDPDATE_TextChanged(sender As Object, e As EventArgs) Handles PER_IDPDATE.TextChanged
        PER_IDPDATE.BackColor = Color.White
    End Sub

    Private Sub PER_IDPDOCUMENTISSUEDATE_Validated(sender As Object, e As EventArgs) Handles PER_IDPDOCUMENTISSUEDATE.Validated
        Dim mydate As Date
        If Not Date.TryParseExact(PER_IDPDOCUMENTISSUEDATE.Text, "yyyy-MM-dd", Globalization.CultureInfo.CurrentUICulture, Globalization.DateTimeStyles.None, mydate) Then
            PER_IDPDOCUMENTISSUEDATE.BackColor = Color.Red
        ElseIf Date.Parse(PER_IDPDOCUMENTISSUEDATE.Text) > Date.Now.Date Then
            PER_IDPDOCUMENTISSUEDATE.BackColor = Color.Red
        Else
            PER_IDPDOCUMENTISSUEDATE.BackColor = Color.White
        End If
    End Sub

    Private Sub VRC_NAME_AR_TextChanged(sender As Object, e As EventArgs) Handles VRC_NAME_AR.TextChanged
        VRC_NAME_AR.BackColor = Color.White
        VRCNAME = VRC_NAME_AR.Text
    End Sub

    Private Sub PER_FAMNO_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_FAMNO.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_FAMNO_TextChanged(sender As Object, e As EventArgs) Handles PER_FAMNO.TextChanged
        PER_FAMNO.BackColor = Color.White
    End Sub

    Private Sub PER_FIRST_TextChanged(sender As Object, e As EventArgs) Handles PER_FIRST.TextChanged
        PER_FIRST.BackColor = Color.White
    End Sub

    Private Sub PER_FATHER_TextChanged(sender As Object, e As EventArgs) Handles PER_FATHER.TextChanged
        PER_FATHER.BackColor = Color.White
    End Sub

    Private Sub PER_GRAND_TextChanged(sender As Object, e As EventArgs) Handles PER_GRAND.TextChanged
        PER_GRAND.BackColor = Color.White
    End Sub

    Private Sub PER_HEAD_TextChanged(sender As Object, e As EventArgs) Handles PER_HEAD.TextChanged
        PER_HEAD.BackColor = Color.White
    End Sub

    Private Sub PER_HEADFATHER_TextChanged(sender As Object, e As EventArgs) Handles PER_HEADFATHER.TextChanged
        PER_HEADFATHER.BackColor = Color.White
    End Sub

    Private Sub PER_HEADGRAND_TextChanged(sender As Object, e As EventArgs) Handles PER_HEADGRAND.TextChanged
        PER_HEADGRAND.BackColor = Color.White
    End Sub

    Private Sub PER_DOB_TextChanged(sender As Object, e As EventArgs) Handles PER_DOB.TextChanged
        PER_DOB.BackColor = Color.White
    End Sub

    Private Sub PER_PUID_TextChanged(sender As Object, e As EventArgs) Handles PER_PUID.TextChanged
        PER_PUID.BackColor = Color.White
    End Sub

    Private Sub PC_NAME_TextChanged(sender As Object, e As EventArgs) Handles PC_NAME.TextChanged
        PC_NAME.BackColor = Color.White
    End Sub

    Private Sub PER_DOCUMENTTYPE1_TextChanged(sender As Object, e As EventArgs)
        PER_DOCUMENTTYPE1.BackColor = Color.White
    End Sub

    Private Sub PER_DOCUMENTNO1_TextChanged(sender As Object, e As EventArgs) Handles PER_DOCUMENTNO1.TextChanged
        PER_DOCUMENTNO1.BackColor = Color.White
    End Sub

    Private Sub PER_DOCUMENTISSUEPLACE1_TextChanged(sender As Object, e As EventArgs) Handles PER_DOCUMENTISSUEPLACE1.TextChanged
        PER_DOCUMENTISSUEPLACE1.BackColor = Color.White
    End Sub

    Private Sub PER_DOCUMENTISSUEDATE1_TextChanged(sender As Object, e As EventArgs) Handles PER_DOCUMENTISSUEDATE1.TextChanged
        PER_DOCUMENTISSUEDATE1.BackColor = Color.White
    End Sub

    Private Sub PER_DOCUMENTTYPE2_TextChanged(sender As Object, e As EventArgs)
        PER_DOCUMENTTYPE2.BackColor = Color.White
    End Sub

    Private Sub PER_DOCUMENTNO2_TextChanged(sender As Object, e As EventArgs) Handles PER_DOCUMENTNO2.TextChanged
        PER_DOCUMENTNO2.BackColor = Color.White
    End Sub

    Private Sub PER_DOCUMENTISSUEPLACE2_TextChanged(sender As Object, e As EventArgs) Handles PER_DOCUMENTISSUEPLACE2.TextChanged
        PER_DOCUMENTISSUEPLACE2.BackColor = Color.White
    End Sub

    Private Sub PER_DOCUMENTISSUEDATE2_TextChanged(sender As Object, e As EventArgs) Handles PER_DOCUMENTISSUEDATE2.TextChanged
        PER_DOCUMENTISSUEDATE2.BackColor = Color.White
    End Sub

    Private Sub PER_OLDVRC_TextChanged(sender As Object, e As EventArgs) Handles PER_OLDVRC.TextChanged
        PER_OLDVRC.BackColor = Color.White
    End Sub

    Private Sub PER_OLDFRC_TextChanged(sender As Object, e As EventArgs) Handles PER_OLDFRC.TextChanged
        PER_OLDFRC.BackColor = Color.White
    End Sub

    Private Sub PER_DELETEDOCUMENTTYPE_TextChanged(sender As Object, e As EventArgs)
        PER_DELETEDOCUMENTTYPE.BackColor = Color.White
    End Sub

    Private Sub PER_DELETEDOCUMENTNO_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DELETEDOCUMENTNO.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DELETEDOCUMENTNO_TextChanged(sender As Object, e As EventArgs) Handles PER_DELETEDOCUMENTNO.TextChanged
        PER_DELETEDOCUMENTNO.BackColor = Color.White
    End Sub

    Private Sub PER_DELETEDOCUMENTISSUEPALCE_TextChanged(sender As Object, e As EventArgs) Handles PER_DELETEDOCUMENTISSUEPALCE.TextChanged
        PER_DELETEDOCUMENTISSUEPALCE.BackColor = Color.White
    End Sub

    Private Sub PER_DELETEDOCUMENTISSUEDATE_TextChanged(sender As Object, e As EventArgs) Handles PER_DELETEDOCUMENTISSUEDATE.TextChanged
        PER_DELETEDOCUMENTISSUEDATE.BackColor = Color.White
    End Sub

    Private Sub PER_ORGGOVID_TextChanged(sender As Object, e As EventArgs) Handles PER_ORGGOVID.TextChanged
        PER_ORGGOVID.BackColor = Color.White
    End Sub

    Private Sub PER_ORGGOVNAME_TextChanged(sender As Object, e As EventArgs) Handles PER_ORGGOVNAME.TextChanged
        PER_ORGGOVNAME.BackColor = Color.White
    End Sub

    Private Sub PER_IDPDATE_Validated(sender As Object, e As EventArgs) Handles PER_IDPDATE.Validated
        Dim mydate As Date
        If Not Date.TryParseExact(PER_IDPDATE.Text, "yyyy-MM-dd", Globalization.CultureInfo.CurrentUICulture, Globalization.DateTimeStyles.None, mydate) Then
            PER_IDPDATE.BackColor = Color.Red
        ElseIf Date.Parse(PER_IDPDATE.Text) > Date.Now.Date Then
            PER_IDPDATE.BackColor = Color.Red
        Else
            PER_IDPDATE.BackColor = Color.White
        End If
    End Sub

    Private Sub PER_IDPVRCID_TextChanged(sender As Object, e As EventArgs) Handles PER_IDPVRCID.TextChanged
        PER_IDPVRCID.BackColor = Color.White
    End Sub

    Private Sub PER_IDPFRCID_TextChanged(sender As Object, e As EventArgs) Handles PER_IDPFRCID.TextChanged
        PER_IDPFRCID.BackColor = Color.White
    End Sub

    Private Sub PER_IDPDOCUMENTTYPE_TextChanged(sender As Object, e As EventArgs)
        PER_IDPDOCUMENTTYPE.BackColor = Color.White
    End Sub

    Private Sub PER_IDPDOCUMENTNO_TextChanged(sender As Object, e As EventArgs) Handles PER_IDPDOCUMENTNO.TextChanged
        PER_IDPDOCUMENTNO.BackColor = Color.White
    End Sub

    Private Sub PER_IDPDOCUMENTISSUEPLACE_TextChanged(sender As Object, e As EventArgs) Handles PER_IDPDOCUMENTISSUEPLACE.TextChanged
        PER_IDPDOCUMENTISSUEPLACE.BackColor = Color.White
    End Sub

    Private Sub PER_IDPDOCUMENTISSUEDATE_TextChanged(sender As Object, e As EventArgs) Handles PER_IDPDOCUMENTISSUEDATE.TextChanged
        PER_IDPDOCUMENTISSUEDATE.BackColor = Color.White
    End Sub

    Private Sub VRC_OID_Validated(sender As Object, e As EventArgs) Handles VRC_OID.Validated
        Dim vrc As New PersonDAL
        Dim result As Boolean
        Dim name As String = ""
        If VRC_OID.Text = "" Then
            Exit Sub
        End If
        result = vrc.VRC_ID_NAME(Integer.Parse(VRC_OID.Text), name)
        If result = False Then
            VRC_OID.BackColor = Color.Red
        Else
            VRC_NAME_AR.Text = name
        End If
    End Sub

    Private Sub VRC_NAME_AR_Validated(sender As Object, e As EventArgs) Handles VRC_NAME_AR.Validated
        Dim vrc As New PersonDAL
        Dim result As Boolean
        Dim id As Integer = 0
        If VRC_NAME_AR.Text = "" Then
            Exit Sub
        End If
        result = vrc.VRC_ID_NAME(id, VRC_NAME_AR.Text)
        If result = False Then
            VRC_NAME_AR.BackColor = Color.Red
        Else
            VRC_OID.Text = id
        End If
    End Sub

    Private Sub PC_ID_Validated(sender As Object, e As EventArgs) Handles PC_ID.Validated
        Dim pc As New PersonDAL
        Dim result As Boolean
        Dim name As String = ""
        If PC_ID.Text = "" Then
            Exit Sub
        End If
        result = pc.PC_ID_NAME(Integer.Parse(PC_ID.Text), name)
        If result = False Then
            PC_ID.BackColor = Color.Red
        Else
            PC_NAME.Text = name
        End If
    End Sub

    Private Sub PC_NAME_Validated(sender As Object, e As EventArgs) Handles PC_NAME.Validated
        Dim pc As New PersonDAL
        Dim result As Boolean
        Dim id As Integer = 0
        If PC_NAME.Text = "" Then
            Exit Sub
        End If
        result = pc.PC_ID_NAME(id, PC_NAME.Text)
        If result = False Then
            PC_NAME.BackColor = Color.Red
        Else
            PC_ID.Text = id
        End If
    End Sub

    Private Sub PER_DOB_Validated(sender As Object, e As EventArgs) Handles PER_DOB.Validated
        Dim mydate As Date
        If Not Date.TryParseExact(PER_DOB.Text, "yyyy-MM-dd", Globalization.CultureInfo.CurrentUICulture, Globalization.DateTimeStyles.None, mydate) Then
            PER_DOB.BackColor = Color.Red
        ElseIf Date.Parse(PER_DOB.Text) > Date.Now.Date Then
            PER_DOB.BackColor = Color.Red
        ElseIf (Date.Now.Date.Year - Date.Parse(PER_DOB.Text).Year) < 18 Then
            PER_DOB.BackColor = Color.Red
        Else
            PER_DOB.BackColor = Color.White
        End If
    End Sub

    Private Sub CHGDocumentType_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub CHGDocumentIssuePlace_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CHGDocumentIssuePlace.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub CHGDocumentNumber_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CHGDocumentNumber.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub CHGDocumentIssueDate_Validated(sender As Object, e As EventArgs) Handles CHGDocumentIssueDate.Validated
        Dim mydate As Date
        If Not Date.TryParseExact(CHGDocumentIssueDate.Text, "yyyy-MM-dd", Globalization.CultureInfo.CurrentUICulture, Globalization.DateTimeStyles.None, mydate) Then
            CHGDocumentIssueDate.BackColor = Color.Red
        ElseIf Date.Parse(CHGDocumentIssueDate.Text) > Date.Now.Date Then
            CHGDocumentIssueDate.BackColor = Color.Red
        Else
            CHGDocumentIssueDate.BackColor = Color.White
        End If
    End Sub

    Private Sub PER_ORGGOVID_Validated(sender As Object, e As EventArgs) Handles PER_ORGGOVID.Validated
        Dim gov As New PersonDAL
        Dim result As Boolean
        Dim name As String = ""
        If PER_ORGGOVID.Text = "" Then
            Exit Sub
        End If
        result = gov.GOV_ID_NAME(Integer.Parse(PER_ORGGOVID.Text), name)
        If result = False Then
            PER_ORGGOVID.BackColor = Color.Red
        Else
            PER_ORGGOVNAME.Text = name
        End If
    End Sub

    Private Sub PER_ORGGOVNAME_Validated(sender As Object, e As EventArgs) Handles PER_ORGGOVNAME.Validated
        Dim gov As New PersonDAL
        Dim result As Boolean
        Dim id As Integer = 0
        If PER_ORGGOVNAME.Text = "" Then
            Exit Sub
        End If
        result = gov.GOV_ID_NAME(id, PER_ORGGOVNAME.Text)
        If result = False Then
            PER_ORGGOVNAME.BackColor = Color.Red
        Else
            PER_ORGGOVID.Text = id
        End If
    End Sub
  
    Private Sub CHGDocumentType_TextChanged(sender As Object, e As EventArgs)
        CHGDocumentType.BackColor = Color.White
    End Sub

    Private Sub CHGDocumentNumber_TextChanged(sender As Object, e As EventArgs) Handles CHGDocumentNumber.TextChanged
        CHGDocumentNumber.BackColor = Color.White
    End Sub

    Private Sub CHGDocumentIssuePlace_TextChanged(sender As Object, e As EventArgs) Handles CHGDocumentIssuePlace.TextChanged
        CHGDocumentIssuePlace.BackColor = Color.White
    End Sub

    Private Sub CHGDocumentIssueDate_TextChanged(sender As Object, e As EventArgs) Handles CHGDocumentIssueDate.TextChanged
        CHGDocumentIssueDate.BackColor = Color.White
    End Sub
End Class
