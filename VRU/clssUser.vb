﻿Imports System.Data.SqlClient

Public Class User

    Inherits DataAccessLayer

    Public userName As String
    Public password As String
    Public id As Integer
    Public Sub New()
        userName = ""
        password = ""
        id = 0
    End Sub

    Public Function Login() As Boolean
        Try
            Dim parmarray() As SqlParameter
            Dim dtResult As New DataTable
            ReDim parmarray(-1)
            Dim cmd As New SqlCommand

            If userName <> "" And password <> "" Then
                parmarray = {newparameter("@logname", SqlDbType.NVarChar, userName), newparameter("@pass", SqlDbType.NVarChar, password)}
            End If

            Using dr As SqlDataReader = ExecuteReader(CommandType.StoredProcedure, "Getusr", parmarray)

                While dr.Read()
                    If dr("Role") = 1 Then
                        id = dr("User_Id")
                        Return True
                    Else
                        id = dr("User_Id")
                        Return False
                    End If
                    
                End While

            End Using

            Return False
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

    End Function


End Class
