﻿Imports System.Configuration
Imports System.IO

Public Class frmPrintForm
    Public finder As PersonDAL
    Private fileName As String
    Private photoChanged As Boolean = False

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Try
            PDFcreator(Integer.Parse(PER_ID.Text))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub frmPrintForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim myform As New Form
            Dim id As Integer
            Dim pId As String

            If finder.perList.Count > 0 Then
                id = finder.person("PER_ID")
                pId = barcode(id)

                'lblBarcodeNo.Text = pId
                'lblBarcode.Text = "*" + id.ToString + "*"
                Dim names(2) As String
                Dim full As String = finder.person("PER_FHFULLNAME").ToString
                names = full.Split(" ")

                For Each cntrl As Control In Me.Controls
                    Dim name As String = cntrl.Name
                    If TypeOf (cntrl) Is TextBox Then
                        Try
                            If cntrl.Name = "PER_HEAD" Then
                                cntrl.Text = names(0) 'first
                            ElseIf cntrl.Name = "PER_HEADFATHER" Then
                                cntrl.Text = names(1) 'second
                            ElseIf cntrl.Name = "PER_HEADGRAND" Then
                                cntrl.Text = names(2) 'third
                            Else
                                cntrl.Text = finder.person(cntrl.Name).ToString
                            End If

                        Catch ex As Exception
                            Continue For
                        End Try
                    ElseIf TypeOf (cntrl) Is CheckBox Then
                        Try
                            Dim c As CheckBox = cntrl
                            c.Checked = finder.person(c.Name)
                            If finder.person("CHC") Then
                                CHG.Checked = True
                                COR.Checked = True
                            End If
                        Catch ex As Exception
                            Continue For
                        End Try
                    ElseIf TypeOf cntrl Is GroupBox Then
                        For Each c As RadioButton In cntrl.Controls
                            Try
                                c.Checked = finder.person(c.Name)
                            Catch ex As Exception
                                Continue For
                            End Try
                        Next
                    ElseIf TypeOf cntrl Is DateTimePicker Then
                        Try
                            Dim d As DateTimePicker = cntrl
                            d.Value = finder.person(d.Name)
                        Catch ex As Exception
                            Continue For
                        End Try
                        'ElseIf TypeOf cntrl Is PictureBox Then
                        '    Try
                        '        Dim d As PictureBox = cntrl
                        '        d.ImageLocation = Application.StartupPath + "\PersonPhoto\" + finder.person(cntrl.Name).ToString + ".jpg"
                        '    Catch ex As Exception
                        '        Continue For
                        '    End Try
                    End If

                Next

                Try
                    PER_GENDERM.Checked = finder.person(PER_GENDER.Name)
                    PER_GENDER.Checked = Not PER_GENDERM.Checked
                    Dim imageData As Byte() = finder.person("PER_IMAGE")

                    Using ms As New MemoryStream(imageData, 0, imageData.Length)
                        ms.Write(imageData, 0, imageData.Length)
                        PER_IMAGE.Image = Image.FromStream(ms, True)
                    End Using

                Catch ex As Exception
                    'do nothing
                End Try

            Else
                id = finder.getLastPerId()
                pId = barcode(id)

                'lblBarcodeNo.Text = pId
                'lblBarcode.Text = "*" + id.ToString + "*"
                PER_ID.Text = id
            End If

            GOV_MOT_ID.Text = ConfigurationManager.AppSettings("govId")
            GOV_NAME_AR.Text = ConfigurationManager.AppSettings("govName")

        Catch ex As Exception
            MsgBox(ex.Message, , Name)
        End Try

    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Dim myform As New PersonDAL
        Dim cntrl As Control
        Dim id As Integer
        Dim pId As String

        For Each cntrl In Me.Controls
            If TypeOf (cntrl) Is TextBox And cntrl.Tag <> "const" Then
                cntrl.Text = ""
            ElseIf TypeOf (cntrl) Is RadioButton Then
                Dim c As RadioButton = cntrl
                c.Checked = False
            ElseIf TypeOf (cntrl) Is ComboBox Then
                Dim c As ComboBox = cntrl
                c.SelectedIndex = -1
            ElseIf TypeOf (cntrl) Is CheckBox Then
                Dim c As CheckBox = cntrl
                c.Checked = False
            ElseIf TypeOf cntrl Is PictureBox Then
                Dim c As PictureBox = cntrl
                c.ImageLocation = ""

            End If

            If cntrl.Tag <> "other" Then
                cntrl.Enabled = False
            End If
        Next

        id = myform.getLastPerId()
        pId = barcode(id)

        'lblBarcodeNo.Text = "*" + pId + "*"
        'lblBarcode.Text = "*" + id.ToString + "*"
        PER_ID.Text = id
    End Sub

    Private Function barcode(num As Integer) As String
        Try
            Dim barcodeNo As String = ""
            Dim digit As Integer
            Dim division As Integer = 10000000
            Dim divResult As Double
            Dim i As Integer

            For i = 0 To 7
                divResult = num / division
                digit = Math.Truncate(divResult)

                barcodeNo = barcodeNo + digit.ToString + "     "
                num = num - (digit * division)
                division = division / 10
            Next

            Return barcodeNo

        Catch ex As Exception
            MsgBox(ex.Message)
            Return ""
        End Try
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim myPerson As New person
            Dim processPerson As New PersonDAL
            Dim result As Integer = 0
            Dim addFlag As Boolean = False
            Dim ms As New MemoryStream()

            GOV_MOT_ID.BackColor = Color.White

            If REGULAR.Checked Then
                myPerson.PER_REGTYPE = 1
            ElseIf SPECIAL.Checked Then
                myPerson.PER_REGTYPE = 2
            ElseIf ABS.Checked Then
                myPerson.PER_REGTYPE = 3
            Else
                MsgBox("الرجاء اختيار نوع التصويت")
                Exit Sub
            End If

            If PER_IMAGE.Image Is Nothing Then
                MsgBox("الرجاءاختيار صورة")
                Exit Sub
            End If

            If ADD.Checked Then
                addFlag = True
                For Each cntrl As Control In Me.Controls
                    If TypeOf (cntrl) Is TextBox And cntrl.Tag = "addition" And cntrl.Text = "" Then
                        cntrl.Focus()
                        Exit Sub
                    End If
                Next

                myPerson.PER_ID = Integer.Parse(PER_ID.Text)
                myPerson.GOV_MOT_ID = Integer.Parse(GOV_MOT_ID.Text)
                myPerson.PER_FAMID = PER_FAMNO.Text
                myPerson.PER_FIRST = PER_FIRST.Text
                myPerson.PER_FATHER = PER_FATHER.Text
                myPerson.PER_GRAND = PER_GRAND.Text
                myPerson.PC_ID = Integer.Parse(PC_ID.Text)
                myPerson.PER_DOB = Integer.Parse(PER_DOB.Text)
                myPerson.PER_GENDER = PER_GENDERM.Checked
                myPerson.PER_PUID = Integer.Parse(PER_PUID.Text)
                myPerson.PER_TYPE = "ADD"
                myPerson.PER_FHFULLNAME = PER_HEAD.Text + " " + PER_HEADFATHER.Text + " " + PER_HEADGRAND.Text
                myPerson.PER_DOCTYPE = PER_DOCUMENTTYPE1.Text
                myPerson.PER_DOCNO = PER_DOCUMENTNO1.Text
                myPerson.PER_DOCPLACE = PER_DOCUMENTISSUEPLACE1.Text
                myPerson.PER_DOCDATE = PER_DOCUMENTISSUEDATE1.Value
                myPerson.PER_DOCTYPE1 = PER_DOCUMENTTYPE2.Text
                myPerson.PER_DOCNO1 = PER_DOCUMENTNO2.Text
                myPerson.PER_DOCPLACE1 = PER_DOCUMENTISSUEPLACE2.Text
                myPerson.PER_DOCDATE1 = PER_DOCUMENTISSUEDATE2.Value
                myPerson.VRC_OID = Integer.Parse(VRC_OID.Text)

                If photoChanged Then
                    PER_IMAGE.Image.Save(ms, PER_IMAGE.Image.RawFormat)
                    myPerson.PER_IMAGE = ms.GetBuffer()
                Else
                    myPerson.PER_IMAGE = finder.person("PER_IMAGE")
                End If
                

                result = processPerson.AddPerson(myPerson)
                If result = 3 Then
                    PC_ID.BackColor = Color.Red
                    GOV_MOT_ID.BackColor = Color.Red
                    VRC_OID.BackColor = Color.Red

                    PC_ID.ForeColor = Color.White
                    GOV_MOT_ID.ForeColor = Color.White
                    VRC_OID.ForeColor = Color.White
                End If
            ElseIf DEL.Checked Then
                For Each cntrl As Control In Me.Controls
                    If TypeOf (cntrl) Is TextBox And cntrl.Tag = "delete" And cntrl.Text = "" Then
                        cntrl.Focus()
                        Exit Sub
                    End If
                Next

                myPerson.PER_DOCTYPE = PER_DELETEDOCUMENTTYPE.Text
                myPerson.PER_DOCNO = PER_DELETEDOCUMENTNO.Text
                myPerson.PER_DOCPLACE = PER_DELETEDOCUMENTISSUEPALCE.Text
                myPerson.PER_DOCDATE = PER_DELETEDOCUMENTISSUEDATE.Value
                myPerson.PER_ID = Integer.Parse(PER_ID.Text)
                myPerson.PER_TYPE = "DEL"

                If photoChanged Then
                    PER_IMAGE.Image.Save(ms, PER_IMAGE.Image.RawFormat)
                    myPerson.PER_IMAGE = ms.GetBuffer()
                Else
                    myPerson.PER_IMAGE = finder.person("PER_IMAGE")
                End If

                result = processPerson.UpdatePerson(myPerson)
            ElseIf IDP.Checked Then
                For Each cntrl As Control In Me.Controls
                    If TypeOf (cntrl) Is TextBox And cntrl.Tag = "idp" And cntrl.Text = "" Then
                        cntrl.Focus()
                        Exit Sub
                    End If
                Next

                myPerson.PER_OLDGOVID = Integer.Parse(PER_ORGGOVID.Text)
                myPerson.PER_OLDGOVNAME = PER_ORGGOVNAME.Text
                myPerson.PER_IDPDATE = PER_IDPDATE.Value
                myPerson.PER_OLDVRC = Integer.Parse(PER_IDPVRCID.Text)
                myPerson.PER_OLDFRC = Integer.Parse(PER_IDPFRCID.Text)
                myPerson.PER_DOCTYPE = PER_IDPDOCUMENTTYPE.Text
                myPerson.PER_DOCNO = PER_IDPDOCUMENTNO.Text
                myPerson.PER_DOCPLACE = PER_IDPDOCUMENTISSUEPLACE.Text
                myPerson.PER_DOCDATE = PER_IDPDOCUMENTISSUEDATE.Value
                myPerson.PER_ID = Integer.Parse(PER_ID.Text)
                myPerson.PER_TYPE = "IDP"
                If photoChanged Then
                    PER_IMAGE.Image.Save(ms, PER_IMAGE.Image.RawFormat)
                    myPerson.PER_IMAGE = ms.GetBuffer()
                Else
                    myPerson.PER_IMAGE = finder.person("PER_IMAGE")
                End If


                result = processPerson.UpdatePerson(myPerson)
            Else
                If CHG.Checked Then
                    myPerson.PER_TYPE = "CHG"
                    For Each cntrl As Control In Me.Controls
                        If TypeOf (cntrl) Is TextBox And cntrl.Tag = "change" And cntrl.Text = "" Then
                            cntrl.Focus()
                            Exit Sub
                        End If
                    Next
                    myPerson.PER_OLDVRC = Integer.Parse(PER_OLDVRC.Text)
                    myPerson.PER_OLDFRC = Integer.Parse(PER_OLDFRC.Text)
                End If

                If COR.Checked Then
                    myPerson.PER_TYPE = "COR"
                    If CHK_DOB.Checked Then
                        If PER_DOB.Text = "" Then
                            PER_DOB.Focus()
                            Exit Sub
                        End If

                    End If
                    If CHK_FATHERNAME.Checked Then
                        If PER_FATHER.Text = "" Then
                            PER_FATHER.Focus()
                            Exit Sub
                        End If

                    End If
                    If CHK_GRANDNAME.Checked Then
                        If PER_GRAND.Text = "" Then
                            PER_GRAND.Focus()
                            Exit Sub
                        End If

                    End If
                    If CHK_VOTERNAME.Checked Then
                        If PER_FIRST.Text = "" Then
                            PER_FIRST.Focus()
                            Exit Sub
                        End If

                    End If
                End If

                myPerson.PER_DOB = Integer.Parse(PER_DOB.Text)
                myPerson.PER_FIRST = PER_FIRST.Text
                myPerson.PER_FATHER = PER_FATHER.Text
                myPerson.PER_GRAND = PER_GRAND.Text
               
                If photoChanged Then
                    PER_IMAGE.Image.Save(ms, PER_IMAGE.Image.RawFormat)
                    myPerson.PER_IMAGE = ms.GetBuffer()
                Else
                    myPerson.PER_IMAGE = finder.person("PER_IMAGE")
                End If

                If CHG.Checked And COR.Checked Then
                    myPerson.PER_TYPE = "CHC"
                End If

                myPerson.PER_ID = Integer.Parse(PER_ID.Text)
                result = processPerson.UpdatePerson(myPerson)
            End If

            If result < 0 Then
                MsgBox("تم الخزن بنجاج")
            Else
                MsgBox("لم تتم عملية الخزن")
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GOV_MOT_ID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles GOV_MOT_ID.KeyPress

        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_ID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_ID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub VRC_OID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles VRC_OID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_FAMID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_FAMNO.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DAYOB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DAYOB.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_MONTHOB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_MONTHOB.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_YOB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOB.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_PUID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_PUID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PC_OID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PC_ID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_OLDVRC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_OLDVRC.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_OLDFRC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_OLDFRC.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_ORGGOVID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_ORGGOVID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPDAY_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPDAY.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPMONTH_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPMONTH.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPYEAR_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPYEAR.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPVRCID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPVRCID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPFRCID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPFRCID.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub GOV_NAME_AR_KeyPress(sender As Object, e As KeyPressEventArgs) Handles GOV_NAME_AR.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_FIRST_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_FIRST.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_FATHER_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_FATHER.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_GRAND_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_GRAND.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub VRC_NAME_AR_KeyPress(sender As Object, e As KeyPressEventArgs) Handles VRC_NAME_AR.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_HEAD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_HEAD.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_HEADFATHER_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_HEADFATHER.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_HEADGRAND_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_HEADGRAND.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PC_NAME_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PC_NAME.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DOCUMENTTYPE2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOCUMENTTYPE2.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DOCUMENTISSUEPLACE1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOCUMENTISSUEPLACE1.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DOCUMENTISSUEPLACE2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOCUMENTISSUEPLACE2.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DELETEDOCUMENTTYPE_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DELETEDOCUMENTTYPE.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DELETEDOCUMENTISSUEPALCE_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DELETEDOCUMENTISSUEPALCE.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_ORGGOVNAME_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_ORGGOVNAME.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPDOCUMENTTYPE_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPDOCUMENTTYPE.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPDOCUMENTISSUEPLACE_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPDOCUMENTISSUEPLACE.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub ADD_CheckedChanged(sender As Object, e As EventArgs) Handles ADD.CheckedChanged
        If ADD.Checked Then
            DEL.Checked = Not ADD.Checked
            COR.Checked = Not ADD.Checked
            CHG.Checked = Not ADD.Checked
            IDP.Checked = Not ADD.Checked
        End If

        For Each cntrl As Control In Me.Controls
            If cntrl.Tag = "addition" Then
                cntrl.Enabled = ADD.Checked
            ElseIf cntrl.Tag <> "other" Then
                cntrl.Enabled = False 'Not ADD.Checked
            End If
        Next
    End Sub

    Private Sub CHG_CheckedChanged(sender As Object, e As EventArgs) Handles CHG.CheckedChanged
        If CHG.Checked Then
            ADD.Checked = Not CHG.Checked
            DEL.Checked = Not CHG.Checked
            IDP.Checked = Not CHG.Checked
        End If

        For Each cntrl As Control In Me.Controls
            If cntrl.Tag = "change" Then
                cntrl.Enabled = CHG.Checked
            ElseIf cntrl.Tag <> "other" Then
                If cntrl.Tag = "correction" And COR.Checked Then
                    cntrl.Enabled = COR.Checked
                Else
                    cntrl.Enabled = False 'Not CHG.Checked
                End If

            End If
        Next
    End Sub

    Private Sub COR_CheckedChanged(sender As Object, e As EventArgs) Handles COR.CheckedChanged
        If COR.Checked Then
            ADD.Checked = Not COR.Checked
            DEL.Checked = Not COR.Checked
            IDP.Checked = Not COR.Checked
        End If

        For Each cntrl As Control In Me.Controls
            If cntrl.Tag = "correction" Then
                cntrl.Enabled = COR.Checked
            ElseIf cntrl.Tag <> "other" Then
                If cntrl.Tag = "change" And CHG.Checked Then
                    cntrl.Enabled = CHG.Checked
                Else
                    cntrl.Enabled = False ' Not COR.Checked
                End If

            End If
        Next
    End Sub

    Private Sub DEL_CheckedChanged(sender As Object, e As EventArgs) Handles DEL.CheckedChanged
        If DEL.Checked Then
            ADD.Checked = Not DEL.Checked
            COR.Checked = Not DEL.Checked
            CHG.Checked = Not DEL.Checked
            IDP.Checked = Not DEL.Checked
        End If

        For Each cntrl As Control In Me.Controls
            If cntrl.Tag = "delete" Then
                cntrl.Enabled = DEL.Checked
            ElseIf cntrl.Tag <> "other" Then
                cntrl.Enabled = False 'Not DEL.Checked
            End If
        Next
    End Sub

    Private Sub IDP_CheckedChanged(sender As Object, e As EventArgs) Handles IDP.CheckedChanged
        If IDP.Checked Then
            ADD.Checked = Not IDP.Checked
            COR.Checked = Not IDP.Checked
            CHG.Checked = Not IDP.Checked
            DEL.Checked = Not IDP.Checked
        End If

        For Each cntrl As Control In Me.Controls
            If cntrl.Tag = "idp" Then
                cntrl.Enabled = IDP.Checked
            ElseIf cntrl.Tag <> "other" Then
                cntrl.Enabled = False 'Not IDP.Checked
            End If
        Next
    End Sub

    Private Sub CHK_VOTERNAME_CheckedChanged(sender As Object, e As EventArgs) Handles CHK_VOTERNAME.CheckedChanged
        Label11.Enabled = CHK_VOTERNAME.Checked And COR.Checked
        PER_FIRST.Enabled = CHK_VOTERNAME.Checked And COR.Checked
    End Sub

    Private Sub CHK_FATHERNAME_CheckedChanged(sender As Object, e As EventArgs) Handles CHK_FATHERNAME.CheckedChanged
        Label9.Enabled = CHK_FATHERNAME.Checked And COR.Checked
        PER_FATHER.Enabled = CHK_FATHERNAME.Checked And COR.Checked
    End Sub

    Private Sub CHK_GRANDNAME_CheckedChanged(sender As Object, e As EventArgs) Handles CHK_GRANDNAME.CheckedChanged
        Label10.Enabled = CHK_GRANDNAME.Checked And COR.Checked
        PER_GRAND.Enabled = CHK_GRANDNAME.Checked And COR.Checked
    End Sub

    Private Sub CHK_DOB_CheckedChanged(sender As Object, e As EventArgs) Handles CHK_DOB.CheckedChanged
        Label21.Enabled = CHK_DOB.Checked And COR.Checked
        PER_DOB.Enabled = CHK_DOB.Checked And COR.Checked
    End Sub

    Private Sub PER_DOCUMENTNO1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOCUMENTNO1.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_DOCUMENTNO2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOCUMENTNO2.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PER_IDPDOCUMENTNO_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_IDPDOCUMENTNO.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub btnPhoto_Click(sender As Object, e As EventArgs) Handles btnPhoto.Click
        Dim filename As String = ""

        OpenFileDialog1.ShowDialog()

        filename = OpenFileDialog1.FileName
        If filename <> "" Then
            photoChanged = True
            PER_IMAGE.ImageLocation = filename
        End If

    End Sub

    Private Sub PC_ID_TextChanged(sender As Object, e As EventArgs) Handles PC_ID.TextChanged
        PC_ID.BackColor = Color.White
        PC_ID.ForeColor = Color.Red

    End Sub


    Private Sub VRC_OID_TextChanged(sender As Object, e As EventArgs) Handles VRC_OID.TextChanged
        VRC_OID.BackColor = Color.White
        VRC_OID.ForeColor = Color.Red
    End Sub

    Private Sub PER_DOCUMENTTYPE1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PER_DOCUMENTTYPE1.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 32 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub
End Class