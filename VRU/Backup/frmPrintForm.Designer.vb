﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrintForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PER_GENDER = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.PER_GENDERM = New System.Windows.Forms.RadioButton()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.GOV_NAME_AR = New System.Windows.Forms.TextBox()
        Me.GOV_MOT_ID = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.VRC_NAME_AR = New System.Windows.Forms.TextBox()
        Me.VRC_OID = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.PER_DOCUMENTTYPE2 = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTISSUEPLACE2 = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTNO2 = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTISSUEPLACE1 = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTNO1 = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTISSUEDATE1 = New System.Windows.Forms.DateTimePicker()
        Me.PER_DOCUMENTISSUEDATE2 = New System.Windows.Forms.DateTimePicker()
        Me.PER_ID = New System.Windows.Forms.TextBox()
        Me.PER_FIRST = New System.Windows.Forms.TextBox()
        Me.PER_FATHER = New System.Windows.Forms.TextBox()
        Me.PER_GRAND = New System.Windows.Forms.TextBox()
        Me.PER_FAMNO = New System.Windows.Forms.TextBox()
        Me.PER_HEAD = New System.Windows.Forms.TextBox()
        Me.PER_HEADFATHER = New System.Windows.Forms.TextBox()
        Me.PER_HEADGRAND = New System.Windows.Forms.TextBox()
        Me.PER_PUID = New System.Windows.Forms.TextBox()
        Me.PC_NAME = New System.Windows.Forms.TextBox()
        Me.PER_DAYOB = New System.Windows.Forms.TextBox()
        Me.PER_MONTHOB = New System.Windows.Forms.TextBox()
        Me.PER_DOB = New System.Windows.Forms.TextBox()
        Me.PC_ID = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.PER_OLDVRC = New System.Windows.Forms.TextBox()
        Me.PER_OLDFRC = New System.Windows.Forms.TextBox()
        Me.CHK_VOTERNAME = New System.Windows.Forms.CheckBox()
        Me.CHK_FATHERNAME = New System.Windows.Forms.CheckBox()
        Me.CHK_GRANDNAME = New System.Windows.Forms.CheckBox()
        Me.CHK_DOB = New System.Windows.Forms.CheckBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.PER_DELETEDOCUMENTTYPE = New System.Windows.Forms.TextBox()
        Me.PER_DELETEDOCUMENTNO = New System.Windows.Forms.TextBox()
        Me.PER_DELETEDOCUMENTISSUEPALCE = New System.Windows.Forms.TextBox()
        Me.PER_DELETEDOCUMENTISSUEDATE = New System.Windows.Forms.DateTimePicker()
        Me.PER_IDPDOCUMENTISSUEDATE = New System.Windows.Forms.DateTimePicker()
        Me.PER_IDPDOCUMENTISSUEPLACE = New System.Windows.Forms.TextBox()
        Me.PER_IDPDOCUMENTNO = New System.Windows.Forms.TextBox()
        Me.PER_IDPDOCUMENTTYPE = New System.Windows.Forms.TextBox()
        Me.ADD = New System.Windows.Forms.CheckBox()
        Me.CHG = New System.Windows.Forms.CheckBox()
        Me.COR = New System.Windows.Forms.CheckBox()
        Me.DEL = New System.Windows.Forms.CheckBox()
        Me.IDP = New System.Windows.Forms.CheckBox()
        Me.PER_IDPYEAR = New System.Windows.Forms.TextBox()
        Me.PER_IDPMONTH = New System.Windows.Forms.TextBox()
        Me.PER_IDPDAY = New System.Windows.Forms.TextBox()
        Me.PER_IDPFRCID = New System.Windows.Forms.TextBox()
        Me.PER_IDPVRCID = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.PER_ORGGOVNAME = New System.Windows.Forms.TextBox()
        Me.PER_ORGGOVID = New System.Windows.Forms.TextBox()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ABS = New System.Windows.Forms.RadioButton()
        Me.SPECIAL = New System.Windows.Forms.RadioButton()
        Me.REGULAR = New System.Windows.Forms.RadioButton()
        Me.PER_IDPDATE = New System.Windows.Forms.DateTimePicker()
        Me.PER_DOCUMENTTYPE1 = New System.Windows.Forms.TextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.PER_IMAGE = New System.Windows.Forms.PictureBox()
        Me.btnPhoto = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.PER_IMAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PER_GENDER
        '
        Me.PER_GENDER.AutoSize = True
        Me.PER_GENDER.Location = New System.Drawing.Point(43, 10)
        Me.PER_GENDER.Name = "PER_GENDER"
        Me.PER_GENDER.Size = New System.Drawing.Size(14, 13)
        Me.PER_GENDER.TabIndex = 1
        Me.PER_GENDER.TabStop = True
        Me.PER_GENDER.Tag = "addition"
        Me.PER_GENDER.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.PER_GENDER)
        Me.GroupBox1.Controls.Add(Me.PER_GENDERM)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Location = New System.Drawing.Point(603, 341)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(69, 29)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = "addition"
        '
        'PER_GENDERM
        '
        Me.PER_GENDERM.AutoSize = True
        Me.PER_GENDERM.Location = New System.Drawing.Point(13, 10)
        Me.PER_GENDERM.Name = "PER_GENDERM"
        Me.PER_GENDERM.Size = New System.Drawing.Size(14, 13)
        Me.PER_GENDERM.TabIndex = 0
        Me.PER_GENDERM.TabStop = True
        Me.PER_GENDERM.Tag = "addition"
        Me.PER_GENDERM.UseVisualStyleBackColor = True
        '
        'PictureBox17
        '
        Me.PictureBox17.Image = Global.VRU.My.Resources.Resources.s1_p2_delimiter
        Me.PictureBox17.Location = New System.Drawing.Point(373, 141)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(10, 185)
        Me.PictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox17.TabIndex = 69
        Me.PictureBox17.TabStop = False
        '
        'GOV_NAME_AR
        '
        Me.GOV_NAME_AR.Enabled = False
        Me.GOV_NAME_AR.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GOV_NAME_AR.ForeColor = System.Drawing.Color.Red
        Me.GOV_NAME_AR.Location = New System.Drawing.Point(489, 121)
        Me.GOV_NAME_AR.Multiline = True
        Me.GOV_NAME_AR.Name = "GOV_NAME_AR"
        Me.GOV_NAME_AR.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.GOV_NAME_AR.Size = New System.Drawing.Size(137, 20)
        Me.GOV_NAME_AR.TabIndex = 2
        Me.GOV_NAME_AR.Tag = "const"
        '
        'GOV_MOT_ID
        '
        Me.GOV_MOT_ID.Enabled = False
        Me.GOV_MOT_ID.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GOV_MOT_ID.ForeColor = System.Drawing.Color.Red
        Me.GOV_MOT_ID.Location = New System.Drawing.Point(624, 121)
        Me.GOV_MOT_ID.MaxLength = 2
        Me.GOV_MOT_ID.Multiline = True
        Me.GOV_MOT_ID.Name = "GOV_MOT_ID"
        Me.GOV_MOT_ID.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.GOV_MOT_ID.Size = New System.Drawing.Size(49, 20)
        Me.GOV_MOT_ID.TabIndex = 1
        Me.GOV_MOT_ID.Tag = "const"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.VRU.My.Resources.Resources.logo_top
        Me.PictureBox1.Location = New System.Drawing.Point(133, 4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(611, 96)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 46
        Me.PictureBox1.TabStop = False
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(207, 718)
        Me.btnPrint.MaximumSize = New System.Drawing.Size(87, 21)
        Me.btnPrint.MinimumSize = New System.Drawing.Size(87, 21)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(87, 21)
        Me.btnPrint.TabIndex = 0
        Me.btnPrint.Tag = "other"
        Me.btnPrint.Text = "طباعة"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Enabled = False
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(680, 105)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 141
        Me.Label1.Tag = "addition"
        Me.Label1.Text = "المحافظة"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Enabled = False
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(638, 105)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 142
        Me.Label2.Tag = "addition"
        Me.Label2.Text = "رقم    "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Enabled = False
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(595, 105)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 13)
        Me.Label3.TabIndex = 143
        Me.Label3.Tag = "addition"
        Me.Label3.Text = "اسم"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Enabled = False
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(677, 149)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 144
        Me.Label4.Tag = "addition"
        Me.Label4.Text = "رقم الناخب"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Enabled = False
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(291, 109)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(85, 13)
        Me.Label5.TabIndex = 145
        Me.Label5.Tag = "addition"
        Me.Label5.Text = "مركز تسجيل الناخبين"
        '
        'VRC_NAME_AR
        '
        Me.VRC_NAME_AR.Enabled = False
        Me.VRC_NAME_AR.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VRC_NAME_AR.ForeColor = System.Drawing.Color.Red
        Me.VRC_NAME_AR.Location = New System.Drawing.Point(17, 122)
        Me.VRC_NAME_AR.Name = "VRC_NAME_AR"
        Me.VRC_NAME_AR.Size = New System.Drawing.Size(147, 20)
        Me.VRC_NAME_AR.TabIndex = 4
        Me.VRC_NAME_AR.Tag = "addition"
        '
        'VRC_OID
        '
        Me.VRC_OID.Enabled = False
        Me.VRC_OID.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VRC_OID.ForeColor = System.Drawing.Color.Red
        Me.VRC_OID.Location = New System.Drawing.Point(164, 122)
        Me.VRC_OID.MaxLength = 5
        Me.VRC_OID.Name = "VRC_OID"
        Me.VRC_OID.Size = New System.Drawing.Size(97, 20)
        Me.VRC_OID.TabIndex = 3
        Me.VRC_OID.Tag = "addition"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Enabled = False
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(235, 105)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(19, 13)
        Me.Label6.TabIndex = 148
        Me.Label6.Tag = "addition"
        Me.Label6.Text = "رقم"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Enabled = False
        Me.Label7.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(133, 105)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(20, 13)
        Me.Label7.TabIndex = 149
        Me.Label7.Tag = "addition"
        Me.Label7.Text = "اسم"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Enabled = False
        Me.Label8.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(170, 149)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label8.Size = New System.Drawing.Size(60, 13)
        Me.Label8.TabIndex = 150
        Me.Label8.Tag = "addition"
        Me.Label8.Text = "رقم العائلة     "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Enabled = False
        Me.Label9.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(605, 230)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label9.Size = New System.Drawing.Size(52, 13)
        Me.Label9.TabIndex = 151
        Me.Label9.Tag = "addition"
        Me.Label9.Text = "اسم الاب     "
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Enabled = False
        Me.Label10.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(598, 276)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label10.Size = New System.Drawing.Size(53, 13)
        Me.Label10.TabIndex = 152
        Me.Label10.Tag = "addition"
        Me.Label10.Text = "اسم الجد     "
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Enabled = False
        Me.Label12.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(88, 181)
        Me.Label12.Name = "Label12"
        Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label12.Size = New System.Drawing.Size(142, 13)
        Me.Label12.TabIndex = 154
        Me.Label12.Tag = "addition"
        Me.Label12.Text = "  اسم رب العائلة في البطاقة التموينية"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Enabled = False
        Me.Label13.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(74, 230)
        Me.Label13.Name = "Label13"
        Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label13.Size = New System.Drawing.Size(151, 13)
        Me.Label13.TabIndex = 155
        Me.Label13.Tag = "addition"
        Me.Label13.Text = " اسم اب رب العائلة في البطاقة التموينية"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Enabled = False
        Me.Label14.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(74, 279)
        Me.Label14.Name = "Label14"
        Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label14.Size = New System.Drawing.Size(152, 13)
        Me.Label14.TabIndex = 156
        Me.Label14.Tag = "addition"
        Me.Label14.Text = " اسم جد رب العائلة في البطاقة التموينية"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Enabled = False
        Me.Label15.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(687, 334)
        Me.Label15.Name = "Label15"
        Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label15.Size = New System.Drawing.Size(31, 13)
        Me.Label15.TabIndex = 2
        Me.Label15.Tag = "addition"
        Me.Label15.Text = "الجنس"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Enabled = False
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(639, 331)
        Me.Label16.Name = "Label16"
        Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label16.Size = New System.Drawing.Size(31, 13)
        Me.Label16.TabIndex = 3
        Me.Label16.Tag = "addition"
        Me.Label16.Text = "انثى"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Enabled = False
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(609, 331)
        Me.Label17.Name = "Label17"
        Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label17.Size = New System.Drawing.Size(25, 13)
        Me.Label17.TabIndex = 4
        Me.Label17.Tag = "addition"
        Me.Label17.Text = "ذكر"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Enabled = False
        Me.Label18.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(507, 341)
        Me.Label18.Name = "Label18"
        Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label18.Size = New System.Drawing.Size(56, 13)
        Me.Label18.TabIndex = 157
        Me.Label18.Tag = "addition"
        Me.Label18.Text = "  تاريخ التولد"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Enabled = False
        Me.Label19.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(453, 291)
        Me.Label19.Name = "Label19"
        Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label19.Size = New System.Drawing.Size(19, 13)
        Me.Label19.TabIndex = 158
        Me.Label19.Tag = ""
        Me.Label19.Text = "يوم"
        Me.Label19.Visible = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Enabled = False
        Me.Label20.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(408, 291)
        Me.Label20.Name = "Label20"
        Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label20.Size = New System.Drawing.Size(24, 13)
        Me.Label20.TabIndex = 159
        Me.Label20.Tag = ""
        Me.Label20.Text = "شهر"
        Me.Label20.Visible = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Enabled = False
        Me.Label21.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(464, 337)
        Me.Label21.Name = "Label21"
        Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label21.Size = New System.Drawing.Size(22, 13)
        Me.Label21.TabIndex = 160
        Me.Label21.Tag = "addition"
        Me.Label21.Text = "سنة"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Enabled = False
        Me.Label22.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(186, 344)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(98, 13)
        Me.Label22.TabIndex = 161
        Me.Label22.Tag = "addition"
        Me.Label22.Text = "رقم وحدة الاقتراع الحالية"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Enabled = False
        Me.Label23.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(524, 374)
        Me.Label23.Name = "Label23"
        Me.Label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label23.Size = New System.Drawing.Size(70, 13)
        Me.Label23.TabIndex = 162
        Me.Label23.Tag = "addition"
        Me.Label23.Text = "رقم مركز الاقتراع"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Enabled = False
        Me.Label24.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(223, 374)
        Me.Label24.Name = "Label24"
        Me.Label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label24.Size = New System.Drawing.Size(71, 13)
        Me.Label24.TabIndex = 163
        Me.Label24.Tag = "addition"
        Me.Label24.Text = "اسم مركز الاقتراع"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Enabled = False
        Me.Label25.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(577, 419)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(118, 13)
        Me.Label25.TabIndex = 164
        Me.Label25.Tag = "addition"
        Me.Label25.Text = "الوثائق المطلوبة لمقدم الطلب "
        '
        'PER_DOCUMENTTYPE2
        '
        Me.PER_DOCUMENTTYPE2.Enabled = False
        Me.PER_DOCUMENTTYPE2.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTTYPE2.ForeColor = System.Drawing.Color.Red
        Me.PER_DOCUMENTTYPE2.Location = New System.Drawing.Point(416, 437)
        Me.PER_DOCUMENTTYPE2.MaxLength = 20
        Me.PER_DOCUMENTTYPE2.Multiline = True
        Me.PER_DOCUMENTTYPE2.Name = "PER_DOCUMENTTYPE2"
        Me.PER_DOCUMENTTYPE2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_DOCUMENTTYPE2.Size = New System.Drawing.Size(155, 21)
        Me.PER_DOCUMENTTYPE2.TabIndex = 23
        Me.PER_DOCUMENTTYPE2.Tag = "addition"
        '
        'PER_DOCUMENTISSUEPLACE2
        '
        Me.PER_DOCUMENTISSUEPLACE2.Enabled = False
        Me.PER_DOCUMENTISSUEPLACE2.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTISSUEPLACE2.ForeColor = System.Drawing.Color.Red
        Me.PER_DOCUMENTISSUEPLACE2.Location = New System.Drawing.Point(116, 437)
        Me.PER_DOCUMENTISSUEPLACE2.MaxLength = 20
        Me.PER_DOCUMENTISSUEPLACE2.Multiline = True
        Me.PER_DOCUMENTISSUEPLACE2.Name = "PER_DOCUMENTISSUEPLACE2"
        Me.PER_DOCUMENTISSUEPLACE2.Size = New System.Drawing.Size(137, 21)
        Me.PER_DOCUMENTISSUEPLACE2.TabIndex = 25
        Me.PER_DOCUMENTISSUEPLACE2.Tag = "addition"
        '
        'PER_DOCUMENTNO2
        '
        Me.PER_DOCUMENTNO2.Enabled = False
        Me.PER_DOCUMENTNO2.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTNO2.ForeColor = System.Drawing.Color.Red
        Me.PER_DOCUMENTNO2.Location = New System.Drawing.Point(259, 437)
        Me.PER_DOCUMENTNO2.MaxLength = 14
        Me.PER_DOCUMENTNO2.Multiline = True
        Me.PER_DOCUMENTNO2.Name = "PER_DOCUMENTNO2"
        Me.PER_DOCUMENTNO2.Size = New System.Drawing.Size(151, 21)
        Me.PER_DOCUMENTNO2.TabIndex = 24
        Me.PER_DOCUMENTNO2.Tag = "addition"
        '
        'PER_DOCUMENTISSUEPLACE1
        '
        Me.PER_DOCUMENTISSUEPLACE1.Enabled = False
        Me.PER_DOCUMENTISSUEPLACE1.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTISSUEPLACE1.ForeColor = System.Drawing.Color.Red
        Me.PER_DOCUMENTISSUEPLACE1.Location = New System.Drawing.Point(115, 410)
        Me.PER_DOCUMENTISSUEPLACE1.MaxLength = 20
        Me.PER_DOCUMENTISSUEPLACE1.Multiline = True
        Me.PER_DOCUMENTISSUEPLACE1.Name = "PER_DOCUMENTISSUEPLACE1"
        Me.PER_DOCUMENTISSUEPLACE1.Size = New System.Drawing.Size(138, 18)
        Me.PER_DOCUMENTISSUEPLACE1.TabIndex = 21
        Me.PER_DOCUMENTISSUEPLACE1.Tag = "addition"
        '
        'PER_DOCUMENTNO1
        '
        Me.PER_DOCUMENTNO1.Enabled = False
        Me.PER_DOCUMENTNO1.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTNO1.ForeColor = System.Drawing.Color.Red
        Me.PER_DOCUMENTNO1.Location = New System.Drawing.Point(259, 409)
        Me.PER_DOCUMENTNO1.MaxLength = 14
        Me.PER_DOCUMENTNO1.Multiline = True
        Me.PER_DOCUMENTNO1.Name = "PER_DOCUMENTNO1"
        Me.PER_DOCUMENTNO1.Size = New System.Drawing.Size(150, 20)
        Me.PER_DOCUMENTNO1.TabIndex = 20
        Me.PER_DOCUMENTNO1.Tag = "addition"
        '
        'PER_DOCUMENTISSUEDATE1
        '
        Me.PER_DOCUMENTISSUEDATE1.Enabled = False
        Me.PER_DOCUMENTISSUEDATE1.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTISSUEDATE1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.PER_DOCUMENTISSUEDATE1.Location = New System.Drawing.Point(15, 409)
        Me.PER_DOCUMENTISSUEDATE1.Name = "PER_DOCUMENTISSUEDATE1"
        Me.PER_DOCUMENTISSUEDATE1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_DOCUMENTISSUEDATE1.Size = New System.Drawing.Size(95, 20)
        Me.PER_DOCUMENTISSUEDATE1.TabIndex = 22
        Me.PER_DOCUMENTISSUEDATE1.Tag = "addition"
        Me.PER_DOCUMENTISSUEDATE1.Value = New Date(2013, 1, 1, 0, 0, 0, 0)
        '
        'PER_DOCUMENTISSUEDATE2
        '
        Me.PER_DOCUMENTISSUEDATE2.Enabled = False
        Me.PER_DOCUMENTISSUEDATE2.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTISSUEDATE2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.PER_DOCUMENTISSUEDATE2.Location = New System.Drawing.Point(15, 437)
        Me.PER_DOCUMENTISSUEDATE2.Name = "PER_DOCUMENTISSUEDATE2"
        Me.PER_DOCUMENTISSUEDATE2.Size = New System.Drawing.Size(95, 20)
        Me.PER_DOCUMENTISSUEDATE2.TabIndex = 26
        Me.PER_DOCUMENTISSUEDATE2.Tag = "addition"
        Me.PER_DOCUMENTISSUEDATE2.Value = New Date(2013, 1, 1, 0, 0, 0, 0)
        '
        'PER_ID
        '
        Me.PER_ID.Enabled = False
        Me.PER_ID.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_ID.ForeColor = System.Drawing.Color.Red
        Me.PER_ID.Location = New System.Drawing.Point(573, 149)
        Me.PER_ID.MaxLength = 8
        Me.PER_ID.Name = "PER_ID"
        Me.PER_ID.Size = New System.Drawing.Size(100, 20)
        Me.PER_ID.TabIndex = 5
        Me.PER_ID.Tag = "addition"
        '
        'PER_FIRST
        '
        Me.PER_FIRST.Enabled = False
        Me.PER_FIRST.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_FIRST.ForeColor = System.Drawing.Color.Red
        Me.PER_FIRST.Location = New System.Drawing.Point(489, 201)
        Me.PER_FIRST.MaxLength = 15
        Me.PER_FIRST.Name = "PER_FIRST"
        Me.PER_FIRST.Size = New System.Drawing.Size(184, 20)
        Me.PER_FIRST.TabIndex = 7
        Me.PER_FIRST.Tag = "addition"
        '
        'PER_FATHER
        '
        Me.PER_FATHER.Enabled = False
        Me.PER_FATHER.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_FATHER.ForeColor = System.Drawing.Color.Red
        Me.PER_FATHER.Location = New System.Drawing.Point(489, 246)
        Me.PER_FATHER.MaxLength = 15
        Me.PER_FATHER.Name = "PER_FATHER"
        Me.PER_FATHER.Size = New System.Drawing.Size(184, 20)
        Me.PER_FATHER.TabIndex = 8
        Me.PER_FATHER.Tag = "addition"
        '
        'PER_GRAND
        '
        Me.PER_GRAND.Enabled = False
        Me.PER_GRAND.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_GRAND.ForeColor = System.Drawing.Color.Red
        Me.PER_GRAND.Location = New System.Drawing.Point(489, 295)
        Me.PER_GRAND.MaxLength = 15
        Me.PER_GRAND.Name = "PER_GRAND"
        Me.PER_GRAND.Size = New System.Drawing.Size(184, 20)
        Me.PER_GRAND.TabIndex = 9
        Me.PER_GRAND.Tag = "addition"
        '
        'PER_FAMNO
        '
        Me.PER_FAMNO.Enabled = False
        Me.PER_FAMNO.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_FAMNO.ForeColor = System.Drawing.Color.Red
        Me.PER_FAMNO.Location = New System.Drawing.Point(17, 149)
        Me.PER_FAMNO.MaxLength = 5
        Me.PER_FAMNO.Name = "PER_FAMNO"
        Me.PER_FAMNO.Size = New System.Drawing.Size(147, 20)
        Me.PER_FAMNO.TabIndex = 6
        Me.PER_FAMNO.Tag = "addition"
        '
        'PER_HEAD
        '
        Me.PER_HEAD.Enabled = False
        Me.PER_HEAD.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_HEAD.ForeColor = System.Drawing.Color.Red
        Me.PER_HEAD.Location = New System.Drawing.Point(42, 201)
        Me.PER_HEAD.MaxLength = 15
        Me.PER_HEAD.Name = "PER_HEAD"
        Me.PER_HEAD.Size = New System.Drawing.Size(244, 20)
        Me.PER_HEAD.TabIndex = 10
        Me.PER_HEAD.Tag = "addition"
        '
        'PER_HEADFATHER
        '
        Me.PER_HEADFATHER.Enabled = False
        Me.PER_HEADFATHER.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_HEADFATHER.ForeColor = System.Drawing.Color.Red
        Me.PER_HEADFATHER.Location = New System.Drawing.Point(42, 246)
        Me.PER_HEADFATHER.MaxLength = 15
        Me.PER_HEADFATHER.Name = "PER_HEADFATHER"
        Me.PER_HEADFATHER.Size = New System.Drawing.Size(244, 20)
        Me.PER_HEADFATHER.TabIndex = 11
        Me.PER_HEADFATHER.Tag = "addition"
        '
        'PER_HEADGRAND
        '
        Me.PER_HEADGRAND.Enabled = False
        Me.PER_HEADGRAND.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_HEADGRAND.ForeColor = System.Drawing.Color.Red
        Me.PER_HEADGRAND.Location = New System.Drawing.Point(42, 295)
        Me.PER_HEADGRAND.MaxLength = 15
        Me.PER_HEADGRAND.Name = "PER_HEADGRAND"
        Me.PER_HEADGRAND.Size = New System.Drawing.Size(244, 20)
        Me.PER_HEADGRAND.TabIndex = 12
        Me.PER_HEADGRAND.Tag = "addition"
        '
        'PER_PUID
        '
        Me.PER_PUID.Enabled = False
        Me.PER_PUID.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_PUID.ForeColor = System.Drawing.Color.Red
        Me.PER_PUID.Location = New System.Drawing.Point(12, 341)
        Me.PER_PUID.MaxLength = 6
        Me.PER_PUID.Name = "PER_PUID"
        Me.PER_PUID.Size = New System.Drawing.Size(168, 20)
        Me.PER_PUID.TabIndex = 16
        Me.PER_PUID.Tag = "addition"
        '
        'PC_NAME
        '
        Me.PC_NAME.Enabled = False
        Me.PC_NAME.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PC_NAME.ForeColor = System.Drawing.Color.Red
        Me.PC_NAME.Location = New System.Drawing.Point(12, 371)
        Me.PC_NAME.Name = "PC_NAME"
        Me.PC_NAME.Size = New System.Drawing.Size(189, 20)
        Me.PC_NAME.TabIndex = 18
        Me.PC_NAME.Tag = "addition"
        '
        'PER_DAYOB
        '
        Me.PER_DAYOB.Enabled = False
        Me.PER_DAYOB.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DAYOB.ForeColor = System.Drawing.Color.Red
        Me.PER_DAYOB.Location = New System.Drawing.Point(442, 305)
        Me.PER_DAYOB.MaxLength = 2
        Me.PER_DAYOB.Name = "PER_DAYOB"
        Me.PER_DAYOB.Size = New System.Drawing.Size(41, 20)
        Me.PER_DAYOB.TabIndex = 13
        Me.PER_DAYOB.Tag = ""
        Me.PER_DAYOB.Visible = False
        '
        'PER_MONTHOB
        '
        Me.PER_MONTHOB.Enabled = False
        Me.PER_MONTHOB.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_MONTHOB.ForeColor = System.Drawing.Color.Red
        Me.PER_MONTHOB.Location = New System.Drawing.Point(401, 305)
        Me.PER_MONTHOB.MaxLength = 2
        Me.PER_MONTHOB.Name = "PER_MONTHOB"
        Me.PER_MONTHOB.Size = New System.Drawing.Size(41, 20)
        Me.PER_MONTHOB.TabIndex = 14
        Me.PER_MONTHOB.Tag = ""
        Me.PER_MONTHOB.Visible = False
        '
        'PER_DOB
        '
        Me.PER_DOB.Enabled = False
        Me.PER_DOB.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOB.ForeColor = System.Drawing.Color.Red
        Me.PER_DOB.Location = New System.Drawing.Point(420, 351)
        Me.PER_DOB.MaxLength = 4
        Me.PER_DOB.Name = "PER_DOB"
        Me.PER_DOB.Size = New System.Drawing.Size(82, 20)
        Me.PER_DOB.TabIndex = 15
        Me.PER_DOB.Tag = "addition"
        '
        'PC_ID
        '
        Me.PC_ID.Enabled = False
        Me.PC_ID.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PC_ID.ForeColor = System.Drawing.Color.Red
        Me.PC_ID.Location = New System.Drawing.Point(340, 377)
        Me.PC_ID.MaxLength = 8
        Me.PC_ID.Name = "PC_ID"
        Me.PC_ID.Size = New System.Drawing.Size(162, 20)
        Me.PC_ID.TabIndex = 17
        Me.PC_ID.Tag = "addition"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(389, 757)
        Me.Label27.Name = "Label27"
        Me.Label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label27.Size = New System.Drawing.Size(74, 13)
        Me.Label27.TabIndex = 191
        Me.Label27.Tag = "other"
        Me.Label27.Text = "ختم مركز التسجيل"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(218, 757)
        Me.Label28.Name = "Label28"
        Me.Label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label28.Size = New System.Drawing.Size(72, 13)
        Me.Label28.TabIndex = 192
        Me.Label28.Tag = "other"
        Me.Label28.Text = "اسم وتوقيع الكاتب"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(27, 757)
        Me.Label29.Name = "Label29"
        Me.Label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label29.Size = New System.Drawing.Size(86, 13)
        Me.Label29.TabIndex = 193
        Me.Label29.Tag = "other"
        Me.Label29.Text = "تاريخ تنظيم الاستمارة"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(689, 468)
        Me.Label30.Name = "Label30"
        Me.Label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label30.Size = New System.Drawing.Size(36, 13)
        Me.Label30.TabIndex = 194
        Me.Label30.Tag = "other"
        Me.Label30.Text = "الاضافة "
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Enabled = False
        Me.Label31.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(192, 468)
        Me.Label31.Name = "Label31"
        Me.Label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label31.Size = New System.Drawing.Size(269, 13)
        Me.Label31.TabIndex = 195
        Me.Label31.Text = "في حالة الاضافة تملأ جميع الحقول في القسم العلوي باستثناء رقم الناخب"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(699, 494)
        Me.Label32.Name = "Label32"
        Me.Label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label32.Size = New System.Drawing.Size(30, 13)
        Me.Label32.TabIndex = 196
        Me.Label32.Tag = "other"
        Me.Label32.Text = "التغيير"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Enabled = False
        Me.Label33.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(422, 494)
        Me.Label33.Name = "Label33"
        Me.Label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label33.Size = New System.Drawing.Size(119, 13)
        Me.Label33.TabIndex = 197
        Me.Label33.Tag = "change"
        Me.Label33.Text = "رقم مركز التسجيل السابق      "
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Enabled = False
        Me.Label34.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(147, 494)
        Me.Label34.Name = "Label34"
        Me.Label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label34.Size = New System.Drawing.Size(99, 13)
        Me.Label34.TabIndex = 198
        Me.Label34.Tag = "change"
        Me.Label34.Text = "رقم مركز التموين السابق"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(687, 527)
        Me.Label35.Name = "Label35"
        Me.Label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label35.Size = New System.Drawing.Size(38, 13)
        Me.Label35.TabIndex = 199
        Me.Label35.Tag = "other"
        Me.Label35.Text = "التصحيح"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Enabled = False
        Me.Label36.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(424, 527)
        Me.Label36.Name = "Label36"
        Me.Label36.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label36.Size = New System.Drawing.Size(160, 13)
        Me.Label36.TabIndex = 200
        Me.Label36.Tag = "correction"
        Me.Label36.Text = "ضع اشارة صح في المربع المراد تصحيحه "
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(695, 559)
        Me.Label37.Name = "Label37"
        Me.Label37.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label37.Size = New System.Drawing.Size(33, 13)
        Me.Label37.TabIndex = 201
        Me.Label37.Tag = "other"
        Me.Label37.Text = "الحذف "
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(675, 596)
        Me.Label38.Name = "Label38"
        Me.Label38.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label38.Size = New System.Drawing.Size(49, 39)
        Me.Label38.TabIndex = 202
        Me.Label38.Tag = "other"
        Me.Label38.Text = "التصويت " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "الغيابي" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " للمهجرين "
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Enabled = False
        Me.Label39.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(521, 596)
        Me.Label39.Name = "Label39"
        Me.Label39.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label39.Size = New System.Drawing.Size(88, 13)
        Me.Label39.TabIndex = 203
        Me.Label39.Tag = "idp"
        Me.Label39.Text = "المحافظة المهجر منها"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Enabled = False
        Me.Label40.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(295, 622)
        Me.Label40.Name = "Label40"
        Me.Label40.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label40.Size = New System.Drawing.Size(67, 13)
        Me.Label40.TabIndex = 204
        Me.Label40.Tag = "idp"
        Me.Label40.Text = "تاريخ التهجير : "
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Enabled = False
        Me.Label41.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(461, 651)
        Me.Label41.Name = "Label41"
        Me.Label41.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label41.Size = New System.Drawing.Size(130, 13)
        Me.Label41.TabIndex = 205
        Me.Label41.Tag = "idp"
        Me.Label41.Text = "رقم مركز التسجيل السابق ان وجد"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Enabled = False
        Me.Label42.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(134, 651)
        Me.Label42.Name = "Label42"
        Me.Label42.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label42.Size = New System.Drawing.Size(128, 13)
        Me.Label42.TabIndex = 206
        Me.Label42.Tag = "idp"
        Me.Label42.Text = "رقم مركز التموين السابق ان وجد"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Enabled = False
        Me.Label43.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(537, 681)
        Me.Label43.Name = "Label43"
        Me.Label43.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label43.Size = New System.Drawing.Size(78, 13)
        Me.Label43.TabIndex = 207
        Me.Label43.Tag = "idp"
        Me.Label43.Text = "وثيقة اثبات التهجير"
        '
        'PER_OLDVRC
        '
        Me.PER_OLDVRC.Enabled = False
        Me.PER_OLDVRC.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_OLDVRC.ForeColor = System.Drawing.Color.Red
        Me.PER_OLDVRC.Location = New System.Drawing.Point(301, 491)
        Me.PER_OLDVRC.MaxLength = 4
        Me.PER_OLDVRC.Name = "PER_OLDVRC"
        Me.PER_OLDVRC.Size = New System.Drawing.Size(115, 20)
        Me.PER_OLDVRC.TabIndex = 27
        Me.PER_OLDVRC.Tag = "change"
        '
        'PER_OLDFRC
        '
        Me.PER_OLDFRC.Enabled = False
        Me.PER_OLDFRC.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_OLDFRC.ForeColor = System.Drawing.Color.Red
        Me.PER_OLDFRC.Location = New System.Drawing.Point(15, 491)
        Me.PER_OLDFRC.MaxLength = 3
        Me.PER_OLDFRC.Name = "PER_OLDFRC"
        Me.PER_OLDFRC.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_OLDFRC.Size = New System.Drawing.Size(126, 20)
        Me.PER_OLDFRC.TabIndex = 28
        Me.PER_OLDFRC.Tag = "change"
        '
        'CHK_VOTERNAME
        '
        Me.CHK_VOTERNAME.AutoSize = True
        Me.CHK_VOTERNAME.Enabled = False
        Me.CHK_VOTERNAME.Location = New System.Drawing.Point(401, 527)
        Me.CHK_VOTERNAME.Name = "CHK_VOTERNAME"
        Me.CHK_VOTERNAME.Size = New System.Drawing.Size(15, 14)
        Me.CHK_VOTERNAME.TabIndex = 210
        Me.CHK_VOTERNAME.Tag = "correction"
        Me.CHK_VOTERNAME.UseVisualStyleBackColor = True
        '
        'CHK_FATHERNAME
        '
        Me.CHK_FATHERNAME.AutoSize = True
        Me.CHK_FATHERNAME.Enabled = False
        Me.CHK_FATHERNAME.Location = New System.Drawing.Point(300, 527)
        Me.CHK_FATHERNAME.Name = "CHK_FATHERNAME"
        Me.CHK_FATHERNAME.Size = New System.Drawing.Size(15, 14)
        Me.CHK_FATHERNAME.TabIndex = 211
        Me.CHK_FATHERNAME.Tag = "correction"
        Me.CHK_FATHERNAME.UseVisualStyleBackColor = True
        '
        'CHK_GRANDNAME
        '
        Me.CHK_GRANDNAME.AutoSize = True
        Me.CHK_GRANDNAME.Enabled = False
        Me.CHK_GRANDNAME.Location = New System.Drawing.Point(189, 528)
        Me.CHK_GRANDNAME.Name = "CHK_GRANDNAME"
        Me.CHK_GRANDNAME.Size = New System.Drawing.Size(15, 14)
        Me.CHK_GRANDNAME.TabIndex = 212
        Me.CHK_GRANDNAME.Tag = "correction"
        Me.CHK_GRANDNAME.UseVisualStyleBackColor = True
        '
        'CHK_DOB
        '
        Me.CHK_DOB.AutoSize = True
        Me.CHK_DOB.Enabled = False
        Me.CHK_DOB.Location = New System.Drawing.Point(88, 528)
        Me.CHK_DOB.Name = "CHK_DOB"
        Me.CHK_DOB.Size = New System.Drawing.Size(15, 14)
        Me.CHK_DOB.TabIndex = 213
        Me.CHK_DOB.Tag = "correction"
        Me.CHK_DOB.UseVisualStyleBackColor = True
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Enabled = False
        Me.Label44.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(326, 528)
        Me.Label44.Name = "Label44"
        Me.Label44.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label44.Size = New System.Drawing.Size(47, 13)
        Me.Label44.TabIndex = 214
        Me.Label44.Tag = "correction"
        Me.Label44.Text = "اسم الناخب"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Enabled = False
        Me.Label45.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(223, 527)
        Me.Label45.Name = "Label45"
        Me.Label45.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label45.Size = New System.Drawing.Size(52, 13)
        Me.Label45.TabIndex = 215
        Me.Label45.Tag = "correction"
        Me.Label45.Text = "اسم الاب     "
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Enabled = False
        Me.Label46.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(113, 527)
        Me.Label46.Name = "Label46"
        Me.Label46.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label46.Size = New System.Drawing.Size(53, 13)
        Me.Label46.TabIndex = 216
        Me.Label46.Tag = "correction"
        Me.Label46.Text = "اسم الجد     "
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Enabled = False
        Me.Label47.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(15, 529)
        Me.Label47.Name = "Label47"
        Me.Label47.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label47.Size = New System.Drawing.Size(56, 13)
        Me.Label47.TabIndex = 217
        Me.Label47.Tag = "correction"
        Me.Label47.Text = "  تاريخ التولد"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Enabled = False
        Me.Label48.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(553, 555)
        Me.Label48.Name = "Label48"
        Me.Label48.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label48.Size = New System.Drawing.Size(67, 13)
        Me.Label48.TabIndex = 218
        Me.Label48.Tag = "delete"
        Me.Label48.Text = "الوثيقة المطلوبة"
        '
        'PER_DELETEDOCUMENTTYPE
        '
        Me.PER_DELETEDOCUMENTTYPE.Enabled = False
        Me.PER_DELETEDOCUMENTTYPE.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DELETEDOCUMENTTYPE.ForeColor = System.Drawing.Color.Red
        Me.PER_DELETEDOCUMENTTYPE.Location = New System.Drawing.Point(398, 552)
        Me.PER_DELETEDOCUMENTTYPE.Name = "PER_DELETEDOCUMENTTYPE"
        Me.PER_DELETEDOCUMENTTYPE.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_DELETEDOCUMENTTYPE.Size = New System.Drawing.Size(132, 20)
        Me.PER_DELETEDOCUMENTTYPE.TabIndex = 29
        Me.PER_DELETEDOCUMENTTYPE.Tag = "delete"
        '
        'PER_DELETEDOCUMENTNO
        '
        Me.PER_DELETEDOCUMENTNO.Enabled = False
        Me.PER_DELETEDOCUMENTNO.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DELETEDOCUMENTNO.ForeColor = System.Drawing.Color.Red
        Me.PER_DELETEDOCUMENTNO.Location = New System.Drawing.Point(259, 552)
        Me.PER_DELETEDOCUMENTNO.Name = "PER_DELETEDOCUMENTNO"
        Me.PER_DELETEDOCUMENTNO.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_DELETEDOCUMENTNO.Size = New System.Drawing.Size(133, 20)
        Me.PER_DELETEDOCUMENTNO.TabIndex = 30
        Me.PER_DELETEDOCUMENTNO.Tag = "delete"
        '
        'PER_DELETEDOCUMENTISSUEPALCE
        '
        Me.PER_DELETEDOCUMENTISSUEPALCE.Enabled = False
        Me.PER_DELETEDOCUMENTISSUEPALCE.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DELETEDOCUMENTISSUEPALCE.ForeColor = System.Drawing.Color.Red
        Me.PER_DELETEDOCUMENTISSUEPALCE.Location = New System.Drawing.Point(116, 552)
        Me.PER_DELETEDOCUMENTISSUEPALCE.Name = "PER_DELETEDOCUMENTISSUEPALCE"
        Me.PER_DELETEDOCUMENTISSUEPALCE.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_DELETEDOCUMENTISSUEPALCE.Size = New System.Drawing.Size(137, 20)
        Me.PER_DELETEDOCUMENTISSUEPALCE.TabIndex = 31
        Me.PER_DELETEDOCUMENTISSUEPALCE.Tag = "delete"
        '
        'PER_DELETEDOCUMENTISSUEDATE
        '
        Me.PER_DELETEDOCUMENTISSUEDATE.Enabled = False
        Me.PER_DELETEDOCUMENTISSUEDATE.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DELETEDOCUMENTISSUEDATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.PER_DELETEDOCUMENTISSUEDATE.Location = New System.Drawing.Point(15, 551)
        Me.PER_DELETEDOCUMENTISSUEDATE.Name = "PER_DELETEDOCUMENTISSUEDATE"
        Me.PER_DELETEDOCUMENTISSUEDATE.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_DELETEDOCUMENTISSUEDATE.Size = New System.Drawing.Size(95, 20)
        Me.PER_DELETEDOCUMENTISSUEDATE.TabIndex = 32
        Me.PER_DELETEDOCUMENTISSUEDATE.Tag = "delete"
        Me.PER_DELETEDOCUMENTISSUEDATE.Value = New Date(2013, 1, 1, 0, 0, 0, 0)
        '
        'PER_IDPDOCUMENTISSUEDATE
        '
        Me.PER_IDPDOCUMENTISSUEDATE.Enabled = False
        Me.PER_IDPDOCUMENTISSUEDATE.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDOCUMENTISSUEDATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.PER_IDPDOCUMENTISSUEDATE.Location = New System.Drawing.Point(12, 681)
        Me.PER_IDPDOCUMENTISSUEDATE.Name = "PER_IDPDOCUMENTISSUEDATE"
        Me.PER_IDPDOCUMENTISSUEDATE.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_IDPDOCUMENTISSUEDATE.Size = New System.Drawing.Size(95, 20)
        Me.PER_IDPDOCUMENTISSUEDATE.TabIndex = 43
        Me.PER_IDPDOCUMENTISSUEDATE.Tag = "idp"
        Me.PER_IDPDOCUMENTISSUEDATE.Value = New Date(2013, 1, 1, 0, 0, 0, 0)
        '
        'PER_IDPDOCUMENTISSUEPLACE
        '
        Me.PER_IDPDOCUMENTISSUEPLACE.Enabled = False
        Me.PER_IDPDOCUMENTISSUEPLACE.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDOCUMENTISSUEPLACE.ForeColor = System.Drawing.Color.Red
        Me.PER_IDPDOCUMENTISSUEPLACE.Location = New System.Drawing.Point(113, 681)
        Me.PER_IDPDOCUMENTISSUEPLACE.MaxLength = 15
        Me.PER_IDPDOCUMENTISSUEPLACE.Name = "PER_IDPDOCUMENTISSUEPLACE"
        Me.PER_IDPDOCUMENTISSUEPLACE.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_IDPDOCUMENTISSUEPLACE.Size = New System.Drawing.Size(137, 20)
        Me.PER_IDPDOCUMENTISSUEPLACE.TabIndex = 42
        Me.PER_IDPDOCUMENTISSUEPLACE.Tag = "idp"
        '
        'PER_IDPDOCUMENTNO
        '
        Me.PER_IDPDOCUMENTNO.Enabled = False
        Me.PER_IDPDOCUMENTNO.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDOCUMENTNO.ForeColor = System.Drawing.Color.Red
        Me.PER_IDPDOCUMENTNO.Location = New System.Drawing.Point(258, 681)
        Me.PER_IDPDOCUMENTNO.MaxLength = 10
        Me.PER_IDPDOCUMENTNO.Name = "PER_IDPDOCUMENTNO"
        Me.PER_IDPDOCUMENTNO.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_IDPDOCUMENTNO.Size = New System.Drawing.Size(133, 20)
        Me.PER_IDPDOCUMENTNO.TabIndex = 41
        Me.PER_IDPDOCUMENTNO.Tag = "idp"
        '
        'PER_IDPDOCUMENTTYPE
        '
        Me.PER_IDPDOCUMENTTYPE.Enabled = False
        Me.PER_IDPDOCUMENTTYPE.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDOCUMENTTYPE.ForeColor = System.Drawing.Color.Red
        Me.PER_IDPDOCUMENTTYPE.Location = New System.Drawing.Point(402, 681)
        Me.PER_IDPDOCUMENTTYPE.MaxLength = 20
        Me.PER_IDPDOCUMENTTYPE.Name = "PER_IDPDOCUMENTTYPE"
        Me.PER_IDPDOCUMENTTYPE.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_IDPDOCUMENTTYPE.Size = New System.Drawing.Size(132, 20)
        Me.PER_IDPDOCUMENTTYPE.TabIndex = 40
        Me.PER_IDPDOCUMENTTYPE.Tag = "idp"
        '
        'ADD
        '
        Me.ADD.AutoSize = True
        Me.ADD.Location = New System.Drawing.Point(658, 467)
        Me.ADD.Name = "ADD"
        Me.ADD.Size = New System.Drawing.Size(15, 14)
        Me.ADD.TabIndex = 227
        Me.ADD.Tag = "other"
        Me.ADD.UseVisualStyleBackColor = True
        '
        'CHG
        '
        Me.CHG.AutoSize = True
        Me.CHG.Location = New System.Drawing.Point(658, 494)
        Me.CHG.Name = "CHG"
        Me.CHG.Size = New System.Drawing.Size(15, 14)
        Me.CHG.TabIndex = 228
        Me.CHG.Tag = "other"
        Me.CHG.UseVisualStyleBackColor = True
        '
        'COR
        '
        Me.COR.AutoSize = True
        Me.COR.Location = New System.Drawing.Point(658, 529)
        Me.COR.Name = "COR"
        Me.COR.Size = New System.Drawing.Size(15, 14)
        Me.COR.TabIndex = 229
        Me.COR.Tag = "other"
        Me.COR.UseVisualStyleBackColor = True
        '
        'DEL
        '
        Me.DEL.AutoSize = True
        Me.DEL.Location = New System.Drawing.Point(658, 559)
        Me.DEL.Name = "DEL"
        Me.DEL.Size = New System.Drawing.Size(15, 14)
        Me.DEL.TabIndex = 230
        Me.DEL.Tag = "other"
        Me.DEL.UseVisualStyleBackColor = True
        '
        'IDP
        '
        Me.IDP.AutoSize = True
        Me.IDP.Location = New System.Drawing.Point(658, 610)
        Me.IDP.Name = "IDP"
        Me.IDP.Size = New System.Drawing.Size(15, 14)
        Me.IDP.TabIndex = 231
        Me.IDP.Tag = "other"
        Me.IDP.UseVisualStyleBackColor = True
        '
        'PER_IDPYEAR
        '
        Me.PER_IDPYEAR.Enabled = False
        Me.PER_IDPYEAR.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPYEAR.ForeColor = System.Drawing.Color.Red
        Me.PER_IDPYEAR.Location = New System.Drawing.Point(37, 593)
        Me.PER_IDPYEAR.MaxLength = 4
        Me.PER_IDPYEAR.Name = "PER_IDPYEAR"
        Me.PER_IDPYEAR.Size = New System.Drawing.Size(82, 20)
        Me.PER_IDPYEAR.TabIndex = 37
        Me.PER_IDPYEAR.Tag = ""
        Me.PER_IDPYEAR.Visible = False
        '
        'PER_IDPMONTH
        '
        Me.PER_IDPMONTH.Enabled = False
        Me.PER_IDPMONTH.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPMONTH.ForeColor = System.Drawing.Color.Red
        Me.PER_IDPMONTH.Location = New System.Drawing.Point(119, 593)
        Me.PER_IDPMONTH.MaxLength = 2
        Me.PER_IDPMONTH.Name = "PER_IDPMONTH"
        Me.PER_IDPMONTH.Size = New System.Drawing.Size(41, 20)
        Me.PER_IDPMONTH.TabIndex = 36
        Me.PER_IDPMONTH.Tag = ""
        Me.PER_IDPMONTH.Visible = False
        '
        'PER_IDPDAY
        '
        Me.PER_IDPDAY.Enabled = False
        Me.PER_IDPDAY.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDAY.ForeColor = System.Drawing.Color.Red
        Me.PER_IDPDAY.Location = New System.Drawing.Point(160, 593)
        Me.PER_IDPDAY.MaxLength = 2
        Me.PER_IDPDAY.Name = "PER_IDPDAY"
        Me.PER_IDPDAY.Size = New System.Drawing.Size(41, 20)
        Me.PER_IDPDAY.TabIndex = 35
        Me.PER_IDPDAY.Tag = ""
        Me.PER_IDPDAY.Visible = False
        '
        'PER_IDPFRCID
        '
        Me.PER_IDPFRCID.Enabled = False
        Me.PER_IDPFRCID.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPFRCID.ForeColor = System.Drawing.Color.Red
        Me.PER_IDPFRCID.Location = New System.Drawing.Point(10, 648)
        Me.PER_IDPFRCID.MaxLength = 3
        Me.PER_IDPFRCID.Name = "PER_IDPFRCID"
        Me.PER_IDPFRCID.Size = New System.Drawing.Size(118, 20)
        Me.PER_IDPFRCID.TabIndex = 39
        Me.PER_IDPFRCID.Tag = "idp"
        '
        'PER_IDPVRCID
        '
        Me.PER_IDPVRCID.Enabled = False
        Me.PER_IDPVRCID.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPVRCID.ForeColor = System.Drawing.Color.Red
        Me.PER_IDPVRCID.Location = New System.Drawing.Point(318, 648)
        Me.PER_IDPVRCID.MaxLength = 4
        Me.PER_IDPVRCID.Name = "PER_IDPVRCID"
        Me.PER_IDPVRCID.Size = New System.Drawing.Size(137, 20)
        Me.PER_IDPVRCID.TabIndex = 38
        Me.PER_IDPVRCID.Tag = "idp"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Enabled = False
        Me.Label49.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.Location = New System.Drawing.Point(351, 580)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(20, 13)
        Me.Label49.TabIndex = 240
        Me.Label49.Tag = "idp"
        Me.Label49.Text = "اسم"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Enabled = False
        Me.Label50.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(394, 580)
        Me.Label50.Name = "Label50"
        Me.Label50.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label50.Size = New System.Drawing.Size(31, 13)
        Me.Label50.TabIndex = 239
        Me.Label50.Tag = "idp"
        Me.Label50.Text = "رقم    "
        '
        'PER_ORGGOVNAME
        '
        Me.PER_ORGGOVNAME.Enabled = False
        Me.PER_ORGGOVNAME.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_ORGGOVNAME.ForeColor = System.Drawing.Color.Red
        Me.PER_ORGGOVNAME.Location = New System.Drawing.Point(245, 596)
        Me.PER_ORGGOVNAME.Multiline = True
        Me.PER_ORGGOVNAME.Name = "PER_ORGGOVNAME"
        Me.PER_ORGGOVNAME.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_ORGGOVNAME.Size = New System.Drawing.Size(137, 20)
        Me.PER_ORGGOVNAME.TabIndex = 34
        Me.PER_ORGGOVNAME.Tag = "idp"
        '
        'PER_ORGGOVID
        '
        Me.PER_ORGGOVID.Enabled = False
        Me.PER_ORGGOVID.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_ORGGOVID.ForeColor = System.Drawing.Color.Red
        Me.PER_ORGGOVID.Location = New System.Drawing.Point(382, 596)
        Me.PER_ORGGOVID.MaxLength = 2
        Me.PER_ORGGOVID.Multiline = True
        Me.PER_ORGGOVID.Name = "PER_ORGGOVID"
        Me.PER_ORGGOVID.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PER_ORGGOVID.Size = New System.Drawing.Size(49, 20)
        Me.PER_ORGGOVID.TabIndex = 33
        Me.PER_ORGGOVID.Tag = "idp"
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(114, 718)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(87, 21)
        Me.btnClear.TabIndex = 244
        Me.btnClear.Tag = "other"
        Me.btnClear.Text = "تفريغ الاستمارة"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(20, 718)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 21)
        Me.btnSave.TabIndex = 245
        Me.btnSave.Tag = "other"
        Me.btnSave.Text = "حفظ"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ABS)
        Me.GroupBox2.Controls.Add(Me.SPECIAL)
        Me.GroupBox2.Controls.Add(Me.REGULAR)
        Me.GroupBox2.Location = New System.Drawing.Point(402, 707)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(331, 40)
        Me.GroupBox2.TabIndex = 247
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Tag = "other"
        '
        'ABS
        '
        Me.ABS.AutoSize = True
        Me.ABS.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ABS.Location = New System.Drawing.Point(10, 14)
        Me.ABS.Name = "ABS"
        Me.ABS.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ABS.Size = New System.Drawing.Size(84, 17)
        Me.ABS.TabIndex = 2
        Me.ABS.TabStop = True
        Me.ABS.Tag = "other"
        Me.ABS.Text = "تصويت مهجرين"
        Me.ABS.UseVisualStyleBackColor = True
        '
        'SPECIAL
        '
        Me.SPECIAL.AutoSize = True
        Me.SPECIAL.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SPECIAL.Location = New System.Drawing.Point(125, 14)
        Me.SPECIAL.Name = "SPECIAL"
        Me.SPECIAL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SPECIAL.Size = New System.Drawing.Size(75, 17)
        Me.SPECIAL.TabIndex = 1
        Me.SPECIAL.TabStop = True
        Me.SPECIAL.Tag = "other"
        Me.SPECIAL.Text = "تصويت خاص"
        Me.SPECIAL.UseVisualStyleBackColor = True
        '
        'REGULAR
        '
        Me.REGULAR.AutoSize = True
        Me.REGULAR.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REGULAR.Location = New System.Drawing.Point(244, 14)
        Me.REGULAR.Name = "REGULAR"
        Me.REGULAR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.REGULAR.Size = New System.Drawing.Size(67, 17)
        Me.REGULAR.TabIndex = 0
        Me.REGULAR.TabStop = True
        Me.REGULAR.Tag = "other"
        Me.REGULAR.Text = "تصويت عام"
        Me.REGULAR.UseVisualStyleBackColor = True
        '
        'PER_IDPDATE
        '
        Me.PER_IDPDATE.Enabled = False
        Me.PER_IDPDATE.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.PER_IDPDATE.Location = New System.Drawing.Point(196, 620)
        Me.PER_IDPDATE.Name = "PER_IDPDATE"
        Me.PER_IDPDATE.Size = New System.Drawing.Size(95, 20)
        Me.PER_IDPDATE.TabIndex = 248
        Me.PER_IDPDATE.Tag = "idp"
        Me.PER_IDPDATE.Value = New Date(2013, 1, 1, 0, 0, 0, 0)
        '
        'PER_DOCUMENTTYPE1
        '
        Me.PER_DOCUMENTTYPE1.Enabled = False
        Me.PER_DOCUMENTTYPE1.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTTYPE1.ForeColor = System.Drawing.Color.Red
        Me.PER_DOCUMENTTYPE1.Location = New System.Drawing.Point(416, 410)
        Me.PER_DOCUMENTTYPE1.Name = "PER_DOCUMENTTYPE1"
        Me.PER_DOCUMENTTYPE1.Size = New System.Drawing.Size(155, 20)
        Me.PER_DOCUMENTTYPE1.TabIndex = 19
        Me.PER_DOCUMENTTYPE1.Tag = "addition"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.VRU.My.Resources.Resources.s1_p2_delimiter
        Me.PictureBox2.Location = New System.Drawing.Point(373, 141)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(10, 185)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox2.TabIndex = 69
        Me.PictureBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.RadioButton1)
        Me.GroupBox3.Controls.Add(Me.RadioButton2)
        Me.GroupBox3.Enabled = False
        Me.GroupBox3.Location = New System.Drawing.Point(603, 341)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(69, 29)
        Me.GroupBox3.TabIndex = 78
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Tag = "addition"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(43, 10)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(14, 13)
        Me.RadioButton1.TabIndex = 1
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Tag = "addition"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(9, 10)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(14, 13)
        Me.RadioButton2.TabIndex = 0
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Tag = "addition"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Enabled = False
        Me.Label11.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(607, 182)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label11.Size = New System.Drawing.Size(47, 13)
        Me.Label11.TabIndex = 153
        Me.Label11.Tag = "addition"
        Me.Label11.Text = "اسم الناخب"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(600, 757)
        Me.Label26.Name = "Label26"
        Me.Label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label26.Size = New System.Drawing.Size(101, 13)
        Me.Label26.TabIndex = 190
        Me.Label26.Tag = "other"
        Me.Label26.Text = "اسم وتوقيع صاحب الطلب "
        '
        'PER_IMAGE
        '
        Me.PER_IMAGE.Location = New System.Drawing.Point(12, 4)
        Me.PER_IMAGE.Name = "PER_IMAGE"
        Me.PER_IMAGE.Size = New System.Drawing.Size(119, 95)
        Me.PER_IMAGE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PER_IMAGE.TabIndex = 249
        Me.PER_IMAGE.TabStop = False
        Me.PER_IMAGE.Tag = "other"
        '
        'btnPhoto
        '
        Me.btnPhoto.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPhoto.Location = New System.Drawing.Point(644, 672)
        Me.btnPhoto.Name = "btnPhoto"
        Me.btnPhoto.Size = New System.Drawing.Size(89, 22)
        Me.btnPhoto.TabIndex = 250
        Me.btnPhoto.Tag = "other"
        Me.btnPhoto.Text = "أختيار صورة الشخص"
        Me.btnPhoto.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "JPG files (*.JPG)|*.JPG"
        '
        'frmPrintForm
        '
        Me.AcceptButton = Me.btnPrint
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(750, 773)
        Me.Controls.Add(Me.btnPhoto)
        Me.Controls.Add(Me.PER_IMAGE)
        Me.Controls.Add(Me.PER_DOCUMENTTYPE1)
        Me.Controls.Add(Me.PER_IDPDATE)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.Label49)
        Me.Controls.Add(Me.Label50)
        Me.Controls.Add(Me.PER_ORGGOVNAME)
        Me.Controls.Add(Me.PER_ORGGOVID)
        Me.Controls.Add(Me.PER_IDPVRCID)
        Me.Controls.Add(Me.PER_IDPFRCID)
        Me.Controls.Add(Me.PER_IDPYEAR)
        Me.Controls.Add(Me.PER_IDPMONTH)
        Me.Controls.Add(Me.PER_IDPDAY)
        Me.Controls.Add(Me.IDP)
        Me.Controls.Add(Me.DEL)
        Me.Controls.Add(Me.COR)
        Me.Controls.Add(Me.CHG)
        Me.Controls.Add(Me.ADD)
        Me.Controls.Add(Me.PER_IDPDOCUMENTISSUEDATE)
        Me.Controls.Add(Me.PER_IDPDOCUMENTISSUEPLACE)
        Me.Controls.Add(Me.PER_IDPDOCUMENTNO)
        Me.Controls.Add(Me.PER_IDPDOCUMENTTYPE)
        Me.Controls.Add(Me.PER_DELETEDOCUMENTISSUEDATE)
        Me.Controls.Add(Me.PER_DELETEDOCUMENTISSUEPALCE)
        Me.Controls.Add(Me.PER_DELETEDOCUMENTNO)
        Me.Controls.Add(Me.PER_DELETEDOCUMENTTYPE)
        Me.Controls.Add(Me.Label48)
        Me.Controls.Add(Me.Label47)
        Me.Controls.Add(Me.Label46)
        Me.Controls.Add(Me.Label45)
        Me.Controls.Add(Me.Label44)
        Me.Controls.Add(Me.CHK_DOB)
        Me.Controls.Add(Me.CHK_GRANDNAME)
        Me.Controls.Add(Me.CHK_FATHERNAME)
        Me.Controls.Add(Me.CHK_VOTERNAME)
        Me.Controls.Add(Me.PER_OLDFRC)
        Me.Controls.Add(Me.PER_OLDVRC)
        Me.Controls.Add(Me.Label43)
        Me.Controls.Add(Me.Label42)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.Label39)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.PC_ID)
        Me.Controls.Add(Me.PER_DOB)
        Me.Controls.Add(Me.PER_MONTHOB)
        Me.Controls.Add(Me.PER_DAYOB)
        Me.Controls.Add(Me.PC_NAME)
        Me.Controls.Add(Me.PER_PUID)
        Me.Controls.Add(Me.PER_HEADGRAND)
        Me.Controls.Add(Me.PER_HEADFATHER)
        Me.Controls.Add(Me.PER_HEAD)
        Me.Controls.Add(Me.PER_FAMNO)
        Me.Controls.Add(Me.PER_GRAND)
        Me.Controls.Add(Me.PER_FATHER)
        Me.Controls.Add(Me.PER_FIRST)
        Me.Controls.Add(Me.PER_ID)
        Me.Controls.Add(Me.PER_DOCUMENTISSUEDATE2)
        Me.Controls.Add(Me.PER_DOCUMENTISSUEDATE1)
        Me.Controls.Add(Me.PER_DOCUMENTTYPE2)
        Me.Controls.Add(Me.PER_DOCUMENTISSUEPLACE2)
        Me.Controls.Add(Me.PER_DOCUMENTNO2)
        Me.Controls.Add(Me.PER_DOCUMENTISSUEPLACE1)
        Me.Controls.Add(Me.PER_DOCUMENTNO1)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.VRC_OID)
        Me.Controls.Add(Me.VRC_NAME_AR)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.PictureBox17)
        Me.Controls.Add(Me.GOV_NAME_AR)
        Me.Controls.Add(Me.GOV_MOT_ID)
        Me.Controls.Add(Me.PictureBox1)
        Me.MaximumSize = New System.Drawing.Size(766, 811)
        Me.MinimumSize = New System.Drawing.Size(766, 811)
        Me.Name = "frmPrintForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "واجهة طباعة الاستمارة"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.PER_IMAGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PER_GENDER As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PER_GENDERM As System.Windows.Forms.RadioButton
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents GOV_NAME_AR As System.Windows.Forms.TextBox
    Friend WithEvents GOV_MOT_ID As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents VRC_OID As System.Windows.Forms.TextBox
    Friend WithEvents VRC_NAME_AR As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents PER_DOCUMENTISSUEDATE2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents PER_DOCUMENTISSUEDATE1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents PER_DOCUMENTTYPE2 As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOCUMENTISSUEPLACE2 As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOCUMENTNO2 As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOCUMENTISSUEPLACE1 As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOCUMENTNO1 As System.Windows.Forms.TextBox
    Friend WithEvents PC_ID As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOB As System.Windows.Forms.TextBox
    Friend WithEvents PER_MONTHOB As System.Windows.Forms.TextBox
    Friend WithEvents PER_DAYOB As System.Windows.Forms.TextBox
    Friend WithEvents PC_NAME As System.Windows.Forms.TextBox
    Friend WithEvents PER_PUID As System.Windows.Forms.TextBox
    Friend WithEvents PER_HEADGRAND As System.Windows.Forms.TextBox
    Friend WithEvents PER_HEADFATHER As System.Windows.Forms.TextBox
    Friend WithEvents PER_HEAD As System.Windows.Forms.TextBox
    Friend WithEvents PER_FAMNO As System.Windows.Forms.TextBox
    Friend WithEvents PER_GRAND As System.Windows.Forms.TextBox
    Friend WithEvents PER_FATHER As System.Windows.Forms.TextBox
    Friend WithEvents PER_FIRST As System.Windows.Forms.TextBox
    Friend WithEvents PER_ID As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents PER_OLDFRC As System.Windows.Forms.TextBox
    Friend WithEvents PER_OLDVRC As System.Windows.Forms.TextBox
    Friend WithEvents CHK_DOB As System.Windows.Forms.CheckBox
    Friend WithEvents CHK_GRANDNAME As System.Windows.Forms.CheckBox
    Friend WithEvents CHK_FATHERNAME As System.Windows.Forms.CheckBox
    Friend WithEvents CHK_VOTERNAME As System.Windows.Forms.CheckBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents PER_DELETEDOCUMENTISSUEDATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents PER_DELETEDOCUMENTISSUEPALCE As System.Windows.Forms.TextBox
    Friend WithEvents PER_DELETEDOCUMENTNO As System.Windows.Forms.TextBox
    Friend WithEvents PER_DELETEDOCUMENTTYPE As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents IDP As System.Windows.Forms.CheckBox
    Friend WithEvents DEL As System.Windows.Forms.CheckBox
    Friend WithEvents COR As System.Windows.Forms.CheckBox
    Friend WithEvents CHG As System.Windows.Forms.CheckBox
    Friend WithEvents ADD As System.Windows.Forms.CheckBox
    Friend WithEvents PER_IDPDOCUMENTISSUEDATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents PER_IDPDOCUMENTISSUEPLACE As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPDOCUMENTNO As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPDOCUMENTTYPE As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents PER_ORGGOVNAME As System.Windows.Forms.TextBox
    Friend WithEvents PER_ORGGOVID As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPVRCID As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPFRCID As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPYEAR As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPMONTH As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPDAY As System.Windows.Forms.TextBox
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ABS As System.Windows.Forms.RadioButton
    Friend WithEvents SPECIAL As System.Windows.Forms.RadioButton
    Friend WithEvents PER_IDPDATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents PER_DOCUMENTTYPE1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents REGULAR As System.Windows.Forms.RadioButton
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents btnPhoto As System.Windows.Forms.Button
    Friend WithEvents PER_IMAGE As System.Windows.Forms.PictureBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
End Class
