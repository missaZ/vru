﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration


Public Class DataAccessControl
    Private _dbConn As SqlConnection
    Private _connectionString As String

    Public Sub New()
        _connectionString = ConfigurationManager.ConnectionStrings("ConnectionVRU").ConnectionString
        _dbConn = New SqlConnection(_connectionString)
    End Sub

    Public Sub OpenConnection()
        If Not _dbConn.State = ConnectionState.Open Then
            _dbConn.Open()
        End If
    End Sub

    Public Sub CloseConnection()
        If Not _dbConn.State = ConnectionState.Closed Then
            _dbConn.Close()
        End If
    End Sub

    Public Function newparameter(ByVal name As String, ByVal type As SqlDbType, ByVal value As Object) As SqlParameter
        Dim param As New SqlParameter(name, type)
        param.Value = value
        Return param
    End Function

    Public Function searchForm(ByVal spname As String, ByRef dtb As DataTable, ByVal ParamArray params() As SqlParameter) As Integer
        Try
            OpenConnection()

            Dim sqladapt As SqlDataAdapter
            Dim sqlcomm As New SqlCommand
            sqlcomm.Connection = _dbConn
            sqlcomm.CommandType = CommandType.StoredProcedure
            sqlcomm.CommandText = spname
            If params IsNot Nothing Then
                sqlcomm.Parameters.AddRange(params)
            End If

            sqladapt = New SqlDataAdapter(sqlcomm)
            sqladapt.Fill(dtb)

            CloseConnection()
            Return 1
        Catch ex As Exception
            MsgBox(ex.Message)
            Return -2
        End Try
    End Function

    Public Sub selectQuery(ByVal query As String, ByRef dtb As DataTable)
        Try
            OpenConnection()

            Dim sqladapt As SqlDataAdapter
            Dim sqlcomm As New SqlCommand
            sqlcomm.Connection = _dbConn
            sqlcomm.CommandType = CommandType.Text
            sqlcomm.CommandText = query
            sqladapt = New SqlDataAdapter(sqlcomm)
            sqladapt.Fill(dtb)

            CloseConnection()

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Public Sub updateForm(ByVal spname As String, ByRef rows As Integer, ByVal ParamArray params() As SqlParameter)
        Try
            OpenConnection()

            Dim sqlcomm As New SqlCommand
            sqlcomm.Connection = _dbConn
            sqlcomm.CommandType = CommandType.StoredProcedure
            sqlcomm.CommandText = spname
            sqlcomm.Parameters.AddRange(params)
            rows = sqlcomm.ExecuteNonQuery

            CloseConnection()

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Public Function executeStoredProcedure(ByVal spname As String, ByVal ParamArray params() As SqlParameter) As Integer
        Try
            Dim r As Integer

            OpenConnection()

            Dim sqlcomm As New SqlCommand
            sqlcomm.Connection = _dbConn
            sqlcomm.CommandType = CommandType.StoredProcedure
            sqlcomm.CommandText = spname

            If params IsNot Nothing Then
                sqlcomm.Parameters.AddRange(params)
            End If
            r = sqlcomm.ExecuteNonQuery()

            CloseConnection()
            Return r
        Catch ex As Exception
            MsgBox(ex.Message)
            Return -2
        End Try
    End Function
End Class
