﻿Imports System.Data.SqlClient

Public Class Form
    Inherits DataAccessControl

    Private _GovId As Integer
    Private _GovName As String
    Private _vrcId As Integer
    Private _vrcName As String
    Private _found As Boolean
    Private _barcode As Long
    Private _rows As Integer
    Private _voterId As Integer
    Private _comment As String
    Private _match As Integer
    Private _isAdmin As Boolean
    Private _name As String
    Private _father As String
    Private _grandFather As String
    Private _famNo As String
    Private _YearOB As Integer
    Private _firstName As String
    Private _secondName As String
    Private _thirdName As String
    Private _pcId As Integer
    Private _gender As Boolean
    Private _type As String
    Private _regType As Integer
    Private _puid As Integer

    Public Sub New()
        MyBase.New()
        _GovId = 0
        _vrcId = 0
        _found = False
        _famNo = 0
        _firstName = ""
        _secondName = ""
        _thirdName = ""
        _YearOB = 0
    End Sub

    Public Property Type As String
        Get
            Return _type

        End Get
        Set(value As String)
            _type = value
        End Set
    End Property

    Public Property RegType As Integer
        Get
            Return _regType

        End Get
        Set(value As Integer)
            _regType = value
        End Set
    End Property

    Public Property PUID As Integer
        Get
            Return _puid

        End Get
        Set(value As Integer)
            _puid = value
        End Set
    End Property

    Public Property PCID As Integer
        Get
            Return _pcId

        End Get
        Set(value As Integer)
            _pcId = value

        End Set
    End Property

    Public Property Gender As Boolean
        Get
            Return _gender

        End Get
        Set(value As Boolean)
            _gender = value
        End Set
    End Property

    Public Property Found As Boolean
        Get
            Return _found
        End Get
        Set(ByVal value As Boolean)
            _found = value
        End Set
    End Property

    Public Property GovernorateId As Integer
        Get
            Return _GovId
        End Get
        Set(ByVal value As Integer)
            _GovId = value

        End Set
    End Property

    Public Property VoterId As Integer
        Get
            Return _voterId
        End Get
        Set(ByVal value As Integer)
            _voterId = value

        End Set
    End Property

    Public Property VRCID As Integer
        Get
            Return _vrcId
        End Get
        Set(value As Integer)
            _vrcId = value
        End Set
    End Property

    Public Property FamilyId As String
        Get
            Return _famNo
        End Get
        Set(value As String)
            _famNo = value
        End Set
    End Property


    Public Property FirstName As String
        Get
            Return _firstName
        End Get
        Set(value As String)
            _firstName = value
        End Set
    End Property

    Public Property SecondName As String
        Get
            Return _secondName
        End Get
        Set(value As String)
            _secondName = value
        End Set
    End Property

    Public Property ThirdName As String
        Get
            Return _thirdName
        End Get
        Set(value As String)
            _thirdName = value
        End Set
    End Property

    Public Property YearOfBirth As Integer
        Get
            Return _YearOB

        End Get
        Set(value As Integer)
            _YearOB = value
        End Set
    End Property

    Public Function Search() As DataTable
        Try
            Dim parmarray() As SqlParameter
            Dim dtResult As New DataTable
            ReDim parmarray(-1)

            If VoterId <> 0 Then
                parmarray = {newparameter("@PER_ID", SqlDbType.BigInt, _voterId)}
            ElseIf _vrcId <> 0 And _famNo <> 0 Then
                parmarray = {newparameter("@FamNo", SqlDbType.Int, _famNo), newparameter("@VRCID", SqlDbType.SmallInt, _vrcId)}
            ElseIf _GovId <> 0 And _firstName <> "" And _secondName <> "" And _thirdName <> "" And _YearOB <> 0 Then
                parmarray = {newparameter("@GOV_MOT_ID", SqlDbType.TinyInt, _GovId), newparameter("@PER_FIRST", SqlDbType.VarChar, _firstName), newparameter("@PER_FATHER", SqlDbType.VarChar, _secondName), newparameter("@PER_GRAND", SqlDbType.VarChar, _thirdName), newparameter("@PER_DOB", SqlDbType.Int, _YearOB)}
            End If

            searchForm("S_Person", dtResult, parmarray)

            If dtResult.Rows.Count = 0 Then
                _found = False
                Return Nothing
            Else
                _found = True
                Return dtResult
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try

    End Function

    Public Function getGovernorates() As DataTable
        Try
            Dim dtResult As New DataTable
            selectQuery("select GOV_MOT_ID, GOV_NAME_AR from [NewVRU].[dbo].[Governorate]", dtResult)

            If dtResult.Rows.Count = 0 Then
                _found = False
                Return Nothing
            Else
                _found = True
                Return dtResult
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try

    End Function

    Public Function getLastPerId() As Integer
        Try
            Dim dtTable As New DataTable

            If searchForm("S_GetLastPersonId", dtTable, Nothing) <> 0 Then
                Return dtTable.Rows(0)(0) + 1
            End If
            Return 0
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try

    End Function

    Public Function addPerson() As Integer
        Try
            Dim Params(8) As SqlParameter

            Params = {newparameter("@PER_ID", SqlDbType.Int, _voterId), newparameter("@PER_PUID", SqlDbType.Int, _puid), newparameter("@PER_FAMID", SqlDbType.VarChar, _famNo), newparameter("@PER_FIRST", SqlDbType.VarChar, _firstName), newparameter("@PER_FATHER", SqlDbType.VarChar, _secondName), newparameter("@PER_GRAND", SqlDbType.VarChar, _thirdName), newparameter("@PER_DOB", SqlDbType.SmallInt, _YearOB), newparameter("@PER_GENDER", SqlDbType.Bit, _gender), newparameter("@PER_OGOVID", SqlDbType.Int, _GovId), newparameter("@PER_PCID", SqlDbType.Int, _pcId), newparameter("@PER_REGTYPE", SqlDbType.Int, _regType)}
          
            Return executeStoredProcedure("Add_Person", Params)
        Catch ex As Exception
            MsgBox(ex.Message)
            Return -2
        End Try

    End Function

    Public Function updatePerson() As Integer
        Try
            Dim params() As SqlParameter
            ReDim params(-1)

            If _type = "CHG" Or _type = "DEL" Or _type = "IDP" Then
                ReDim params(2)
                params = {newparameter("@PER_ID", SqlDbType.Int, _voterId), newparameter("@TYPE", SqlDbType.VarChar, _type), newparameter("@REG_TYPE", SqlDbType.Int, _regType)}
            ElseIf _type = "CHC" Or _type = "COR" Then
                ReDim params(6)
                params = {newparameter("@PER_ID", SqlDbType.Int, _voterId), newparameter("@TYPE", SqlDbType.VarChar, _type), newparameter("@REG_TYPE", SqlDbType.Int, _regType), newparameter("@PER_GRAND", SqlDbType.VarChar, _thirdName), newparameter("@PER_FIRST", SqlDbType.VarChar, _firstName), newparameter("@PER_FATHER", SqlDbType.VarChar, _secondName), newparameter("@PER_DOB", SqlDbType.SmallInt, _YearOB)}
            End If
            Return executeStoredProcedure("Update_Person", params)

        Catch ex As Exception
            MsgBox(ex.Message)
            Return -2
        End Try
    End Function

End Class
