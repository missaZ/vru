﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration

Public Enum SqlConnectionOwnership
    ''' <summary>Connection is owned and managed by SqlHelper</summary>
    Internal
    ''' <summary>Connection is owned and managed by the caller</summary>
    External
End Enum

Public Class DataAccessLayer
    Private _dbConn As SqlConnection
    Private _connectionString As String

    Public ReadOnly Property CONNECTION_STRING
        Get
            Return _connectionString
        End Get
    End Property

    Public ReadOnly Property DB_CONN As SqlConnection
        Get
            Return _dbConn
        End Get
    End Property

    Public Sub New()
        _connectionString = ConfigurationManager.ConnectionStrings("ConnectionVRU").ConnectionString
        _dbConn = New SqlConnection(_connectionString)
    End Sub

    Protected Sub OpenConnection()
        If Not _dbConn.State = ConnectionState.Open Then
            _dbConn.Open()
        End If
    End Sub

    Protected Sub CloseConnection()
        If Not _dbConn.State = ConnectionState.Closed Then
            _dbConn.Close()
        End If
    End Sub

    Protected Function newparameter(ByVal name As String, ByVal type As SqlDbType, ByVal value As Object) As SqlParameter
        Dim param As New SqlParameter(name, type)
        param.Value = value
        Return param
    End Function

    Protected Function ExecuteReader(transaction As SqlTransaction, commandType As CommandType, commandText As String, commandParameters As SqlParameter(), connectionOwnership As SqlConnectionOwnership) As SqlDataReader
        If _dbConn Is Nothing Then
            Throw New ArgumentNullException("connection")
        End If

        Dim mustCloseConnection As Boolean = False
        ' Create a command and prepare it for execution
        Dim cmd As New SqlCommand()
        Try
            PrepareCommand(cmd, transaction, commandType, commandText, commandParameters, _
                mustCloseConnection)

            Dim dataReader As SqlDataReader

            If connectionOwnership = SqlConnectionOwnership.External Then
                dataReader = cmd.ExecuteReader()
            Else
                dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            End If

            Dim canClear As Boolean = True
            For Each commandParameter As SqlParameter In cmd.Parameters
                If commandParameter.Direction <> ParameterDirection.Input Then
                    canClear = False
                End If
            Next

            If canClear Then
                cmd.Parameters.Clear()
            End If

            Return dataReader
        Catch
            If mustCloseConnection Then
                _dbConn.Close()
            End If
            Throw
        End Try
    End Function

    Protected Function ExecuteReader(commandType As CommandType, commandText As String) As SqlDataReader
        ' Pass through the call providing null for the set of SqlParameters
        Return ExecuteReader(commandType, commandText, DirectCast(Nothing, SqlParameter()))
    End Function

    Protected Function ExecuteReader(commandType As CommandType, commandText As String, ParamArray commandParameters As SqlParameter()) As SqlDataReader
        If _connectionString Is Nothing OrElse _connectionString.Length = 0 Then
            Throw New ArgumentNullException("connectionString")
        End If
        'Dim connection As SqlConnection = Nothing
        Try
            'connection = New SqlConnection(_connectionString)
            'connection.Open()
            OpenConnection()

            ' Call the private overload that takes an internally owned connection in place of the connection string
            Return ExecuteReader(Nothing, commandType, commandText, commandParameters, SqlConnectionOwnership.Internal)
        Catch
            ' If we fail to return the SqlDatReader, we need to close the connection ourselves
            If _dbConn IsNot Nothing Then
                CloseConnection()
            End If
            Throw
        End Try

    End Function

    Protected Function ExecuteReader(connection As SqlConnection, commandType As CommandType, commandText As String) As SqlDataReader
        ' Pass through the call providing null for the set of SqlParameters
        Return ExecuteReader(connection, commandType, commandText, DirectCast(Nothing, SqlParameter()))
    End Function

    Protected Function ExecuteReader(connection As SqlConnection, commandType As CommandType, commandText As String, ParamArray commandParameters As SqlParameter()) As SqlDataReader
        ' Pass through the call to the private overload using a null transaction value and an externally owned connection
        Return ExecuteReader(DirectCast(Nothing, SqlTransaction), commandType, commandText, commandParameters, SqlConnectionOwnership.External)
    End Function

    Protected Function ExecuteReader(transaction As SqlTransaction, commandType As CommandType, commandText As String) As SqlDataReader
        ' Pass through the call providing null for the set of SqlParameters
        Return ExecuteReader(transaction, commandType, commandText, DirectCast(Nothing, SqlParameter()))
    End Function

    Protected Function ExecuteReader(transaction As SqlTransaction, commandType As CommandType, commandText As String, ParamArray commandParameters As SqlParameter()) As SqlDataReader
        If transaction Is Nothing Then
            Throw New ArgumentNullException("transaction")
        End If
        If transaction IsNot Nothing AndAlso transaction.Connection Is Nothing Then
            Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
        End If

        ' Pass through to private overload, indicating that the connection is owned by the caller
        Return ExecuteReader(transaction, commandType, commandText, commandParameters, SqlConnectionOwnership.External)
    End Function

    Protected Sub PrepareCommand(command As SqlCommand, transaction As SqlTransaction, commandType As CommandType, commandText As String, commandParameters As SqlParameter(), _
            mustCloseConnection As Boolean)
        If command Is Nothing Then
            Throw New ArgumentNullException("command")
        End If
        If commandText Is Nothing OrElse commandText.Length = 0 Then
            Throw New ArgumentNullException("commandText")
        End If

        ' If the provided connection is not open, we will open it
        If _dbConn.State <> ConnectionState.Open Then
            mustCloseConnection = True
            _dbConn.Open()
        Else
            mustCloseConnection = False
        End If

        ' Associate the connection with the command
        command.Connection = _dbConn

        ' Set the command text (stored procedure name or SQL statement)
        command.CommandText = commandText

        ' If we were provided a transaction, assign it
        If transaction IsNot Nothing Then
            If transaction.Connection Is Nothing Then
                Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            End If
            command.Transaction = transaction
        End If

        ' Set the command type
        command.CommandType = commandType

        ' Attach the command parameters if they are provided
        If commandParameters IsNot Nothing Then
            AttachParameters(command, commandParameters)
        End If
        Return
    End Sub

    Protected Sub AttachParameters(command As SqlCommand, commandParameters As SqlParameter())
        If command Is Nothing Then
            Throw New ArgumentNullException("command")
        End If
        If commandParameters IsNot Nothing Then
            For Each p As SqlParameter In commandParameters
                If p IsNot Nothing Then
                    ' Check for derived output value with no value assigned
                    If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso (p.Value Is Nothing) Then
                        p.Value = DBNull.Value
                    End If
                    command.Parameters.Add(p)
                End If
            Next
        End If
    End Sub

    Protected Function ExecuteNonQuery(commandType As CommandType, commandText As String, ParamArray commandParameters As SqlParameter()) As Integer
        If _dbConn Is Nothing Then
            Throw New ArgumentNullException("connection")
        End If

        ' Create a command and prepare it for execution
        Dim cmd As New SqlCommand()
        Dim mustCloseConnection As Boolean = False
        PrepareCommand(cmd, DirectCast(Nothing, SqlTransaction), commandType, commandText, commandParameters, _
            mustCloseConnection)

        ' Finally, execute the command
        Dim retval As Integer = cmd.ExecuteNonQuery()

        ' Detach the SqlParameters from the command object, so they can be used again
        cmd.Parameters.Clear()
        If mustCloseConnection Then
            _dbConn.Close()
        End If
        Return retval
    End Function
End Class
