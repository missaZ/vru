﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPhoto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnGrab = New System.Windows.Forms.Button()
        Me.btnReCampture = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(12, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(260, 179)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'btnGrab
        '
        Me.btnGrab.Location = New System.Drawing.Point(106, 227)
        Me.btnGrab.Name = "btnGrab"
        Me.btnGrab.Size = New System.Drawing.Size(75, 23)
        Me.btnGrab.TabIndex = 1
        Me.btnGrab.Text = "Grab"
        Me.btnGrab.UseVisualStyleBackColor = True
        '
        'btnReCampture
        '
        Me.btnReCampture.Location = New System.Drawing.Point(12, 227)
        Me.btnReCampture.Name = "btnReCampture"
        Me.btnReCampture.Size = New System.Drawing.Size(75, 23)
        Me.btnReCampture.TabIndex = 3
        Me.btnReCampture.Text = "Re Capture"
        Me.btnReCampture.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(197, 227)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "OK"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmPhoto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnReCampture)
        Me.Controls.Add(Me.btnGrab)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "frmPhoto"
        Me.Text = "Photo"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnGrab As System.Windows.Forms.Button
    Friend WithEvents btnReCampture As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button

End Class
