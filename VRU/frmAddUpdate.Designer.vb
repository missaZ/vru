﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddUpdate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GOV_MOT_ID = New System.Windows.Forms.TextBox()
        Me.GOV_NAME_AR = New System.Windows.Forms.TextBox()
        Me.PER_ID = New System.Windows.Forms.TextBox()
        Me.PER_FIRST = New System.Windows.Forms.TextBox()
        Me.PER_FATHER = New System.Windows.Forms.TextBox()
        Me.PER_GRAND = New System.Windows.Forms.TextBox()
        Me.PER_HEAD = New System.Windows.Forms.TextBox()
        Me.PER_HEADFATHER = New System.Windows.Forms.TextBox()
        Me.PER_HEADGRAND = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.PER_GENDER = New System.Windows.Forms.RadioButton()
        Me.PER_GENDERM = New System.Windows.Forms.RadioButton()
        Me.PER_DOB = New System.Windows.Forms.TextBox()
        Me.PER_PUID = New System.Windows.Forms.TextBox()
        Me.PC_ID = New System.Windows.Forms.TextBox()
        Me.PC_NAME = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTNO1 = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTISSUEPLACE1 = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTNO2 = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTISSUEPLACE2 = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.ADD = New System.Windows.Forms.CheckBox()
        Me.PER_OLDVRC = New System.Windows.Forms.TextBox()
        Me.PER_OLDFRC = New System.Windows.Forms.TextBox()
        Me.CHG = New System.Windows.Forms.CheckBox()
        Me.COR = New System.Windows.Forms.CheckBox()
        Me.CHK_VOTERNAME = New System.Windows.Forms.CheckBox()
        Me.CHK_FATHERNAME = New System.Windows.Forms.CheckBox()
        Me.CHK_GRANDNAME = New System.Windows.Forms.CheckBox()
        Me.CHK_DOB = New System.Windows.Forms.CheckBox()
        Me.DEL = New System.Windows.Forms.CheckBox()
        Me.IDP = New System.Windows.Forms.CheckBox()
        Me.PER_DELETEDOCUMENTNO = New System.Windows.Forms.TextBox()
        Me.PER_DELETEDOCUMENTISSUEPALCE = New System.Windows.Forms.TextBox()
        Me.PER_ORGGOVID = New System.Windows.Forms.TextBox()
        Me.PER_ORGGOVNAME = New System.Windows.Forms.TextBox()
        Me.PER_IDPVRCID = New System.Windows.Forms.TextBox()
        Me.PER_IDPFRCID = New System.Windows.Forms.TextBox()
        Me.PER_IDPDOCUMENTNO = New System.Windows.Forms.TextBox()
        Me.PER_IDPDOCUMENTISSUEPLACE = New System.Windows.Forms.TextBox()
        Me.PER_IDPDATE = New System.Windows.Forms.TextBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ABS = New System.Windows.Forms.RadioButton()
        Me.SPECIAL = New System.Windows.Forms.RadioButton()
        Me.REGULAR = New System.Windows.Forms.RadioButton()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.PER_DOCUMENTISSUEDATE1 = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTISSUEDATE2 = New System.Windows.Forms.TextBox()
        Me.PER_DELETEDOCUMENTISSUEDATE = New System.Windows.Forms.TextBox()
        Me.PER_IDPDOCUMENTISSUEDATE = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.CHGDocumentIssueDate = New System.Windows.Forms.TextBox()
        Me.CHGDocumentIssuePlace = New System.Windows.Forms.TextBox()
        Me.CHGDocumentNumber = New System.Windows.Forms.TextBox()
        Me.VRC_OID = New System.Windows.Forms.TextBox()
        Me.VRC_NAME_AR = New System.Windows.Forms.TextBox()
        Me.PER_FAMNO = New System.Windows.Forms.TextBox()
        Me.PER_DOCUMENTTYPE1 = New System.Windows.Forms.ComboBox()
        Me.PER_DOCUMENTTYPE2 = New System.Windows.Forms.ComboBox()
        Me.CHGDocumentType = New System.Windows.Forms.ComboBox()
        Me.PER_DELETEDOCUMENTTYPE = New System.Windows.Forms.ComboBox()
        Me.PER_IDPDOCUMENTTYPE = New System.Windows.Forms.ComboBox()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.PER_IMAGE = New System.Windows.Forms.PictureBox()
        Me.PictureBox73 = New System.Windows.Forms.PictureBox()
        Me.PictureBox70 = New System.Windows.Forms.PictureBox()
        Me.PictureBox68 = New System.Windows.Forms.PictureBox()
        Me.PictureBox67 = New System.Windows.Forms.PictureBox()
        Me.PictureBox66 = New System.Windows.Forms.PictureBox()
        Me.PictureBox65 = New System.Windows.Forms.PictureBox()
        Me.PictureBox64 = New System.Windows.Forms.PictureBox()
        Me.PictureBox62 = New System.Windows.Forms.PictureBox()
        Me.PictureBox61 = New System.Windows.Forms.PictureBox()
        Me.PictureBox60 = New System.Windows.Forms.PictureBox()
        Me.PictureBox59 = New System.Windows.Forms.PictureBox()
        Me.PictureBox58 = New System.Windows.Forms.PictureBox()
        Me.PictureBox57 = New System.Windows.Forms.PictureBox()
        Me.PictureBox56 = New System.Windows.Forms.PictureBox()
        Me.PictureBox55 = New System.Windows.Forms.PictureBox()
        Me.PictureBox54 = New System.Windows.Forms.PictureBox()
        Me.PictureBox53 = New System.Windows.Forms.PictureBox()
        Me.PictureBox52 = New System.Windows.Forms.PictureBox()
        Me.PictureBox51 = New System.Windows.Forms.PictureBox()
        Me.PictureBox50 = New System.Windows.Forms.PictureBox()
        Me.PictureBox49 = New System.Windows.Forms.PictureBox()
        Me.PictureBox48 = New System.Windows.Forms.PictureBox()
        Me.PictureBox47 = New System.Windows.Forms.PictureBox()
        Me.PictureBox46 = New System.Windows.Forms.PictureBox()
        Me.PictureBox45 = New System.Windows.Forms.PictureBox()
        Me.PictureBox44 = New System.Windows.Forms.PictureBox()
        Me.PictureBox43 = New System.Windows.Forms.PictureBox()
        Me.PictureBox36 = New System.Windows.Forms.PictureBox()
        Me.PictureBox34 = New System.Windows.Forms.PictureBox()
        Me.PictureBox33 = New System.Windows.Forms.PictureBox()
        Me.PictureBox32 = New System.Windows.Forms.PictureBox()
        Me.PictureBox31 = New System.Windows.Forms.PictureBox()
        Me.PictureBox30 = New System.Windows.Forms.PictureBox()
        Me.PictureBox29 = New System.Windows.Forms.PictureBox()
        Me.PictureBox28 = New System.Windows.Forms.PictureBox()
        Me.PictureBox27 = New System.Windows.Forms.PictureBox()
        Me.PictureBox26 = New System.Windows.Forms.PictureBox()
        Me.PictureBox25 = New System.Windows.Forms.PictureBox()
        Me.PictureBox24 = New System.Windows.Forms.PictureBox()
        Me.PictureBox23 = New System.Windows.Forms.PictureBox()
        Me.PictureBox22 = New System.Windows.Forms.PictureBox()
        Me.PictureBox21 = New System.Windows.Forms.PictureBox()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox35 = New System.Windows.Forms.PictureBox()
        Me.PictureBox37 = New System.Windows.Forms.PictureBox()
        Me.PictureBox38 = New System.Windows.Forms.PictureBox()
        Me.PictureBox39 = New System.Windows.Forms.PictureBox()
        Me.PictureBox40 = New System.Windows.Forms.PictureBox()
        Me.PictureBox41 = New System.Windows.Forms.PictureBox()
        Me.PictureBox42 = New System.Windows.Forms.PictureBox()
        Me.PictureBox63 = New System.Windows.Forms.PictureBox()
        Me.PictureBox69 = New System.Windows.Forms.PictureBox()
        Me.PictureBox71 = New System.Windows.Forms.PictureBox()
        Me.PictureBox72 = New System.Windows.Forms.PictureBox()
        Me.PictureBox74 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PER_IMAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox74, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GOV_MOT_ID
        '
        Me.GOV_MOT_ID.BackColor = System.Drawing.Color.LightCyan
        Me.GOV_MOT_ID.Enabled = False
        Me.GOV_MOT_ID.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GOV_MOT_ID.ForeColor = System.Drawing.Color.Black
        Me.GOV_MOT_ID.Location = New System.Drawing.Point(922, 313)
        Me.GOV_MOT_ID.Multiline = True
        Me.GOV_MOT_ID.Name = "GOV_MOT_ID"
        Me.GOV_MOT_ID.Size = New System.Drawing.Size(124, 34)
        Me.GOV_MOT_ID.TabIndex = 100000
        Me.GOV_MOT_ID.Tag = "const"
        '
        'GOV_NAME_AR
        '
        Me.GOV_NAME_AR.BackColor = System.Drawing.Color.LightCyan
        Me.GOV_NAME_AR.Enabled = False
        Me.GOV_NAME_AR.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GOV_NAME_AR.ForeColor = System.Drawing.Color.Black
        Me.GOV_NAME_AR.Location = New System.Drawing.Point(649, 313)
        Me.GOV_NAME_AR.Multiline = True
        Me.GOV_NAME_AR.Name = "GOV_NAME_AR"
        Me.GOV_NAME_AR.Size = New System.Drawing.Size(267, 34)
        Me.GOV_NAME_AR.TabIndex = 0
        Me.GOV_NAME_AR.Tag = "const"
        '
        'PER_ID
        '
        Me.PER_ID.BackColor = System.Drawing.Color.LightCyan
        Me.PER_ID.Enabled = False
        Me.PER_ID.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_ID.ForeColor = System.Drawing.Color.Black
        Me.PER_ID.Location = New System.Drawing.Point(649, 369)
        Me.PER_ID.Multiline = True
        Me.PER_ID.Name = "PER_ID"
        Me.PER_ID.Size = New System.Drawing.Size(336, 38)
        Me.PER_ID.TabIndex = 3
        Me.PER_ID.Tag = ""
        '
        'PER_FIRST
        '
        Me.PER_FIRST.Enabled = False
        Me.PER_FIRST.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_FIRST.ForeColor = System.Drawing.Color.Black
        Me.PER_FIRST.Location = New System.Drawing.Point(649, 460)
        Me.PER_FIRST.Multiline = True
        Me.PER_FIRST.Name = "PER_FIRST"
        Me.PER_FIRST.Size = New System.Drawing.Size(479, 32)
        Me.PER_FIRST.TabIndex = 5
        Me.PER_FIRST.Tag = "addition"
        '
        'PER_FATHER
        '
        Me.PER_FATHER.Enabled = False
        Me.PER_FATHER.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_FATHER.ForeColor = System.Drawing.Color.Black
        Me.PER_FATHER.Location = New System.Drawing.Point(649, 536)
        Me.PER_FATHER.Multiline = True
        Me.PER_FATHER.Name = "PER_FATHER"
        Me.PER_FATHER.Size = New System.Drawing.Size(479, 32)
        Me.PER_FATHER.TabIndex = 6
        Me.PER_FATHER.Tag = "addition"
        '
        'PER_GRAND
        '
        Me.PER_GRAND.Enabled = False
        Me.PER_GRAND.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_GRAND.ForeColor = System.Drawing.Color.Black
        Me.PER_GRAND.Location = New System.Drawing.Point(649, 615)
        Me.PER_GRAND.Multiline = True
        Me.PER_GRAND.Name = "PER_GRAND"
        Me.PER_GRAND.Size = New System.Drawing.Size(479, 32)
        Me.PER_GRAND.TabIndex = 7
        Me.PER_GRAND.Tag = "addition"
        '
        'PER_HEAD
        '
        Me.PER_HEAD.Enabled = False
        Me.PER_HEAD.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_HEAD.ForeColor = System.Drawing.Color.Black
        Me.PER_HEAD.Location = New System.Drawing.Point(13, 460)
        Me.PER_HEAD.Multiline = True
        Me.PER_HEAD.Name = "PER_HEAD"
        Me.PER_HEAD.Size = New System.Drawing.Size(503, 32)
        Me.PER_HEAD.TabIndex = 8
        Me.PER_HEAD.Tag = "addition"
        '
        'PER_HEADFATHER
        '
        Me.PER_HEADFATHER.Enabled = False
        Me.PER_HEADFATHER.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_HEADFATHER.ForeColor = System.Drawing.Color.Black
        Me.PER_HEADFATHER.Location = New System.Drawing.Point(13, 536)
        Me.PER_HEADFATHER.Multiline = True
        Me.PER_HEADFATHER.Name = "PER_HEADFATHER"
        Me.PER_HEADFATHER.Size = New System.Drawing.Size(503, 32)
        Me.PER_HEADFATHER.TabIndex = 9
        Me.PER_HEADFATHER.Tag = "addition"
        '
        'PER_HEADGRAND
        '
        Me.PER_HEADGRAND.Enabled = False
        Me.PER_HEADGRAND.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_HEADGRAND.ForeColor = System.Drawing.Color.Black
        Me.PER_HEADGRAND.Location = New System.Drawing.Point(13, 615)
        Me.PER_HEADGRAND.Multiline = True
        Me.PER_HEADGRAND.Name = "PER_HEADGRAND"
        Me.PER_HEADGRAND.Size = New System.Drawing.Size(503, 32)
        Me.PER_HEADGRAND.TabIndex = 10
        Me.PER_HEADGRAND.Tag = "addition"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.PER_GENDER)
        Me.GroupBox1.Controls.Add(Me.PER_GENDERM)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Location = New System.Drawing.Point(847, 687)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(184, 29)
        Me.GroupBox1.TabIndex = 33
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = "addition"
        '
        'PER_GENDER
        '
        Me.PER_GENDER.AutoSize = True
        Me.PER_GENDER.Location = New System.Drawing.Point(28, 10)
        Me.PER_GENDER.Name = "PER_GENDER"
        Me.PER_GENDER.Size = New System.Drawing.Size(14, 13)
        Me.PER_GENDER.TabIndex = 1
        Me.PER_GENDER.TabStop = True
        Me.PER_GENDER.UseVisualStyleBackColor = True
        '
        'PER_GENDERM
        '
        Me.PER_GENDERM.AutoSize = True
        Me.PER_GENDERM.Location = New System.Drawing.Point(124, 12)
        Me.PER_GENDERM.Name = "PER_GENDERM"
        Me.PER_GENDERM.Size = New System.Drawing.Size(14, 13)
        Me.PER_GENDERM.TabIndex = 0
        Me.PER_GENDERM.TabStop = True
        Me.PER_GENDERM.UseVisualStyleBackColor = True
        '
        'PER_DOB
        '
        Me.PER_DOB.Enabled = False
        Me.PER_DOB.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOB.ForeColor = System.Drawing.Color.Black
        Me.PER_DOB.Location = New System.Drawing.Point(440, 683)
        Me.PER_DOB.Multiline = True
        Me.PER_DOB.Name = "PER_DOB"
        Me.PER_DOB.Size = New System.Drawing.Size(272, 33)
        Me.PER_DOB.TabIndex = 11
        Me.PER_DOB.Tag = "addition"
        '
        'PER_PUID
        '
        Me.PER_PUID.Enabled = False
        Me.PER_PUID.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_PUID.ForeColor = System.Drawing.Color.Black
        Me.PER_PUID.Location = New System.Drawing.Point(13, 668)
        Me.PER_PUID.MaxLength = 5
        Me.PER_PUID.Multiline = True
        Me.PER_PUID.Name = "PER_PUID"
        Me.PER_PUID.Size = New System.Drawing.Size(250, 37)
        Me.PER_PUID.TabIndex = 12
        Me.PER_PUID.Tag = "addition"
        '
        'PC_ID
        '
        Me.PC_ID.Enabled = False
        Me.PC_ID.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PC_ID.ForeColor = System.Drawing.Color.Black
        Me.PC_ID.Location = New System.Drawing.Point(757, 730)
        Me.PC_ID.MaxLength = 6
        Me.PC_ID.Multiline = True
        Me.PC_ID.Name = "PC_ID"
        Me.PC_ID.Size = New System.Drawing.Size(242, 38)
        Me.PC_ID.TabIndex = 13
        Me.PC_ID.Tag = "addition"
        '
        'PC_NAME
        '
        Me.PC_NAME.Enabled = False
        Me.PC_NAME.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PC_NAME.ForeColor = System.Drawing.Color.Black
        Me.PC_NAME.Location = New System.Drawing.Point(13, 730)
        Me.PC_NAME.Multiline = True
        Me.PC_NAME.Name = "PC_NAME"
        Me.PC_NAME.Size = New System.Drawing.Size(537, 38)
        Me.PC_NAME.TabIndex = 14
        Me.PC_NAME.Tag = "addition"
        '
        'PER_DOCUMENTNO1
        '
        Me.PER_DOCUMENTNO1.Enabled = False
        Me.PER_DOCUMENTNO1.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTNO1.ForeColor = System.Drawing.Color.Black
        Me.PER_DOCUMENTNO1.Location = New System.Drawing.Point(374, 825)
        Me.PER_DOCUMENTNO1.Multiline = True
        Me.PER_DOCUMENTNO1.Name = "PER_DOCUMENTNO1"
        Me.PER_DOCUMENTNO1.Size = New System.Drawing.Size(176, 33)
        Me.PER_DOCUMENTNO1.TabIndex = 16
        Me.PER_DOCUMENTNO1.Tag = "addition"
        '
        'PER_DOCUMENTISSUEPLACE1
        '
        Me.PER_DOCUMENTISSUEPLACE1.Enabled = False
        Me.PER_DOCUMENTISSUEPLACE1.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTISSUEPLACE1.ForeColor = System.Drawing.Color.Black
        Me.PER_DOCUMENTISSUEPLACE1.Location = New System.Drawing.Point(192, 825)
        Me.PER_DOCUMENTISSUEPLACE1.Multiline = True
        Me.PER_DOCUMENTISSUEPLACE1.Name = "PER_DOCUMENTISSUEPLACE1"
        Me.PER_DOCUMENTISSUEPLACE1.Size = New System.Drawing.Size(176, 33)
        Me.PER_DOCUMENTISSUEPLACE1.TabIndex = 17
        Me.PER_DOCUMENTISSUEPLACE1.Tag = "addition"
        '
        'PER_DOCUMENTNO2
        '
        Me.PER_DOCUMENTNO2.Enabled = False
        Me.PER_DOCUMENTNO2.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTNO2.ForeColor = System.Drawing.Color.Black
        Me.PER_DOCUMENTNO2.Location = New System.Drawing.Point(374, 864)
        Me.PER_DOCUMENTNO2.Multiline = True
        Me.PER_DOCUMENTNO2.Name = "PER_DOCUMENTNO2"
        Me.PER_DOCUMENTNO2.Size = New System.Drawing.Size(176, 32)
        Me.PER_DOCUMENTNO2.TabIndex = 20
        Me.PER_DOCUMENTNO2.Tag = "addition"
        '
        'PER_DOCUMENTISSUEPLACE2
        '
        Me.PER_DOCUMENTISSUEPLACE2.Enabled = False
        Me.PER_DOCUMENTISSUEPLACE2.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTISSUEPLACE2.ForeColor = System.Drawing.Color.Black
        Me.PER_DOCUMENTISSUEPLACE2.Location = New System.Drawing.Point(192, 864)
        Me.PER_DOCUMENTISSUEPLACE2.Multiline = True
        Me.PER_DOCUMENTISSUEPLACE2.Name = "PER_DOCUMENTISSUEPLACE2"
        Me.PER_DOCUMENTISSUEPLACE2.Size = New System.Drawing.Size(176, 32)
        Me.PER_DOCUMENTISSUEPLACE2.TabIndex = 21
        Me.PER_DOCUMENTISSUEPLACE2.Tag = "addition"
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(897, 1722)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(287, 35)
        Me.btnSave.TabIndex = 39
        Me.btnSave.Tag = "other"
        Me.btnSave.Text = "حفظ"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(311, 1722)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(287, 35)
        Me.btnClear.TabIndex = 41
        Me.btnClear.Tag = "other"
        Me.btnClear.Text = "تفريغ الحقول"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'ADD
        '
        Me.ADD.AutoSize = True
        Me.ADD.Location = New System.Drawing.Point(1045, 996)
        Me.ADD.Name = "ADD"
        Me.ADD.Size = New System.Drawing.Size(15, 14)
        Me.ADD.TabIndex = 77
        Me.ADD.Tag = "other"
        Me.ADD.UseVisualStyleBackColor = True
        '
        'PER_OLDVRC
        '
        Me.PER_OLDVRC.Enabled = False
        Me.PER_OLDVRC.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_OLDVRC.ForeColor = System.Drawing.Color.Black
        Me.PER_OLDVRC.Location = New System.Drawing.Point(612, 1073)
        Me.PER_OLDVRC.Multiline = True
        Me.PER_OLDVRC.Name = "PER_OLDVRC"
        Me.PER_OLDVRC.Size = New System.Drawing.Size(174, 44)
        Me.PER_OLDVRC.TabIndex = 23
        Me.PER_OLDVRC.Tag = "change"
        '
        'PER_OLDFRC
        '
        Me.PER_OLDFRC.Enabled = False
        Me.PER_OLDFRC.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_OLDFRC.ForeColor = System.Drawing.Color.Black
        Me.PER_OLDFRC.Location = New System.Drawing.Point(51, 1073)
        Me.PER_OLDFRC.Multiline = True
        Me.PER_OLDFRC.Name = "PER_OLDFRC"
        Me.PER_OLDFRC.Size = New System.Drawing.Size(183, 44)
        Me.PER_OLDFRC.TabIndex = 24
        Me.PER_OLDFRC.Tag = "change"
        '
        'CHG
        '
        Me.CHG.AutoSize = True
        Me.CHG.Location = New System.Drawing.Point(1045, 1082)
        Me.CHG.Name = "CHG"
        Me.CHG.Size = New System.Drawing.Size(15, 14)
        Me.CHG.TabIndex = 85
        Me.CHG.Tag = "other"
        Me.CHG.UseVisualStyleBackColor = True
        '
        'COR
        '
        Me.COR.AutoSize = True
        Me.COR.Location = New System.Drawing.Point(1045, 1185)
        Me.COR.Name = "COR"
        Me.COR.Size = New System.Drawing.Size(15, 14)
        Me.COR.TabIndex = 86
        Me.COR.Tag = "other"
        Me.COR.UseVisualStyleBackColor = True
        '
        'CHK_VOTERNAME
        '
        Me.CHK_VOTERNAME.AutoSize = True
        Me.CHK_VOTERNAME.Enabled = False
        Me.CHK_VOTERNAME.Location = New System.Drawing.Point(608, 1185)
        Me.CHK_VOTERNAME.Name = "CHK_VOTERNAME"
        Me.CHK_VOTERNAME.Size = New System.Drawing.Size(15, 14)
        Me.CHK_VOTERNAME.TabIndex = 89
        Me.CHK_VOTERNAME.Tag = "correction"
        Me.CHK_VOTERNAME.UseVisualStyleBackColor = True
        '
        'CHK_FATHERNAME
        '
        Me.CHK_FATHERNAME.AutoSize = True
        Me.CHK_FATHERNAME.Enabled = False
        Me.CHK_FATHERNAME.Location = New System.Drawing.Point(463, 1185)
        Me.CHK_FATHERNAME.Name = "CHK_FATHERNAME"
        Me.CHK_FATHERNAME.Size = New System.Drawing.Size(15, 14)
        Me.CHK_FATHERNAME.TabIndex = 94
        Me.CHK_FATHERNAME.Tag = "correction"
        Me.CHK_FATHERNAME.UseVisualStyleBackColor = True
        '
        'CHK_GRANDNAME
        '
        Me.CHK_GRANDNAME.AutoSize = True
        Me.CHK_GRANDNAME.Enabled = False
        Me.CHK_GRANDNAME.Location = New System.Drawing.Point(319, 1185)
        Me.CHK_GRANDNAME.Name = "CHK_GRANDNAME"
        Me.CHK_GRANDNAME.Size = New System.Drawing.Size(15, 14)
        Me.CHK_GRANDNAME.TabIndex = 95
        Me.CHK_GRANDNAME.Tag = "correction"
        Me.CHK_GRANDNAME.UseVisualStyleBackColor = True
        '
        'CHK_DOB
        '
        Me.CHK_DOB.AutoSize = True
        Me.CHK_DOB.Enabled = False
        Me.CHK_DOB.Location = New System.Drawing.Point(175, 1185)
        Me.CHK_DOB.Name = "CHK_DOB"
        Me.CHK_DOB.Size = New System.Drawing.Size(15, 14)
        Me.CHK_DOB.TabIndex = 96
        Me.CHK_DOB.Tag = "correction"
        Me.CHK_DOB.UseVisualStyleBackColor = True
        '
        'DEL
        '
        Me.DEL.AutoSize = True
        Me.DEL.Location = New System.Drawing.Point(1045, 1277)
        Me.DEL.Name = "DEL"
        Me.DEL.Size = New System.Drawing.Size(15, 14)
        Me.DEL.TabIndex = 98
        Me.DEL.Tag = "other"
        Me.DEL.UseVisualStyleBackColor = True
        '
        'IDP
        '
        Me.IDP.AutoSize = True
        Me.IDP.Location = New System.Drawing.Point(1045, 1492)
        Me.IDP.Name = "IDP"
        Me.IDP.Size = New System.Drawing.Size(15, 14)
        Me.IDP.TabIndex = 101
        Me.IDP.Tag = "other"
        Me.IDP.UseVisualStyleBackColor = True
        '
        'PER_DELETEDOCUMENTNO
        '
        Me.PER_DELETEDOCUMENTNO.Enabled = False
        Me.PER_DELETEDOCUMENTNO.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DELETEDOCUMENTNO.ForeColor = System.Drawing.Color.Black
        Me.PER_DELETEDOCUMENTNO.Location = New System.Drawing.Point(359, 1285)
        Me.PER_DELETEDOCUMENTNO.Multiline = True
        Me.PER_DELETEDOCUMENTNO.Name = "PER_DELETEDOCUMENTNO"
        Me.PER_DELETEDOCUMENTNO.Size = New System.Drawing.Size(165, 31)
        Me.PER_DELETEDOCUMENTNO.TabIndex = 26
        Me.PER_DELETEDOCUMENTNO.Tag = "delete"
        '
        'PER_DELETEDOCUMENTISSUEPALCE
        '
        Me.PER_DELETEDOCUMENTISSUEPALCE.Enabled = False
        Me.PER_DELETEDOCUMENTISSUEPALCE.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DELETEDOCUMENTISSUEPALCE.ForeColor = System.Drawing.Color.Black
        Me.PER_DELETEDOCUMENTISSUEPALCE.Location = New System.Drawing.Point(188, 1285)
        Me.PER_DELETEDOCUMENTISSUEPALCE.Multiline = True
        Me.PER_DELETEDOCUMENTISSUEPALCE.Name = "PER_DELETEDOCUMENTISSUEPALCE"
        Me.PER_DELETEDOCUMENTISSUEPALCE.Size = New System.Drawing.Size(165, 31)
        Me.PER_DELETEDOCUMENTISSUEPALCE.TabIndex = 27
        Me.PER_DELETEDOCUMENTISSUEPALCE.Tag = "delete"
        '
        'PER_ORGGOVID
        '
        Me.PER_ORGGOVID.Enabled = False
        Me.PER_ORGGOVID.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_ORGGOVID.ForeColor = System.Drawing.Color.Black
        Me.PER_ORGGOVID.Location = New System.Drawing.Point(462, 1373)
        Me.PER_ORGGOVID.Multiline = True
        Me.PER_ORGGOVID.Name = "PER_ORGGOVID"
        Me.PER_ORGGOVID.Size = New System.Drawing.Size(123, 42)
        Me.PER_ORGGOVID.TabIndex = 29
        Me.PER_ORGGOVID.Tag = "idp"
        '
        'PER_ORGGOVNAME
        '
        Me.PER_ORGGOVNAME.Enabled = False
        Me.PER_ORGGOVNAME.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_ORGGOVNAME.ForeColor = System.Drawing.Color.Black
        Me.PER_ORGGOVNAME.Location = New System.Drawing.Point(110, 1374)
        Me.PER_ORGGOVNAME.Multiline = True
        Me.PER_ORGGOVNAME.Name = "PER_ORGGOVNAME"
        Me.PER_ORGGOVNAME.Size = New System.Drawing.Size(313, 41)
        Me.PER_ORGGOVNAME.TabIndex = 30
        Me.PER_ORGGOVNAME.Tag = "idp"
        '
        'PER_IDPVRCID
        '
        Me.PER_IDPVRCID.Enabled = False
        Me.PER_IDPVRCID.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPVRCID.ForeColor = System.Drawing.Color.Black
        Me.PER_IDPVRCID.Location = New System.Drawing.Point(533, 1546)
        Me.PER_IDPVRCID.Multiline = True
        Me.PER_IDPVRCID.Name = "PER_IDPVRCID"
        Me.PER_IDPVRCID.Size = New System.Drawing.Size(155, 43)
        Me.PER_IDPVRCID.TabIndex = 33
        Me.PER_IDPVRCID.Tag = "idp"
        '
        'PER_IDPFRCID
        '
        Me.PER_IDPFRCID.Enabled = False
        Me.PER_IDPFRCID.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPFRCID.ForeColor = System.Drawing.Color.Black
        Me.PER_IDPFRCID.Location = New System.Drawing.Point(110, 1546)
        Me.PER_IDPFRCID.Multiline = True
        Me.PER_IDPFRCID.Name = "PER_IDPFRCID"
        Me.PER_IDPFRCID.Size = New System.Drawing.Size(160, 43)
        Me.PER_IDPFRCID.TabIndex = 34
        Me.PER_IDPFRCID.Tag = "idp"
        '
        'PER_IDPDOCUMENTNO
        '
        Me.PER_IDPDOCUMENTNO.Enabled = False
        Me.PER_IDPDOCUMENTNO.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDOCUMENTNO.ForeColor = System.Drawing.Color.Black
        Me.PER_IDPDOCUMENTNO.Location = New System.Drawing.Point(352, 1658)
        Me.PER_IDPDOCUMENTNO.Multiline = True
        Me.PER_IDPDOCUMENTNO.Name = "PER_IDPDOCUMENTNO"
        Me.PER_IDPDOCUMENTNO.Size = New System.Drawing.Size(174, 33)
        Me.PER_IDPDOCUMENTNO.TabIndex = 36
        Me.PER_IDPDOCUMENTNO.Tag = "idp"
        '
        'PER_IDPDOCUMENTISSUEPLACE
        '
        Me.PER_IDPDOCUMENTISSUEPLACE.Enabled = False
        Me.PER_IDPDOCUMENTISSUEPLACE.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDOCUMENTISSUEPLACE.ForeColor = System.Drawing.Color.Black
        Me.PER_IDPDOCUMENTISSUEPLACE.Location = New System.Drawing.Point(175, 1658)
        Me.PER_IDPDOCUMENTISSUEPLACE.Multiline = True
        Me.PER_IDPDOCUMENTISSUEPLACE.Name = "PER_IDPDOCUMENTISSUEPLACE"
        Me.PER_IDPDOCUMENTISSUEPLACE.Size = New System.Drawing.Size(174, 33)
        Me.PER_IDPDOCUMENTISSUEPLACE.TabIndex = 37
        Me.PER_IDPDOCUMENTISSUEPLACE.Tag = "idp"
        '
        'PER_IDPDATE
        '
        Me.PER_IDPDATE.Enabled = False
        Me.PER_IDPDATE.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDATE.ForeColor = System.Drawing.Color.Black
        Me.PER_IDPDATE.Location = New System.Drawing.Point(115, 1453)
        Me.PER_IDPDATE.Multiline = True
        Me.PER_IDPDATE.Name = "PER_IDPDATE"
        Me.PER_IDPDATE.Size = New System.Drawing.Size(309, 52)
        Me.PER_IDPDATE.TabIndex = 32
        Me.PER_IDPDATE.Tag = "idp"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(9, 1722)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(285, 32)
        Me.btnCancel.TabIndex = 42
        Me.btnCancel.Tag = "other"
        Me.btnCancel.Text = "الغاء"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(604, 1722)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(287, 35)
        Me.btnPrint.TabIndex = 40
        Me.btnPrint.Tag = "other"
        Me.btnPrint.Text = "طباعة"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.LightGray
        Me.GroupBox2.Controls.Add(Me.ABS)
        Me.GroupBox2.Controls.Add(Me.SPECIAL)
        Me.GroupBox2.Controls.Add(Me.REGULAR)
        Me.GroupBox2.Location = New System.Drawing.Point(7, 215)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1176, 42)
        Me.GroupBox2.TabIndex = 248
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Tag = "other"
        '
        'ABS
        '
        Me.ABS.AutoSize = True
        Me.ABS.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ABS.Location = New System.Drawing.Point(678, 19)
        Me.ABS.Name = "ABS"
        Me.ABS.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ABS.Size = New System.Drawing.Size(84, 17)
        Me.ABS.TabIndex = 2
        Me.ABS.TabStop = True
        Me.ABS.Tag = "other"
        Me.ABS.Text = "تصويت مهجرين"
        Me.ABS.UseVisualStyleBackColor = True
        '
        'SPECIAL
        '
        Me.SPECIAL.AutoSize = True
        Me.SPECIAL.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SPECIAL.Location = New System.Drawing.Point(886, 19)
        Me.SPECIAL.Name = "SPECIAL"
        Me.SPECIAL.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.SPECIAL.Size = New System.Drawing.Size(75, 17)
        Me.SPECIAL.TabIndex = 1
        Me.SPECIAL.TabStop = True
        Me.SPECIAL.Tag = "other"
        Me.SPECIAL.Text = "تصويت خاص"
        Me.SPECIAL.UseVisualStyleBackColor = True
        '
        'REGULAR
        '
        Me.REGULAR.AutoSize = True
        Me.REGULAR.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REGULAR.Location = New System.Drawing.Point(1078, 19)
        Me.REGULAR.Name = "REGULAR"
        Me.REGULAR.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.REGULAR.Size = New System.Drawing.Size(67, 17)
        Me.REGULAR.TabIndex = 0
        Me.REGULAR.TabStop = True
        Me.REGULAR.Tag = "other"
        Me.REGULAR.Text = "تصويت عام"
        Me.REGULAR.UseVisualStyleBackColor = True
        '
        'PER_DOCUMENTISSUEDATE1
        '
        Me.PER_DOCUMENTISSUEDATE1.Enabled = False
        Me.PER_DOCUMENTISSUEDATE1.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTISSUEDATE1.ForeColor = System.Drawing.Color.Black
        Me.PER_DOCUMENTISSUEDATE1.Location = New System.Drawing.Point(18, 825)
        Me.PER_DOCUMENTISSUEDATE1.Multiline = True
        Me.PER_DOCUMENTISSUEDATE1.Name = "PER_DOCUMENTISSUEDATE1"
        Me.PER_DOCUMENTISSUEDATE1.Size = New System.Drawing.Size(168, 34)
        Me.PER_DOCUMENTISSUEDATE1.TabIndex = 18
        Me.PER_DOCUMENTISSUEDATE1.Tag = "addition"
        '
        'PER_DOCUMENTISSUEDATE2
        '
        Me.PER_DOCUMENTISSUEDATE2.Enabled = False
        Me.PER_DOCUMENTISSUEDATE2.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTISSUEDATE2.ForeColor = System.Drawing.Color.Black
        Me.PER_DOCUMENTISSUEDATE2.Location = New System.Drawing.Point(18, 864)
        Me.PER_DOCUMENTISSUEDATE2.Multiline = True
        Me.PER_DOCUMENTISSUEDATE2.Name = "PER_DOCUMENTISSUEDATE2"
        Me.PER_DOCUMENTISSUEDATE2.Size = New System.Drawing.Size(170, 32)
        Me.PER_DOCUMENTISSUEDATE2.TabIndex = 22
        Me.PER_DOCUMENTISSUEDATE2.Tag = "addition"
        '
        'PER_DELETEDOCUMENTISSUEDATE
        '
        Me.PER_DELETEDOCUMENTISSUEDATE.Enabled = False
        Me.PER_DELETEDOCUMENTISSUEDATE.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DELETEDOCUMENTISSUEDATE.ForeColor = System.Drawing.Color.Black
        Me.PER_DELETEDOCUMENTISSUEDATE.Location = New System.Drawing.Point(17, 1285)
        Me.PER_DELETEDOCUMENTISSUEDATE.Multiline = True
        Me.PER_DELETEDOCUMENTISSUEDATE.Name = "PER_DELETEDOCUMENTISSUEDATE"
        Me.PER_DELETEDOCUMENTISSUEDATE.Size = New System.Drawing.Size(165, 31)
        Me.PER_DELETEDOCUMENTISSUEDATE.TabIndex = 28
        Me.PER_DELETEDOCUMENTISSUEDATE.Tag = "delete"
        '
        'PER_IDPDOCUMENTISSUEDATE
        '
        Me.PER_IDPDOCUMENTISSUEDATE.Enabled = False
        Me.PER_IDPDOCUMENTISSUEDATE.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDOCUMENTISSUEDATE.ForeColor = System.Drawing.Color.Black
        Me.PER_IDPDOCUMENTISSUEDATE.Location = New System.Drawing.Point(9, 1658)
        Me.PER_IDPDOCUMENTISSUEDATE.Multiline = True
        Me.PER_IDPDOCUMENTISSUEDATE.Name = "PER_IDPDOCUMENTISSUEDATE"
        Me.PER_IDPDOCUMENTISSUEDATE.Size = New System.Drawing.Size(162, 33)
        Me.PER_IDPDOCUMENTISSUEDATE.TabIndex = 38
        Me.PER_IDPDOCUMENTISSUEDATE.Tag = "idp"
        '
        'CHGDocumentIssueDate
        '
        Me.CHGDocumentIssueDate.Enabled = False
        Me.CHGDocumentIssueDate.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CHGDocumentIssueDate.ForeColor = System.Drawing.Color.Black
        Me.CHGDocumentIssueDate.Location = New System.Drawing.Point(15, 1024)
        Me.CHGDocumentIssueDate.Multiline = True
        Me.CHGDocumentIssueDate.Name = "CHGDocumentIssueDate"
        Me.CHGDocumentIssueDate.Size = New System.Drawing.Size(173, 34)
        Me.CHGDocumentIssueDate.TabIndex = 22
        Me.CHGDocumentIssueDate.Tag = "change"
        '
        'CHGDocumentIssuePlace
        '
        Me.CHGDocumentIssuePlace.Enabled = False
        Me.CHGDocumentIssuePlace.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CHGDocumentIssuePlace.ForeColor = System.Drawing.Color.Black
        Me.CHGDocumentIssuePlace.Location = New System.Drawing.Point(194, 1024)
        Me.CHGDocumentIssuePlace.Multiline = True
        Me.CHGDocumentIssuePlace.Name = "CHGDocumentIssuePlace"
        Me.CHGDocumentIssuePlace.Size = New System.Drawing.Size(174, 33)
        Me.CHGDocumentIssuePlace.TabIndex = 21
        Me.CHGDocumentIssuePlace.Tag = "change"
        '
        'CHGDocumentNumber
        '
        Me.CHGDocumentNumber.Enabled = False
        Me.CHGDocumentNumber.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CHGDocumentNumber.ForeColor = System.Drawing.Color.Black
        Me.CHGDocumentNumber.Location = New System.Drawing.Point(374, 1024)
        Me.CHGDocumentNumber.Multiline = True
        Me.CHGDocumentNumber.Name = "CHGDocumentNumber"
        Me.CHGDocumentNumber.Size = New System.Drawing.Size(176, 34)
        Me.CHGDocumentNumber.TabIndex = 20
        Me.CHGDocumentNumber.Tag = "change"
        '
        'VRC_OID
        '
        Me.VRC_OID.Enabled = False
        Me.VRC_OID.Font = New System.Drawing.Font("Times New Roman", 20.25!)
        Me.VRC_OID.Location = New System.Drawing.Point(220, 308)
        Me.VRC_OID.Multiline = True
        Me.VRC_OID.Name = "VRC_OID"
        Me.VRC_OID.Size = New System.Drawing.Size(118, 34)
        Me.VRC_OID.TabIndex = 0
        Me.VRC_OID.Tag = "addition"
        '
        'VRC_NAME_AR
        '
        Me.VRC_NAME_AR.Enabled = False
        Me.VRC_NAME_AR.Font = New System.Drawing.Font("Times New Roman", 20.25!)
        Me.VRC_NAME_AR.Location = New System.Drawing.Point(4, 308)
        Me.VRC_NAME_AR.Multiline = True
        Me.VRC_NAME_AR.Name = "VRC_NAME_AR"
        Me.VRC_NAME_AR.Size = New System.Drawing.Size(210, 34)
        Me.VRC_NAME_AR.TabIndex = 2
        Me.VRC_NAME_AR.Tag = "addition"
        '
        'PER_FAMNO
        '
        Me.PER_FAMNO.Enabled = False
        Me.PER_FAMNO.Font = New System.Drawing.Font("Times New Roman", 20.25!)
        Me.PER_FAMNO.Location = New System.Drawing.Point(7, 369)
        Me.PER_FAMNO.MaxLength = 5
        Me.PER_FAMNO.Multiline = True
        Me.PER_FAMNO.Name = "PER_FAMNO"
        Me.PER_FAMNO.Size = New System.Drawing.Size(331, 38)
        Me.PER_FAMNO.TabIndex = 3
        Me.PER_FAMNO.Tag = "addition"
        '
        'PER_DOCUMENTTYPE1
        '
        Me.PER_DOCUMENTTYPE1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.PER_DOCUMENTTYPE1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.PER_DOCUMENTTYPE1.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTTYPE1.FormattingEnabled = True
        Me.PER_DOCUMENTTYPE1.Items.AddRange(New Object() {"هوية الاحوال المدنية", "شهادة الجنسية", "بطاقة السكن", "جواز السفر"})
        Me.PER_DOCUMENTTYPE1.Location = New System.Drawing.Point(556, 825)
        Me.PER_DOCUMENTTYPE1.Name = "PER_DOCUMENTTYPE1"
        Me.PER_DOCUMENTTYPE1.Size = New System.Drawing.Size(313, 33)
        Me.PER_DOCUMENTTYPE1.TabIndex = 15
        Me.PER_DOCUMENTTYPE1.Tag = "addition"
        '
        'PER_DOCUMENTTYPE2
        '
        Me.PER_DOCUMENTTYPE2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.PER_DOCUMENTTYPE2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.PER_DOCUMENTTYPE2.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DOCUMENTTYPE2.FormattingEnabled = True
        Me.PER_DOCUMENTTYPE2.Items.AddRange(New Object() {"هوية الاحوال المدنية", "شهادة الجنسية", "بطاقة السكن", "جواز السفر"})
        Me.PER_DOCUMENTTYPE2.Location = New System.Drawing.Point(556, 864)
        Me.PER_DOCUMENTTYPE2.Name = "PER_DOCUMENTTYPE2"
        Me.PER_DOCUMENTTYPE2.Size = New System.Drawing.Size(313, 33)
        Me.PER_DOCUMENTTYPE2.TabIndex = 19
        Me.PER_DOCUMENTTYPE2.Tag = "addition"
        '
        'CHGDocumentType
        '
        Me.CHGDocumentType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CHGDocumentType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CHGDocumentType.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CHGDocumentType.FormattingEnabled = True
        Me.CHGDocumentType.Items.AddRange(New Object() {"هوية الاحوال المدنية", "شهادة الجنسية", "بطاقة السكن", "جواز السفر"})
        Me.CHGDocumentType.Location = New System.Drawing.Point(556, 1024)
        Me.CHGDocumentType.Name = "CHGDocumentType"
        Me.CHGDocumentType.Size = New System.Drawing.Size(313, 33)
        Me.CHGDocumentType.TabIndex = 23
        Me.CHGDocumentType.Tag = "change"
        '
        'PER_DELETEDOCUMENTTYPE
        '
        Me.PER_DELETEDOCUMENTTYPE.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.PER_DELETEDOCUMENTTYPE.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.PER_DELETEDOCUMENTTYPE.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_DELETEDOCUMENTTYPE.FormattingEnabled = True
        Me.PER_DELETEDOCUMENTTYPE.Items.AddRange(New Object() {"هوية الاحوال المدنية", "شهادة الجنسية", "بطاقة السكن", "جواز السفر"})
        Me.PER_DELETEDOCUMENTTYPE.Location = New System.Drawing.Point(530, 1285)
        Me.PER_DELETEDOCUMENTTYPE.Name = "PER_DELETEDOCUMENTTYPE"
        Me.PER_DELETEDOCUMENTTYPE.Size = New System.Drawing.Size(274, 33)
        Me.PER_DELETEDOCUMENTTYPE.TabIndex = 25
        Me.PER_DELETEDOCUMENTTYPE.Tag = "delete"
        '
        'PER_IDPDOCUMENTTYPE
        '
        Me.PER_IDPDOCUMENTTYPE.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.PER_IDPDOCUMENTTYPE.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.PER_IDPDOCUMENTTYPE.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PER_IDPDOCUMENTTYPE.FormattingEnabled = True
        Me.PER_IDPDOCUMENTTYPE.Items.AddRange(New Object() {"هوية الاحوال المدنية", "شهادة الجنسية", "بطاقة السكن", "جواز السفر"})
        Me.PER_IDPDOCUMENTTYPE.Location = New System.Drawing.Point(533, 1658)
        Me.PER_IDPDOCUMENTTYPE.Name = "PER_IDPDOCUMENTTYPE"
        Me.PER_IDPDOCUMENTTYPE.Size = New System.Drawing.Size(226, 33)
        Me.PER_IDPDOCUMENTTYPE.TabIndex = 35
        Me.PER_IDPDOCUMENTTYPE.Tag = "idp"
        '
        'PictureBox19
        '
        Me.PictureBox19.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox19.Image = Global.VRU.My.Resources.Resources.s1_p3_text1_2
        Me.PictureBox19.Location = New System.Drawing.Point(942, 662)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(89, 20)
        Me.PictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox19.TabIndex = 30
        Me.PictureBox19.TabStop = False
        '
        'PER_IMAGE
        '
        Me.PER_IMAGE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PER_IMAGE.Location = New System.Drawing.Point(4, 12)
        Me.PER_IMAGE.Name = "PER_IMAGE"
        Me.PER_IMAGE.Size = New System.Drawing.Size(132, 157)
        Me.PER_IMAGE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PER_IMAGE.TabIndex = 100001
        Me.PER_IMAGE.TabStop = False
        Me.PER_IMAGE.Tag = "other"
        '
        'PictureBox73
        '
        Me.PictureBox73.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox73.Image = Global.VRU.My.Resources.Resources.s2_p5_line3_label2
        Me.PictureBox73.Location = New System.Drawing.Point(281, 1532)
        Me.PictureBox73.Name = "PictureBox73"
        Me.PictureBox73.Size = New System.Drawing.Size(235, 66)
        Me.PictureBox73.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox73.TabIndex = 119
        Me.PictureBox73.TabStop = False
        '
        'PictureBox70
        '
        Me.PictureBox70.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox70.Image = Global.VRU.My.Resources.Resources.s2_p5_line2_label1
        Me.PictureBox70.Location = New System.Drawing.Point(462, 1438)
        Me.PictureBox70.Name = "PictureBox70"
        Me.PictureBox70.Size = New System.Drawing.Size(155, 71)
        Me.PictureBox70.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox70.TabIndex = 116
        Me.PictureBox70.TabStop = False
        '
        'PictureBox68
        '
        Me.PictureBox68.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox68.Image = Global.VRU.My.Resources.Resources.s1_p1_text2
        Me.PictureBox68.Location = New System.Drawing.Point(110, 1339)
        Me.PictureBox68.Name = "PictureBox68"
        Me.PictureBox68.Size = New System.Drawing.Size(314, 33)
        Me.PictureBox68.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox68.TabIndex = 114
        Me.PictureBox68.TabStop = False
        '
        'PictureBox67
        '
        Me.PictureBox67.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox67.Image = Global.VRU.My.Resources.Resources.s2_p5_line1_label2
        Me.PictureBox67.Location = New System.Drawing.Point(424, 1373)
        Me.PictureBox67.Name = "PictureBox67"
        Me.PictureBox67.Size = New System.Drawing.Size(35, 42)
        Me.PictureBox67.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox67.TabIndex = 113
        Me.PictureBox67.TabStop = False
        '
        'PictureBox66
        '
        Me.PictureBox66.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox66.Image = Global.VRU.My.Resources.Resources.s1_p1_text1
        Me.PictureBox66.Location = New System.Drawing.Point(462, 1340)
        Me.PictureBox66.Name = "PictureBox66"
        Me.PictureBox66.Size = New System.Drawing.Size(123, 32)
        Me.PictureBox66.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox66.TabIndex = 112
        Me.PictureBox66.TabStop = False
        '
        'PictureBox65
        '
        Me.PictureBox65.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox65.Image = Global.VRU.My.Resources.Resources.s2_p5_line4_label1
        Me.PictureBox65.Location = New System.Drawing.Point(765, 1613)
        Me.PictureBox65.Name = "PictureBox65"
        Me.PictureBox65.Size = New System.Drawing.Size(228, 91)
        Me.PictureBox65.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox65.TabIndex = 111
        Me.PictureBox65.TabStop = False
        '
        'PictureBox64
        '
        Me.PictureBox64.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox64.Image = Global.VRU.My.Resources.Resources.s2_p5_line3_label1
        Me.PictureBox64.Location = New System.Drawing.Point(700, 1533)
        Me.PictureBox64.Name = "PictureBox64"
        Me.PictureBox64.Size = New System.Drawing.Size(266, 65)
        Me.PictureBox64.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox64.TabIndex = 110
        Me.PictureBox64.TabStop = False
        '
        'PictureBox62
        '
        Me.PictureBox62.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox62.Image = Global.VRU.My.Resources.Resources.s2_p5_line1_label1
        Me.PictureBox62.Location = New System.Drawing.Point(745, 1335)
        Me.PictureBox62.Name = "PictureBox62"
        Me.PictureBox62.Size = New System.Drawing.Size(248, 88)
        Me.PictureBox62.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox62.TabIndex = 108
        Me.PictureBox62.TabStop = False
        '
        'PictureBox61
        '
        Me.PictureBox61.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox61.Image = Global.VRU.My.Resources.Resources.s2_p4_label2
        Me.PictureBox61.Location = New System.Drawing.Point(810, 1240)
        Me.PictureBox61.Name = "PictureBox61"
        Me.PictureBox61.Size = New System.Drawing.Size(183, 88)
        Me.PictureBox61.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox61.TabIndex = 103
        Me.PictureBox61.TabStop = False
        '
        'PictureBox60
        '
        Me.PictureBox60.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox60.Image = Global.VRU.My.Resources.Resources.arrow_blackIdp
        Me.PictureBox60.Location = New System.Drawing.Point(999, 1335)
        Me.PictureBox60.Name = "PictureBox60"
        Me.PictureBox60.Size = New System.Drawing.Size(39, 369)
        Me.PictureBox60.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox60.TabIndex = 102
        Me.PictureBox60.TabStop = False
        '
        'PictureBox59
        '
        Me.PictureBox59.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox59.Image = Global.VRU.My.Resources.Resources.s2_p5_label1
        Me.PictureBox59.Location = New System.Drawing.Point(1066, 1340)
        Me.PictureBox59.Name = "PictureBox59"
        Me.PictureBox59.Size = New System.Drawing.Size(118, 364)
        Me.PictureBox59.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox59.TabIndex = 100
        Me.PictureBox59.TabStop = False
        '
        'PictureBox58
        '
        Me.PictureBox58.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox58.Image = Global.VRU.My.Resources.Resources.arrow_black
        Me.PictureBox58.Location = New System.Drawing.Point(999, 1240)
        Me.PictureBox58.Name = "PictureBox58"
        Me.PictureBox58.Size = New System.Drawing.Size(38, 88)
        Me.PictureBox58.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox58.TabIndex = 99
        Me.PictureBox58.TabStop = False
        '
        'PictureBox57
        '
        Me.PictureBox57.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox57.Image = Global.VRU.My.Resources.Resources.s2_p4_label1
        Me.PictureBox57.Location = New System.Drawing.Point(1066, 1240)
        Me.PictureBox57.Name = "PictureBox57"
        Me.PictureBox57.Size = New System.Drawing.Size(118, 88)
        Me.PictureBox57.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox57.TabIndex = 97
        Me.PictureBox57.TabStop = False
        '
        'PictureBox56
        '
        Me.PictureBox56.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox56.Image = Global.VRU.My.Resources.Resources.s2_p3_label4
        Me.PictureBox56.Location = New System.Drawing.Point(340, 1148)
        Me.PictureBox56.Name = "PictureBox56"
        Me.PictureBox56.Size = New System.Drawing.Size(117, 87)
        Me.PictureBox56.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox56.TabIndex = 93
        Me.PictureBox56.TabStop = False
        '
        'PictureBox55
        '
        Me.PictureBox55.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox55.Image = Global.VRU.My.Resources.Resources.s2_p3_label6
        Me.PictureBox55.Location = New System.Drawing.Point(52, 1148)
        Me.PictureBox55.Name = "PictureBox55"
        Me.PictureBox55.Size = New System.Drawing.Size(117, 88)
        Me.PictureBox55.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox55.TabIndex = 92
        Me.PictureBox55.TabStop = False
        '
        'PictureBox54
        '
        Me.PictureBox54.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox54.Image = Global.VRU.My.Resources.Resources.s2_p3_label5
        Me.PictureBox54.Location = New System.Drawing.Point(196, 1147)
        Me.PictureBox54.Name = "PictureBox54"
        Me.PictureBox54.Size = New System.Drawing.Size(117, 87)
        Me.PictureBox54.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox54.TabIndex = 91
        Me.PictureBox54.TabStop = False
        '
        'PictureBox53
        '
        Me.PictureBox53.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox53.Image = Global.VRU.My.Resources.Resources.s2_p3_label3
        Me.PictureBox53.Location = New System.Drawing.Point(484, 1149)
        Me.PictureBox53.Name = "PictureBox53"
        Me.PictureBox53.Size = New System.Drawing.Size(117, 86)
        Me.PictureBox53.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox53.TabIndex = 90
        Me.PictureBox53.TabStop = False
        '
        'PictureBox52
        '
        Me.PictureBox52.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox52.Image = Global.VRU.My.Resources.Resources.s2_p3_label2
        Me.PictureBox52.Location = New System.Drawing.Point(630, 1146)
        Me.PictureBox52.Name = "PictureBox52"
        Me.PictureBox52.Size = New System.Drawing.Size(363, 88)
        Me.PictureBox52.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox52.TabIndex = 88
        Me.PictureBox52.TabStop = False
        '
        'PictureBox51
        '
        Me.PictureBox51.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox51.Image = Global.VRU.My.Resources.Resources.arrow_black
        Me.PictureBox51.Location = New System.Drawing.Point(999, 1146)
        Me.PictureBox51.Name = "PictureBox51"
        Me.PictureBox51.Size = New System.Drawing.Size(38, 88)
        Me.PictureBox51.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox51.TabIndex = 87
        Me.PictureBox51.TabStop = False
        '
        'PictureBox50
        '
        Me.PictureBox50.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox50.Image = Global.VRU.My.Resources.Resources.s2_p3_label1
        Me.PictureBox50.Location = New System.Drawing.Point(1066, 1146)
        Me.PictureBox50.Name = "PictureBox50"
        Me.PictureBox50.Size = New System.Drawing.Size(118, 88)
        Me.PictureBox50.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox50.TabIndex = 84
        Me.PictureBox50.TabStop = False
        '
        'PictureBox49
        '
        Me.PictureBox49.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox49.Image = Global.VRU.My.Resources.Resources.s2_p2_label3
        Me.PictureBox49.Location = New System.Drawing.Point(240, 1073)
        Me.PictureBox49.Name = "PictureBox49"
        Me.PictureBox49.Size = New System.Drawing.Size(192, 57)
        Me.PictureBox49.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox49.TabIndex = 81
        Me.PictureBox49.TabStop = False
        '
        'PictureBox48
        '
        Me.PictureBox48.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox48.Image = Global.VRU.My.Resources.Resources.s2_p2_label2
        Me.PictureBox48.Location = New System.Drawing.Point(792, 1063)
        Me.PictureBox48.Name = "PictureBox48"
        Me.PictureBox48.Size = New System.Drawing.Size(201, 67)
        Me.PictureBox48.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox48.TabIndex = 80
        Me.PictureBox48.TabStop = False
        '
        'PictureBox47
        '
        Me.PictureBox47.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox47.Image = Global.VRU.My.Resources.Resources.arrow_black
        Me.PictureBox47.Location = New System.Drawing.Point(999, 1052)
        Me.PictureBox47.Name = "PictureBox47"
        Me.PictureBox47.Size = New System.Drawing.Size(39, 88)
        Me.PictureBox47.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox47.TabIndex = 79
        Me.PictureBox47.TabStop = False
        '
        'PictureBox46
        '
        Me.PictureBox46.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox46.Image = Global.VRU.My.Resources.Resources.s2_p2_label1
        Me.PictureBox46.Location = New System.Drawing.Point(1066, 1052)
        Me.PictureBox46.Name = "PictureBox46"
        Me.PictureBox46.Size = New System.Drawing.Size(118, 88)
        Me.PictureBox46.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox46.TabIndex = 78
        Me.PictureBox46.TabStop = False
        '
        'PictureBox45
        '
        Me.PictureBox45.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox45.Image = Global.VRU.My.Resources.Resources.s2_p1_label2
        Me.PictureBox45.Location = New System.Drawing.Point(15, 958)
        Me.PictureBox45.Name = "PictureBox45"
        Me.PictureBox45.Size = New System.Drawing.Size(978, 32)
        Me.PictureBox45.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox45.TabIndex = 76
        Me.PictureBox45.TabStop = False
        '
        'PictureBox44
        '
        Me.PictureBox44.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox44.Image = Global.VRU.My.Resources.Resources.arrow_black
        Me.PictureBox44.Location = New System.Drawing.Point(999, 958)
        Me.PictureBox44.Name = "PictureBox44"
        Me.PictureBox44.Size = New System.Drawing.Size(39, 88)
        Me.PictureBox44.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox44.TabIndex = 75
        Me.PictureBox44.TabStop = False
        '
        'PictureBox43
        '
        Me.PictureBox43.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox43.Image = Global.VRU.My.Resources.Resources.s2_p1_label1
        Me.PictureBox43.Location = New System.Drawing.Point(1066, 958)
        Me.PictureBox43.Name = "PictureBox43"
        Me.PictureBox43.Size = New System.Drawing.Size(118, 88)
        Me.PictureBox43.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox43.TabIndex = 74
        Me.PictureBox43.TabStop = False
        '
        'PictureBox36
        '
        Me.PictureBox36.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox36.Image = Global.VRU.My.Resources.Resources.placeedited
        Me.PictureBox36.Location = New System.Drawing.Point(192, 797)
        Me.PictureBox36.Name = "PictureBox36"
        Me.PictureBox36.Size = New System.Drawing.Size(176, 26)
        Me.PictureBox36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox36.TabIndex = 54
        Me.PictureBox36.TabStop = False
        '
        'PictureBox34
        '
        Me.PictureBox34.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox34.Image = Global.VRU.My.Resources.Resources.dateedited
        Me.PictureBox34.Location = New System.Drawing.Point(17, 797)
        Me.PictureBox34.Name = "PictureBox34"
        Me.PictureBox34.Size = New System.Drawing.Size(171, 26)
        Me.PictureBox34.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox34.TabIndex = 52
        Me.PictureBox34.TabStop = False
        '
        'PictureBox33
        '
        Me.PictureBox33.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox33.Image = Global.VRU.My.Resources.Resources.noedited
        Me.PictureBox33.Location = New System.Drawing.Point(374, 797)
        Me.PictureBox33.Name = "PictureBox33"
        Me.PictureBox33.Size = New System.Drawing.Size(176, 26)
        Me.PictureBox33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox33.TabIndex = 51
        Me.PictureBox33.TabStop = False
        '
        'PictureBox32
        '
        Me.PictureBox32.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox32.Image = Global.VRU.My.Resources.Resources.typeedited
        Me.PictureBox32.Location = New System.Drawing.Point(556, 798)
        Me.PictureBox32.Name = "PictureBox32"
        Me.PictureBox32.Size = New System.Drawing.Size(313, 25)
        Me.PictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox32.TabIndex = 50
        Me.PictureBox32.TabStop = False
        '
        'PictureBox31
        '
        Me.PictureBox31.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox31.Image = Global.VRU.My.Resources.Resources.s2_header1
        Me.PictureBox31.Location = New System.Drawing.Point(15, 918)
        Me.PictureBox31.Name = "PictureBox31"
        Me.PictureBox31.Size = New System.Drawing.Size(1169, 34)
        Me.PictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox31.TabIndex = 49
        Me.PictureBox31.TabStop = False
        '
        'PictureBox30
        '
        Me.PictureBox30.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox30.Image = Global.VRU.My.Resources.Resources.s1_p5_line2_label1
        Me.PictureBox30.Location = New System.Drawing.Point(875, 858)
        Me.PictureBox30.Name = "PictureBox30"
        Me.PictureBox30.Size = New System.Drawing.Size(39, 54)
        Me.PictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox30.TabIndex = 48
        Me.PictureBox30.TabStop = False
        '
        'PictureBox29
        '
        Me.PictureBox29.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox29.Image = Global.VRU.My.Resources.Resources.s1_p5_line1_label1
        Me.PictureBox29.Location = New System.Drawing.Point(875, 795)
        Me.PictureBox29.Name = "PictureBox29"
        Me.PictureBox29.Size = New System.Drawing.Size(39, 57)
        Me.PictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox29.TabIndex = 47
        Me.PictureBox29.TabStop = False
        '
        'PictureBox28
        '
        Me.PictureBox28.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox28.Image = Global.VRU.My.Resources.Resources.s1_p5_label1
        Me.PictureBox28.Location = New System.Drawing.Point(920, 795)
        Me.PictureBox28.Name = "PictureBox28"
        Me.PictureBox28.Size = New System.Drawing.Size(214, 118)
        Me.PictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox28.TabIndex = 46
        Me.PictureBox28.TabStop = False
        '
        'PictureBox27
        '
        Me.PictureBox27.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox27.Image = Global.VRU.My.Resources.Resources.s1_p4_label2
        Me.PictureBox27.Location = New System.Drawing.Point(556, 722)
        Me.PictureBox27.Name = "PictureBox27"
        Me.PictureBox27.Size = New System.Drawing.Size(147, 65)
        Me.PictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox27.TabIndex = 43
        Me.PictureBox27.TabStop = False
        '
        'PictureBox26
        '
        Me.PictureBox26.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox26.Image = Global.VRU.My.Resources.Resources.s1_p4_label1
        Me.PictureBox26.Location = New System.Drawing.Point(1005, 730)
        Me.PictureBox26.Name = "PictureBox26"
        Me.PictureBox26.Size = New System.Drawing.Size(123, 50)
        Me.PictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox26.TabIndex = 42
        Me.PictureBox26.TabStop = False
        '
        'PictureBox25
        '
        Me.PictureBox25.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox25.Image = Global.VRU.My.Resources.Resources.s1_p3_label3
        Me.PictureBox25.Location = New System.Drawing.Point(269, 661)
        Me.PictureBox25.Name = "PictureBox25"
        Me.PictureBox25.Size = New System.Drawing.Size(165, 55)
        Me.PictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox25.TabIndex = 40
        Me.PictureBox25.TabStop = False
        '
        'PictureBox24
        '
        Me.PictureBox24.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox24.Location = New System.Drawing.Point(440, 661)
        Me.PictureBox24.Name = "PictureBox24"
        Me.PictureBox24.Size = New System.Drawing.Size(100, 21)
        Me.PictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox24.TabIndex = 36
        Me.PictureBox24.TabStop = False
        '
        'PictureBox23
        '
        Me.PictureBox23.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox23.Location = New System.Drawing.Point(540, 661)
        Me.PictureBox23.Name = "PictureBox23"
        Me.PictureBox23.Size = New System.Drawing.Size(86, 21)
        Me.PictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox23.TabIndex = 35
        Me.PictureBox23.TabStop = False
        '
        'PictureBox22
        '
        Me.PictureBox22.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox22.Location = New System.Drawing.Point(626, 661)
        Me.PictureBox22.Name = "PictureBox22"
        Me.PictureBox22.Size = New System.Drawing.Size(86, 21)
        Me.PictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox22.TabIndex = 34
        Me.PictureBox22.TabStop = False
        '
        'PictureBox21
        '
        Me.PictureBox21.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox21.Image = Global.VRU.My.Resources.Resources.s1_p3_text1_1
        Me.PictureBox21.Location = New System.Drawing.Point(847, 661)
        Me.PictureBox21.Name = "PictureBox21"
        Me.PictureBox21.Size = New System.Drawing.Size(89, 20)
        Me.PictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox21.TabIndex = 32
        Me.PictureBox21.TabStop = False
        '
        'PictureBox20
        '
        Me.PictureBox20.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox20.Image = Global.VRU.My.Resources.Resources.s1_p3_label2
        Me.PictureBox20.Location = New System.Drawing.Point(716, 661)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(100, 50)
        Me.PictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox20.TabIndex = 31
        Me.PictureBox20.TabStop = False
        '
        'PictureBox18
        '
        Me.PictureBox18.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox18.Image = Global.VRU.My.Resources.Resources.s1_p3_label1
        Me.PictureBox18.Location = New System.Drawing.Point(1037, 661)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(91, 50)
        Me.PictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox18.TabIndex = 29
        Me.PictureBox18.TabStop = False
        '
        'PictureBox17
        '
        Me.PictureBox17.Image = Global.VRU.My.Resources.Resources.s1_p2_delimiter
        Me.PictureBox17.Location = New System.Drawing.Point(572, 390)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(10, 241)
        Me.PictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox17.TabIndex = 24
        Me.PictureBox17.TabStop = False
        '
        'PictureBox16
        '
        Me.PictureBox16.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox16.Image = Global.VRU.My.Resources.Resources.s1_p2_left_line1_label1
        Me.PictureBox16.Location = New System.Drawing.Point(386, 349)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(130, 64)
        Me.PictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox16.TabIndex = 23
        Me.PictureBox16.TabStop = False
        '
        'PictureBox15
        '
        Me.PictureBox15.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox15.Image = Global.VRU.My.Resources.Resources.s1_p2_left_line2_text
        Me.PictureBox15.Location = New System.Drawing.Point(13, 426)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(503, 28)
        Me.PictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox15.TabIndex = 22
        Me.PictureBox15.TabStop = False
        '
        'PictureBox14
        '
        Me.PictureBox14.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox14.Image = Global.VRU.My.Resources.Resources.s1_p2_left_line3_text
        Me.PictureBox14.Location = New System.Drawing.Point(13, 498)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(503, 32)
        Me.PictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox14.TabIndex = 21
        Me.PictureBox14.TabStop = False
        '
        'PictureBox13
        '
        Me.PictureBox13.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox13.Image = Global.VRU.My.Resources.Resources.s1_p2_left_line4_text
        Me.PictureBox13.Location = New System.Drawing.Point(13, 574)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(503, 35)
        Me.PictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox13.TabIndex = 20
        Me.PictureBox13.TabStop = False
        '
        'PictureBox12
        '
        Me.PictureBox12.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox12.Image = Global.VRU.My.Resources.Resources.s1_p2_right_line3_text
        Me.PictureBox12.Location = New System.Drawing.Point(649, 498)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(479, 32)
        Me.PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox12.TabIndex = 15
        Me.PictureBox12.TabStop = False
        '
        'PictureBox11
        '
        Me.PictureBox11.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox11.Image = Global.VRU.My.Resources.Resources.s1_p2_right_line2_text
        Me.PictureBox11.Location = New System.Drawing.Point(649, 426)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(479, 28)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox11.TabIndex = 14
        Me.PictureBox11.TabStop = False
        '
        'PictureBox10
        '
        Me.PictureBox10.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox10.Image = Global.VRU.My.Resources.Resources.s1_p2_right_line4_text
        Me.PictureBox10.Location = New System.Drawing.Point(649, 574)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(479, 35)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox10.TabIndex = 13
        Me.PictureBox10.TabStop = False
        '
        'PictureBox9
        '
        Me.PictureBox9.Image = Global.VRU.My.Resources.Resources.s1_p2_right_line1_label1
        Me.PictureBox9.Location = New System.Drawing.Point(991, 353)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(137, 65)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox9.TabIndex = 12
        Me.PictureBox9.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox8.Image = Global.VRU.My.Resources.Resources.s1_p1_text1
        Me.PictureBox8.Location = New System.Drawing.Point(220, 259)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(118, 43)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox8.TabIndex = 11
        Me.PictureBox8.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox7.Image = Global.VRU.My.Resources.Resources.s1_p1_text2
        Me.PictureBox7.Location = New System.Drawing.Point(649, 264)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(267, 42)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox7.TabIndex = 10
        Me.PictureBox7.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.Image = Global.VRU.My.Resources.Resources.s1_p1_label2
        Me.PictureBox6.Location = New System.Drawing.Point(344, 259)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(196, 84)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox6.TabIndex = 9
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox5.Image = Global.VRU.My.Resources.Resources.s1_p1_text2
        Me.PictureBox5.Location = New System.Drawing.Point(4, 259)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(210, 43)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox5.TabIndex = 8
        Me.PictureBox5.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox4.Image = Global.VRU.My.Resources.Resources.s1_p1_text1
        Me.PictureBox4.Location = New System.Drawing.Point(922, 264)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(124, 42)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox4.TabIndex = 4
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.Image = Global.VRU.My.Resources.Resources.s1_p1_label
        Me.PictureBox3.Location = New System.Drawing.Point(1065, 263)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(121, 84)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox3.TabIndex = 2
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.VRU.My.Resources.Resources.s1_header
        Me.PictureBox2.Location = New System.Drawing.Point(7, 175)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(1176, 36)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 1
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.VRU.My.Resources.Resources.logo_top
        Me.PictureBox1.Location = New System.Drawing.Point(142, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(1038, 157)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Tag = "other"
        '
        'PictureBox35
        '
        Me.PictureBox35.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox35.Image = Global.VRU.My.Resources.Resources.placeedited
        Me.PictureBox35.Location = New System.Drawing.Point(192, 995)
        Me.PictureBox35.Name = "PictureBox35"
        Me.PictureBox35.Size = New System.Drawing.Size(176, 26)
        Me.PictureBox35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox35.TabIndex = 100010
        Me.PictureBox35.TabStop = False
        '
        'PictureBox37
        '
        Me.PictureBox37.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox37.Image = Global.VRU.My.Resources.Resources.dateedited
        Me.PictureBox37.Location = New System.Drawing.Point(17, 995)
        Me.PictureBox37.Name = "PictureBox37"
        Me.PictureBox37.Size = New System.Drawing.Size(171, 26)
        Me.PictureBox37.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox37.TabIndex = 100009
        Me.PictureBox37.TabStop = False
        '
        'PictureBox38
        '
        Me.PictureBox38.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox38.Image = Global.VRU.My.Resources.Resources.noedited
        Me.PictureBox38.Location = New System.Drawing.Point(374, 995)
        Me.PictureBox38.Name = "PictureBox38"
        Me.PictureBox38.Size = New System.Drawing.Size(176, 26)
        Me.PictureBox38.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox38.TabIndex = 100008
        Me.PictureBox38.TabStop = False
        '
        'PictureBox39
        '
        Me.PictureBox39.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox39.Image = Global.VRU.My.Resources.Resources.typeedited
        Me.PictureBox39.Location = New System.Drawing.Point(556, 996)
        Me.PictureBox39.Name = "PictureBox39"
        Me.PictureBox39.Size = New System.Drawing.Size(313, 25)
        Me.PictureBox39.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox39.TabIndex = 100007
        Me.PictureBox39.TabStop = False
        '
        'PictureBox40
        '
        Me.PictureBox40.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox40.Image = Global.VRU.My.Resources.Resources.placeedited
        Me.PictureBox40.Location = New System.Drawing.Point(188, 1255)
        Me.PictureBox40.Name = "PictureBox40"
        Me.PictureBox40.Size = New System.Drawing.Size(165, 26)
        Me.PictureBox40.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox40.TabIndex = 100014
        Me.PictureBox40.TabStop = False
        '
        'PictureBox41
        '
        Me.PictureBox41.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox41.Image = Global.VRU.My.Resources.Resources.dateedited
        Me.PictureBox41.Location = New System.Drawing.Point(17, 1255)
        Me.PictureBox41.Name = "PictureBox41"
        Me.PictureBox41.Size = New System.Drawing.Size(165, 26)
        Me.PictureBox41.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox41.TabIndex = 100013
        Me.PictureBox41.TabStop = False
        '
        'PictureBox42
        '
        Me.PictureBox42.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox42.Image = Global.VRU.My.Resources.Resources.noedited
        Me.PictureBox42.Location = New System.Drawing.Point(359, 1255)
        Me.PictureBox42.Name = "PictureBox42"
        Me.PictureBox42.Size = New System.Drawing.Size(165, 26)
        Me.PictureBox42.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox42.TabIndex = 100012
        Me.PictureBox42.TabStop = False
        '
        'PictureBox63
        '
        Me.PictureBox63.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox63.Image = Global.VRU.My.Resources.Resources.typeedited
        Me.PictureBox63.Location = New System.Drawing.Point(530, 1256)
        Me.PictureBox63.Name = "PictureBox63"
        Me.PictureBox63.Size = New System.Drawing.Size(274, 25)
        Me.PictureBox63.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox63.TabIndex = 100011
        Me.PictureBox63.TabStop = False
        '
        'PictureBox69
        '
        Me.PictureBox69.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox69.Image = Global.VRU.My.Resources.Resources.placeedited
        Me.PictureBox69.Location = New System.Drawing.Point(175, 1626)
        Me.PictureBox69.Name = "PictureBox69"
        Me.PictureBox69.Size = New System.Drawing.Size(174, 26)
        Me.PictureBox69.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox69.TabIndex = 100018
        Me.PictureBox69.TabStop = False
        '
        'PictureBox71
        '
        Me.PictureBox71.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox71.Image = Global.VRU.My.Resources.Resources.dateedited
        Me.PictureBox71.Location = New System.Drawing.Point(11, 1626)
        Me.PictureBox71.Name = "PictureBox71"
        Me.PictureBox71.Size = New System.Drawing.Size(160, 26)
        Me.PictureBox71.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox71.TabIndex = 100017
        Me.PictureBox71.TabStop = False
        '
        'PictureBox72
        '
        Me.PictureBox72.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox72.Image = Global.VRU.My.Resources.Resources.noedited
        Me.PictureBox72.Location = New System.Drawing.Point(355, 1626)
        Me.PictureBox72.Name = "PictureBox72"
        Me.PictureBox72.Size = New System.Drawing.Size(171, 26)
        Me.PictureBox72.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox72.TabIndex = 100016
        Me.PictureBox72.TabStop = False
        '
        'PictureBox74
        '
        Me.PictureBox74.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox74.Image = Global.VRU.My.Resources.Resources.typeedited
        Me.PictureBox74.Location = New System.Drawing.Point(533, 1627)
        Me.PictureBox74.Name = "PictureBox74"
        Me.PictureBox74.Size = New System.Drawing.Size(226, 25)
        Me.PictureBox74.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox74.TabIndex = 100015
        Me.PictureBox74.TabStop = False
        '
        'frmAddUpdate
        '
        Me.AcceptButton = Me.btnSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.LightGray
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(1209, 766)
        Me.Controls.Add(Me.PictureBox69)
        Me.Controls.Add(Me.PictureBox71)
        Me.Controls.Add(Me.PictureBox72)
        Me.Controls.Add(Me.PictureBox74)
        Me.Controls.Add(Me.PictureBox40)
        Me.Controls.Add(Me.PictureBox41)
        Me.Controls.Add(Me.PictureBox42)
        Me.Controls.Add(Me.PictureBox63)
        Me.Controls.Add(Me.PictureBox35)
        Me.Controls.Add(Me.PictureBox37)
        Me.Controls.Add(Me.PictureBox38)
        Me.Controls.Add(Me.PictureBox39)
        Me.Controls.Add(Me.PER_IDPDOCUMENTTYPE)
        Me.Controls.Add(Me.PER_DELETEDOCUMENTTYPE)
        Me.Controls.Add(Me.CHGDocumentType)
        Me.Controls.Add(Me.PER_DOCUMENTTYPE2)
        Me.Controls.Add(Me.PER_DOCUMENTTYPE1)
        Me.Controls.Add(Me.PER_FAMNO)
        Me.Controls.Add(Me.VRC_NAME_AR)
        Me.Controls.Add(Me.PictureBox19)
        Me.Controls.Add(Me.VRC_OID)
        Me.Controls.Add(Me.CHGDocumentIssueDate)
        Me.Controls.Add(Me.CHGDocumentIssuePlace)
        Me.Controls.Add(Me.CHGDocumentNumber)
        Me.Controls.Add(Me.PER_IMAGE)
        Me.Controls.Add(Me.PER_IDPDOCUMENTISSUEDATE)
        Me.Controls.Add(Me.PER_DELETEDOCUMENTISSUEDATE)
        Me.Controls.Add(Me.PER_DOCUMENTISSUEDATE2)
        Me.Controls.Add(Me.PER_DOCUMENTISSUEDATE1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.PER_IDPDATE)
        Me.Controls.Add(Me.PER_IDPDOCUMENTISSUEPLACE)
        Me.Controls.Add(Me.PER_IDPDOCUMENTNO)
        Me.Controls.Add(Me.PER_IDPFRCID)
        Me.Controls.Add(Me.PER_IDPVRCID)
        Me.Controls.Add(Me.PER_ORGGOVNAME)
        Me.Controls.Add(Me.PER_ORGGOVID)
        Me.Controls.Add(Me.PictureBox73)
        Me.Controls.Add(Me.PictureBox70)
        Me.Controls.Add(Me.PictureBox68)
        Me.Controls.Add(Me.PictureBox67)
        Me.Controls.Add(Me.PictureBox66)
        Me.Controls.Add(Me.PictureBox65)
        Me.Controls.Add(Me.PictureBox64)
        Me.Controls.Add(Me.PictureBox62)
        Me.Controls.Add(Me.PER_DELETEDOCUMENTISSUEPALCE)
        Me.Controls.Add(Me.PER_DELETEDOCUMENTNO)
        Me.Controls.Add(Me.PictureBox61)
        Me.Controls.Add(Me.PictureBox60)
        Me.Controls.Add(Me.IDP)
        Me.Controls.Add(Me.PictureBox59)
        Me.Controls.Add(Me.PictureBox58)
        Me.Controls.Add(Me.DEL)
        Me.Controls.Add(Me.PictureBox57)
        Me.Controls.Add(Me.CHK_DOB)
        Me.Controls.Add(Me.CHK_GRANDNAME)
        Me.Controls.Add(Me.CHK_FATHERNAME)
        Me.Controls.Add(Me.PictureBox56)
        Me.Controls.Add(Me.PictureBox55)
        Me.Controls.Add(Me.PictureBox54)
        Me.Controls.Add(Me.PictureBox53)
        Me.Controls.Add(Me.CHK_VOTERNAME)
        Me.Controls.Add(Me.PictureBox52)
        Me.Controls.Add(Me.PictureBox51)
        Me.Controls.Add(Me.COR)
        Me.Controls.Add(Me.CHG)
        Me.Controls.Add(Me.PictureBox50)
        Me.Controls.Add(Me.PER_OLDFRC)
        Me.Controls.Add(Me.PER_OLDVRC)
        Me.Controls.Add(Me.PictureBox49)
        Me.Controls.Add(Me.PictureBox48)
        Me.Controls.Add(Me.PictureBox47)
        Me.Controls.Add(Me.PictureBox46)
        Me.Controls.Add(Me.ADD)
        Me.Controls.Add(Me.PictureBox45)
        Me.Controls.Add(Me.PictureBox44)
        Me.Controls.Add(Me.PictureBox43)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.PER_DOCUMENTISSUEPLACE2)
        Me.Controls.Add(Me.PER_DOCUMENTNO2)
        Me.Controls.Add(Me.PER_DOCUMENTISSUEPLACE1)
        Me.Controls.Add(Me.PER_DOCUMENTNO1)
        Me.Controls.Add(Me.PictureBox36)
        Me.Controls.Add(Me.PictureBox34)
        Me.Controls.Add(Me.PictureBox33)
        Me.Controls.Add(Me.PictureBox32)
        Me.Controls.Add(Me.PictureBox31)
        Me.Controls.Add(Me.PictureBox30)
        Me.Controls.Add(Me.PictureBox29)
        Me.Controls.Add(Me.PictureBox28)
        Me.Controls.Add(Me.PC_NAME)
        Me.Controls.Add(Me.PC_ID)
        Me.Controls.Add(Me.PictureBox27)
        Me.Controls.Add(Me.PictureBox26)
        Me.Controls.Add(Me.PER_PUID)
        Me.Controls.Add(Me.PictureBox25)
        Me.Controls.Add(Me.PER_DOB)
        Me.Controls.Add(Me.PictureBox24)
        Me.Controls.Add(Me.PictureBox23)
        Me.Controls.Add(Me.PictureBox22)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.PictureBox21)
        Me.Controls.Add(Me.PictureBox20)
        Me.Controls.Add(Me.PictureBox18)
        Me.Controls.Add(Me.PER_HEADGRAND)
        Me.Controls.Add(Me.PER_HEADFATHER)
        Me.Controls.Add(Me.PER_HEAD)
        Me.Controls.Add(Me.PictureBox17)
        Me.Controls.Add(Me.PictureBox16)
        Me.Controls.Add(Me.PictureBox15)
        Me.Controls.Add(Me.PictureBox14)
        Me.Controls.Add(Me.PictureBox13)
        Me.Controls.Add(Me.PER_GRAND)
        Me.Controls.Add(Me.PER_FATHER)
        Me.Controls.Add(Me.PER_FIRST)
        Me.Controls.Add(Me.PER_ID)
        Me.Controls.Add(Me.PictureBox12)
        Me.Controls.Add(Me.PictureBox11)
        Me.Controls.Add(Me.PictureBox10)
        Me.Controls.Add(Me.PictureBox9)
        Me.Controls.Add(Me.PictureBox8)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.GOV_NAME_AR)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.GOV_MOT_ID)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1225, 920)
        Me.MinimumSize = New System.Drawing.Size(1225, 801)
        Me.Name = "frmAddUpdate"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PER_IMAGE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox74, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents GOV_MOT_ID As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents GOV_NAME_AR As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents PER_ID As System.Windows.Forms.TextBox
    Friend WithEvents PER_FIRST As System.Windows.Forms.TextBox
    Friend WithEvents PER_FATHER As System.Windows.Forms.TextBox
    Friend WithEvents PER_GRAND As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox16 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents PER_HEAD As System.Windows.Forms.TextBox
    Friend WithEvents PER_HEADFATHER As System.Windows.Forms.TextBox
    Friend WithEvents PER_HEADGRAND As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox18 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox19 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox20 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox21 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PER_GENDER As System.Windows.Forms.RadioButton
    Friend WithEvents PER_GENDERM As System.Windows.Forms.RadioButton
    Friend WithEvents PictureBox22 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox23 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox24 As System.Windows.Forms.PictureBox
    Friend WithEvents PER_DOB As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox25 As System.Windows.Forms.PictureBox
    Friend WithEvents PER_PUID As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox26 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox27 As System.Windows.Forms.PictureBox
    Friend WithEvents PC_ID As System.Windows.Forms.TextBox
    Friend WithEvents PC_NAME As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox28 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox29 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox30 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox31 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox32 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox33 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox34 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox36 As System.Windows.Forms.PictureBox
    Friend WithEvents PER_DOCUMENTNO1 As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOCUMENTISSUEPLACE1 As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOCUMENTNO2 As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOCUMENTISSUEPLACE2 As System.Windows.Forms.TextBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents PictureBox43 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox45 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox44 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox48 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox47 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox46 As System.Windows.Forms.PictureBox
    Friend WithEvents ADD As System.Windows.Forms.CheckBox
    Friend WithEvents PictureBox49 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox50 As System.Windows.Forms.PictureBox
    Friend WithEvents PER_OLDFRC As System.Windows.Forms.TextBox
    Friend WithEvents PER_OLDVRC As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox52 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox51 As System.Windows.Forms.PictureBox
    Friend WithEvents COR As System.Windows.Forms.CheckBox
    Friend WithEvents CHG As System.Windows.Forms.CheckBox
    Friend WithEvents CHK_VOTERNAME As System.Windows.Forms.CheckBox
    Friend WithEvents CHK_DOB As System.Windows.Forms.CheckBox
    Friend WithEvents CHK_GRANDNAME As System.Windows.Forms.CheckBox
    Friend WithEvents CHK_FATHERNAME As System.Windows.Forms.CheckBox
    Friend WithEvents PictureBox56 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox55 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox54 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox53 As System.Windows.Forms.PictureBox
    Friend WithEvents DEL As System.Windows.Forms.CheckBox
    Friend WithEvents PictureBox57 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox58 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox60 As System.Windows.Forms.PictureBox
    Friend WithEvents IDP As System.Windows.Forms.CheckBox
    Friend WithEvents PictureBox59 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox61 As System.Windows.Forms.PictureBox
    Friend WithEvents PER_DELETEDOCUMENTISSUEPALCE As System.Windows.Forms.TextBox
    Friend WithEvents PER_DELETEDOCUMENTNO As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox62 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox73 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox70 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox68 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox67 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox66 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox65 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox64 As System.Windows.Forms.PictureBox
    Friend WithEvents PER_ORGGOVNAME As System.Windows.Forms.TextBox
    Friend WithEvents PER_ORGGOVID As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPFRCID As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPVRCID As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPDOCUMENTISSUEPLACE As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPDOCUMENTNO As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPDATE As System.Windows.Forms.TextBox
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ABS As System.Windows.Forms.RadioButton
    Friend WithEvents SPECIAL As System.Windows.Forms.RadioButton
    Friend WithEvents REGULAR As System.Windows.Forms.RadioButton
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents PER_DOCUMENTISSUEDATE1 As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOCUMENTISSUEDATE2 As System.Windows.Forms.TextBox
    Friend WithEvents PER_DELETEDOCUMENTISSUEDATE As System.Windows.Forms.TextBox
    Friend WithEvents PER_IDPDOCUMENTISSUEDATE As System.Windows.Forms.TextBox
    Friend WithEvents PER_IMAGE As System.Windows.Forms.PictureBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents CHGDocumentIssueDate As System.Windows.Forms.TextBox
    Friend WithEvents CHGDocumentIssuePlace As System.Windows.Forms.TextBox
    Friend WithEvents CHGDocumentNumber As System.Windows.Forms.TextBox
    Friend WithEvents VRC_OID As System.Windows.Forms.TextBox
    Friend WithEvents VRC_NAME_AR As System.Windows.Forms.TextBox
    Friend WithEvents PER_FAMNO As System.Windows.Forms.TextBox
    Friend WithEvents PER_DOCUMENTTYPE1 As System.Windows.Forms.ComboBox
    Friend WithEvents PER_DOCUMENTTYPE2 As System.Windows.Forms.ComboBox
    Friend WithEvents CHGDocumentType As System.Windows.Forms.ComboBox
    Friend WithEvents PER_DELETEDOCUMENTTYPE As System.Windows.Forms.ComboBox
    Friend WithEvents PER_IDPDOCUMENTTYPE As System.Windows.Forms.ComboBox
    Friend WithEvents PictureBox35 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox37 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox38 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox39 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox40 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox41 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox42 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox63 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox69 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox71 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox72 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox74 As System.Windows.Forms.PictureBox

End Class
