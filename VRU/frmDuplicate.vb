﻿Public Class frmDuplicate
    Public Per_Id2 As Integer = 0
    Public PER_ID As Integer = 0
    Public dupType As Integer

    Private Sub frmDuplicate_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Dim duplicate As New PersonDAL
        duplicate.DeleteTemp(Per_Id2)
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        'If MsgBox("هل انت متاكد من الالغاء", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
        frmAddUpdate.myPER_ID = Me.PER_ID
        Dim duplicate As New PersonDAL
        duplicate.DeleteTemp(Per_Id2)
        Me.Close()
        'End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        frmAddUpdate.myPER_ID = Me.PER_ID
        Dim duplicate As New PersonDAL
        duplicate.DeleteTemp(Per_Id2)
        Me.Close()
    End Sub

    Private Sub btnCorrect_Click(sender As Object, e As EventArgs) Handles btnCorrect.Click
        Dim duplicate As New PersonDAL
        If duplicate.CorrectDuplicate(Per_Id2) = -1 Then
            MsgBox("لقد تم التحديث!")
            frmAddUpdate.myPER_ID = Me.PER_ID
            Me.Close()
        End If
       
    End Sub

    Private Sub gvDuplicates_RowHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles gvDuplicates.RowHeaderMouseClick
        Label2.Text = gvDuplicates.Rows(e.RowIndex).Cells("نوع التكرار").Value
        dupType = gvDuplicates.Rows(e.RowIndex).Cells("did").Value
        Per_Id2 = gvDuplicates.Rows(e.RowIndex).Cells("رقم الناخب المكرر").Value
        PER_ID = gvDuplicates.Rows(e.RowIndex).Cells("رقم الناخب").Value
    End Sub

    Private Sub frmDuplicate_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label2.Text = gvDuplicates.Rows(0).Cells("نوع التكرار").Value
        Per_Id2 = gvDuplicates.Rows(0).Cells("رقم الناخب المكرر").Value
        Me.PER_ID = gvDuplicates.Rows(0).Cells("رقم الناخب").Value
        dupType = gvDuplicates.Rows(0).Cells("did").Value
        gvDuplicates.Columns("did").Visible = False
    End Sub

    Private Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click
        If dupType = 1 Or dupType = 2 Or dupType = 3 Then
            MsgBox("لا يمكن اضافة هذا الشخص لوجود تكرار حرج")
            Exit Sub
        End If
        Dim duplicate As New PersonDAL
        duplicate.DeleteTemp(Per_Id2)
        frmAddUpdate.addPerson = True
        Me.Close()
    End Sub
End Class