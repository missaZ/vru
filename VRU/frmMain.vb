﻿Imports System.Windows.Forms
Imports System.IO

Public Class frmMain

    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ExitToolStripMenuItem.Click
        End
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Close all child forms of the parent.
        For Each childform In Me.MdiChildren
            childform.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer

    Private Sub SearchToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SearchToolStripMenuItem.Click
        CloseAllToolStripMenuItem_Click(sender, e)

        frmSearch.MdiParent = Me
        frmSearch.Show()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        CloseAllToolStripMenuItem_Click(sender, e)

        frmAbout.MdiParent = Me
        frmAbout.Show()
    End Sub

    Private Sub UpdateToolStripMenuItem_Click(sender As Object, e As EventArgs)
        CloseAllToolStripMenuItem_Click(sender, e)

        frmSearch.MdiParent = Me
        frmSearch.Show()
    End Sub

    Private Sub TestToolStripMenuItem_Click(sender As Object, e As EventArgs)
        CloseAllToolStripMenuItem_Click(sender, e)

        frmPrintForm.MdiParent = Me
        frmPrintForm.Show()
    End Sub

    Private Sub AddToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddToolStripMenuItem.Click
        frmSearch.isSearch = False
        frmAddUpdate.finder = New PersonDAL
        frmAddUpdate.MdiParent = Me
        frmAddUpdate.ADD.Checked = True
        frmAddUpdate.Show()

    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        SaveSetting(Application.ProductName, Me.Name, "vrc", VRC)
        SaveSetting(Application.ProductName, Me.Name, "vrcname", VRCNAME)
        frmLogIn.Close()
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        VRC = GetSetting(Application.ProductName, Me.Name, "vrc")
        VRCNAME = GetSetting(Application.ProductName, Me.Name, "vrcname")
    End Sub

    Private Sub ReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReportToolStripMenuItem.Click
        Shell(Directory.GetCurrentDirectory() & "\Reports\Report.exe")
    End Sub
End Class
