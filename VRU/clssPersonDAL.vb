﻿Imports System.Data.SqlClient

Public Class PersonDAL
    Inherits DataAccessLayer

    Public perList As List(Of person)

    Public Sub New()
        perList = New List(Of person)
    End Sub

    Private i As Integer

    Public Property Index As Integer
        Get
            Return i

        End Get
        Set(value As Integer)
            i = value
        End Set
    End Property

    Public Function person(key As String) As Object
        Try
            Select Case key
                Case "GOV_MOT_ID"
                    Return perList(i).GOV_MOT_ID
                Case "GOV_NAME_AR"
                    Return perList(i).GOV_NAME_AR
                Case "PER_ID"
                    Return perList(i).PER_ID
                Case "PER_FAMNO"
                    Return perList(i).PER_FAMID
                Case "PER_DOB"
                    Return perList(i).PER_DOB
                Case "PER_FIRST"
                    Return perList(i).PER_FIRST
                Case "PER_FATHER"
                    Return perList(i).PER_FATHER
                Case "PER_GRAND"
                    Return perList(i).PER_GRAND
                Case "PER_GENDER"
                    Return perList(i).PER_GENDER
                Case "REGULAR"
                    Return perList(i).REGULAR
                Case "SPECIAL"
                    Return perList(i).SPECIAL
                Case "ABS"
                    Return perList(i).ABS
                Case "COR"
                    Return perList(i).COR
                Case "DEL"
                    Return perList(i).DEL
                Case "CHG"
                    Return perList(i).CHG
                Case "IDP"
                    Return perList(i).IDP
                Case "ADD"
                    Return perList(i).ADD
                Case "CHC"
                    Return perList(i).CHC
                Case "PER_PUID"
                    Return perList(i).PER_PUID
                Case "PC_ID"
                    Return perList(i).PC_ID
                Case "VRC_OID"
                    Return perList(i).VRC_OID
                Case "PC_NAME"
                    Return perList(i).PC_NAME
                Case "VRC_NAME_AR"
                    Return perList(i).VRC_NAME_AR
                Case "PER_REGTYPE"
                    Return perList(i).PER_REGTYPE
                Case "PER_TYPE"
                    Return perList(i).PER_TYPE
                Case "PER_FHFULLNAME"
                    Return perList(i).PER_FHFULLNAME
                Case "PER_OLDVRC"
                    Return perList(i).PER_OLDVRC
                Case "PER_OLDFRC"
                    Return perList(i).PER_OLDFRC
                Case "PER_DELETEDOCUMENTTYPE"
                    Return perList(i).PER_DELDOCTYPE
                Case "PER_DELETEDOCUMENTNO"
                    Return perList(i).PER_DELDOCNO
                Case "PER_DELETEDOCUMENTISSUEPALCE"
                    Return perList(i).PER_DELDOCPLACE
                Case "PER_DELETEDOCUMENTISSUEDATE"
                    Return perList(i).PER_DELDOCDATE
                Case "PER_IDPDATE"
                    Return perList(i).PER_IDPDATE
                Case "PER_ORGGOVID"
                    Return perList(i).PER_OLDGOVID
                Case "PER_ORGGOVNAME"
                    Return perList(i).PER_OLDGOVNAME
                Case "PER_IDPDOCUMENTTYPE"
                    Return perList(i).PER_IDPDOCTYPE
                Case "PER_IDPDOCUMENTNO"
                    Return perList(i).PER_IDPDOCNO
                Case "PER_IDPDOCUMENTISSUEPLACE"
                    Return perList(i).PER_IDPDOCPLACE
                Case "PER_IDPDOCUMENTISSUEDATE"
                    Return perList(i).PER_IDPDOCDATE
                Case "PER_DOCUMENTTYPE1"
                    Return perList(i).PER_DOCTYPE
                Case "PER_DOCUMENTNO1"
                    Return perList(i).PER_DOCNO
                Case "PER_DOCUMENTISSUEPLACE1"
                    Return perList(i).PER_DOCPLACE
                Case "PER_DOCUMENTISSUEDATE1"
                    Return perList(i).PER_DOCDATE
                Case "PER_DOCUMENTTYPE2"
                    Return perList(i).PER_DOCTYPE1
                Case "PER_DOCUMENTNO2"
                    Return perList(i).PER_DOCNO1
                Case "PER_DOCUMENTISSUEPLACE2"
                    Return perList(i).PER_DOCPLACE1
                Case "PER_DOCUMENTISSUEDATE2"
                    Return perList(i).PER_DOCDATE1
                Case "PER_IDPVRCID"
                    Return perList(i).PER_IDPVRC
                Case "PER_IDPFRCID"
                    Return perList(i).PER_IDPFRC
                Case "PER_PHOTO"
                    Return perList(i).PER_ID
                Case "PER_IMAGE"
                    Return perList(i).PER_IMAGE

                Case "CHGDocumentType"
                    Return perList(i).PER_CHGDOCTYPE
                Case "CHGDocumentNumber"
                    Return perList(i).PER_CHGDOCNO
                Case "CHGDocumentIssuePlace"
                    Return perList(i).PER_CHGDOCPLACE
                Case "CHGDocumentIssueDate"
                    Return perList(i).PER_CHGDOCDATE
                Case Else
                    Return ""
            End Select


            Return Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function AddPerson(ByVal per As person) As Integer
        Try
            Dim strCommandText As String = "Add_Person"

            If Not check_PC_VRC_GOV(per) Then
                MsgBox("الرجاء التاكد من المحافظه ومركز التسجيل ومركز الاقتراع")
                Return 3
            End If

            Using DB_CONN
                Dim cmd As New SqlCommand(strCommandText, DB_CONN)
                cmd.CommandType = CommandType.StoredProcedure

                Dim p As SqlParameter()
                p = {newparameter("@RC", SqlDbType.Int, per.VRC_OID), newparameter("@UserID", SqlDbType.VarChar, userId.ToString), newparameter("@PER_IMAGE", SqlDbType.Image, per.PER_IMAGE), newparameter("@PER_DOCTYPE1", SqlDbType.NVarChar, per.PER_DOCTYPE), newparameter("@PER_DOCNO1", SqlDbType.NVarChar, per.PER_DOCNO), newparameter("@PER_DOCPLACE1", SqlDbType.NVarChar, per.PER_DOCPLACE), newparameter("@PER_DOCDATE1", SqlDbType.DateTime, per.PER_DOCDATE), newparameter("@PER_DOCTYPE2", SqlDbType.NVarChar, per.PER_DOCTYPE1), newparameter("@PER_DOCNO2", SqlDbType.NVarChar, per.PER_DOCNO1), newparameter("@PER_DOCPLACE2", SqlDbType.NVarChar, per.PER_DOCPLACE1), newparameter("@PER_DOCDATE2", SqlDbType.DateTime, per.PER_DOCDATE1), newparameter("@PER_ID", SqlDbType.Int, per.PER_ID), newparameter("@PER_FHFULLNAME", SqlDbType.VarChar, per.PER_FHFULLNAME), newparameter("@PER_VRTYPE", SqlDbType.VarChar, per.PER_TYPE), newparameter("@PER_PUID", SqlDbType.Int, per.PER_PUID), newparameter("@PER_FAMID", SqlDbType.VarChar, per.PER_FAMID), newparameter("@PER_FIRST", SqlDbType.VarChar, per.PER_FIRST), newparameter("@PER_FATHER", SqlDbType.VarChar, per.PER_FATHER), newparameter("@PER_GRAND", SqlDbType.VarChar, per.PER_GRAND), newparameter("@PER_DOB", SqlDbType.DateTime, per.PER_DOB), newparameter("@PER_GENDER", SqlDbType.Bit, per.PER_GENDER), newparameter("@PER_OGOVID", SqlDbType.Int, per.GOV_MOT_ID), newparameter("@PER_PCID", SqlDbType.Int, per.PC_ID), newparameter("@PER_REGTYPE", SqlDbType.Int, per.PER_REGTYPE)}

                Return ExecuteNonQuery(CommandType.StoredProcedure, strCommandText, p)

            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function

    Public Function DeleteTemp(ByVal per_id As Integer) As Integer
        Try
            Dim strCommandText As String = "Del_DUP"

            Using DB_CONN
                Dim cmd As New SqlCommand(strCommandText, DB_CONN)
                cmd.CommandType = CommandType.StoredProcedure

                Dim p As SqlParameter()
                p = {newparameter("@PER_ID2", SqlDbType.Int, per_id)}

                Return ExecuteNonQuery(CommandType.StoredProcedure, strCommandText, p)

            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function

    Public Function CorrectDuplicate(ByVal per_id As Integer) As Integer
        Try
            Dim strCommandText As String = "Update_DUP"

            Using DB_CONN
                Dim cmd As New SqlCommand(strCommandText, DB_CONN)
                cmd.CommandType = CommandType.StoredProcedure

                Dim p As SqlParameter()
                p = {newparameter("@PER_ID2", SqlDbType.Int, per_id)}

                Return ExecuteNonQuery(CommandType.StoredProcedure, strCommandText, p)

            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function

    Private Function check_PC_VRC_GOV(per As person) As Boolean
        Try
            Dim parmarray() As SqlParameter
            Dim dtResult As New DataTable
            Dim cmd As New SqlCommand

            parmarray = {newparameter("@PC", SqlDbType.Int, per.PC_ID), newparameter("@VRC", SqlDbType.SmallInt, per.VRC_OID), newparameter("@GOV", SqlDbType.TinyInt, per.GOV_MOT_ID)}

            Using dr As SqlDataReader = ExecuteReader(CommandType.StoredProcedure, "STD_CheckPC_VRC_GOV", parmarray)

                While dr.Read()
                    Return True
                End While
            End Using

            Return False
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

    End Function

    Public Function VRC_ID_NAME(ByRef id As Integer, ByRef name As String) As Boolean
        Try
            Dim parmarray() As SqlParameter
            Dim dtResult As New DataTable
            Dim cmd As New SqlCommand

            parmarray = {newparameter("@VRC_ID", SqlDbType.Int, id), newparameter("@VRCname", SqlDbType.VarChar, name)}

            Using dr As SqlDataReader = ExecuteReader(CommandType.StoredProcedure, "VRC_ID_NAME", parmarray)

                If dr.Read() Then
                    If dr("VRC_NAME_AR") IsNot Nothing And dr("VRC_OID") IsNot Nothing Then
                        name = dr("VRC_NAME_AR")
                        id = dr("VRC_OID")
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            End Using
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

    End Function

    Public Function PC_ID_NAME(ByRef id As Integer, ByRef name As String) As Boolean
        Try
            Dim parmarray() As SqlParameter
            Dim dtResult As New DataTable
            Dim cmd As New SqlCommand

            parmarray = {newparameter("@PC_ID", SqlDbType.Int, id), newparameter("@PCname", SqlDbType.VarChar, name)}

            Using dr As SqlDataReader = ExecuteReader(CommandType.StoredProcedure, "PC_ID_NAME", parmarray)

                If dr.Read() Then
                    If dr("PC_NAME") IsNot Nothing And dr("PC_ID") IsNot Nothing Then
                        name = dr("PC_NAME")
                        id = dr("PC_ID")
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            End Using
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

    End Function

    Public Function GOV_ID_NAME(ByRef id As Integer, ByRef name As String) As Boolean
        Try
            Dim parmarray() As SqlParameter
            parmarray = {newparameter("@GOV_ID", SqlDbType.Int, id), newparameter("@GOVname", SqlDbType.VarChar, name)}

            Using dr As SqlDataReader = ExecuteReader(CommandType.StoredProcedure, "GOV_ID_NAME", parmarray)

                If dr.Read() Then
                    If dr("GOV_NAME_AR") IsNot Nothing And dr("GOV_MOT_ID") IsNot Nothing Then
                        name = dr("GOV_NAME_AR")
                        id = dr("GOV_MOT_ID")
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            End Using
           
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

    End Function

    Public Function UpdatePerson(per As person) As Integer

        Try
            Dim strCommandText As String = "Update_Person"

            If Not check_PC_VRC_GOV(per) Then
                MsgBox("الرجاء التاكد من المحافظه ومركز التسجيل ومركز الاقتراع")
                Return 3
            End If

            Using DB_CONN
                Dim cmd As New SqlCommand(strCommandText, DB_CONN)
                cmd.CommandType = CommandType.StoredProcedure

                Dim p As SqlParameter()
                ReDim p(-1)
                If per.PER_TYPE = "DEL" Then
                    ReDim p(8)
                    p = {newparameter("@RC", SqlDbType.Int, per.VRC_OID), newparameter("@UserID", SqlDbType.VarChar, userId.ToString), newparameter("@PER_IMAGE", SqlDbType.Image, per.PER_IMAGE), newparameter("@DOC_TYPE1", SqlDbType.NVarChar, per.PER_DOCTYPE), newparameter("@DOC_NO1", SqlDbType.NVarChar, per.PER_DOCNO), newparameter("@DOC_PLACE1", SqlDbType.NVarChar, per.PER_DOCPLACE), newparameter("@DOC_DATE1", SqlDbType.NVarChar, per.PER_DOCDATE), newparameter("@PER_ID", SqlDbType.Int, per.PER_ID), newparameter("@TYPE", SqlDbType.VarChar, per.PER_TYPE), newparameter("@REG_TYPE", SqlDbType.Int, per.PER_REGTYPE)}
                ElseIf per.PER_TYPE = "CHG" Then
                    ReDim p(6)
                    p = {newparameter("@RC", SqlDbType.Int, per.VRC_OID), newparameter("@UserID", SqlDbType.VarChar, userId.ToString), newparameter("@PC_ID", SqlDbType.Int, per.PC_ID), newparameter("@DOC_TYPE1", SqlDbType.NVarChar, per.PER_DOCTYPE), newparameter("@DOC_NO1", SqlDbType.NVarChar, per.PER_DOCNO), newparameter("@DOC_PLACE1", SqlDbType.NVarChar, per.PER_DOCPLACE), newparameter("@DOC_DATE1", SqlDbType.DateTime, per.PER_DOCDATE), newparameter("@PER_IMAGE", SqlDbType.Image, per.PER_IMAGE), newparameter("@OLD_VRC", SqlDbType.Int, per.PER_OLDVRC), newparameter("@OLD_FRC", SqlDbType.Int, per.PER_OLDFRC), newparameter("@PER_ID", SqlDbType.Int, per.PER_ID), newparameter("@TYPE", SqlDbType.VarChar, per.PER_TYPE), newparameter("@REG_TYPE", SqlDbType.Int, per.PER_REGTYPE)}
                ElseIf per.PER_TYPE = "IDP" Then
                    ReDim p(13)
                    p = {newparameter("@RC", SqlDbType.Int, per.VRC_OID), newparameter("@UserID", SqlDbType.VarChar, userId.ToString), newparameter("@PC_ID", SqlDbType.Int, per.PC_ID), newparameter("@PER_IMAGE", SqlDbType.Image, per.PER_IMAGE), newparameter("@OLD_VRC", SqlDbType.Int, per.PER_OLDVRC), newparameter("@OLD_FRC", SqlDbType.Int, per.PER_OLDFRC), newparameter("@IDP_DATE", SqlDbType.DateTime, per.PER_IDPDATE), newparameter("@ORG_GOV_ID", SqlDbType.Int, per.PER_OLDGOVID), newparameter("@ORG_GOV_NAME", SqlDbType.NVarChar, per.PER_OLDGOVNAME), newparameter("@DOC_TYPE1", SqlDbType.NVarChar, per.PER_DOCTYPE), newparameter("@DOC_NO1", SqlDbType.NVarChar, per.PER_DOCNO), newparameter("@DOC_PLACE1", SqlDbType.NVarChar, per.PER_DOCPLACE), newparameter("@DOC_DATE1", SqlDbType.NVarChar, per.PER_DOCDATE), newparameter("@PER_ID", SqlDbType.Int, per.PER_ID), newparameter("@TYPE", SqlDbType.VarChar, per.PER_TYPE), newparameter("@REG_TYPE", SqlDbType.Int, per.PER_REGTYPE)}
                ElseIf per.PER_TYPE = "CHC" Then
                    ReDim p(8)
                    p = {newparameter("@RC", SqlDbType.Int, per.VRC_OID), newparameter("@UserID", SqlDbType.VarChar, userId.ToString), newparameter("@DOC_TYPE1", SqlDbType.NVarChar, per.PER_DOCTYPE), newparameter("@DOC_NO1", SqlDbType.NVarChar, per.PER_DOCNO), newparameter("@DOC_PLACE1", SqlDbType.NVarChar, per.PER_DOCPLACE), newparameter("@DOC_DATE1", SqlDbType.DateTime, per.PER_DOCDATE), newparameter("@PER_IMAGE", SqlDbType.Image, per.PER_IMAGE), newparameter("@OLD_VRC", SqlDbType.Int, per.PER_OLDVRC), newparameter("@OLD_FRC", SqlDbType.Int, per.PER_OLDFRC), newparameter("@PER_ID", SqlDbType.Int, per.PER_ID), newparameter("@TYPE", SqlDbType.VarChar, per.PER_TYPE), newparameter("@REG_TYPE", SqlDbType.Int, per.PER_REGTYPE), newparameter("@PER_GRAND", SqlDbType.VarChar, per.PER_GRAND), newparameter("@PER_FIRST", SqlDbType.VarChar, per.PER_FIRST), newparameter("@PER_FATHER", SqlDbType.VarChar, per.PER_FATHER), newparameter("@PER_DOB", SqlDbType.DateTime, per.PER_DOB), newparameter("@PC_ID", SqlDbType.Int, per.PC_ID)}
                ElseIf per.PER_TYPE = "COR" Then
                    ReDim p(8)
                    p = {newparameter("@RC", SqlDbType.Int, per.VRC_OID), newparameter("@UserID", SqlDbType.VarChar, userId.ToString), newparameter("@PER_IMAGE", SqlDbType.Image, per.PER_IMAGE), newparameter("@PER_ID", SqlDbType.Int, per.PER_ID), newparameter("@TYPE", SqlDbType.VarChar, per.PER_TYPE), newparameter("@REG_TYPE", SqlDbType.Int, per.PER_REGTYPE), newparameter("@PER_GRAND", SqlDbType.VarChar, per.PER_GRAND), newparameter("@PER_FIRST", SqlDbType.VarChar, per.PER_FIRST), newparameter("@PER_FATHER", SqlDbType.VarChar, per.PER_FATHER), newparameter("@PER_DOB", SqlDbType.DateTime, per.PER_DOB)}
                End If
                Return Convert.ToInt32(ExecuteNonQuery(CommandType.StoredProcedure, strCommandText, p))
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function

    Public Function GetPerson(ByVal per As person) As List(Of person)
        Try
            Dim parmarray() As SqlParameter
            Dim dtResult As New DataTable
            ReDim parmarray(-1)
            Dim cmd As New SqlCommand

            If per.PER_ID <> 0 Then
                parmarray = {newparameter("@PER_ID", SqlDbType.BigInt, per.PER_ID)}
            ElseIf (per.VRC_OID <> 0 And per.PER_FAMID <> 0) Or (per.VRC_ID <> 0 And per.PER_FAMID <> 0) Then
                parmarray = {newparameter("@FamNo", SqlDbType.Int, per.PER_FAMID), newparameter("@VRCOID", SqlDbType.SmallInt, per.VRC_OID), newparameter("@VRCID", SqlDbType.SmallInt, per.VRC_ID)}
            ElseIf per.GOV_MOT_ID <> 0 And per.PER_FIRST <> "" And per.PER_FATHER <> "" And per.PER_GRAND <> "" And per.PER_DOB <> 0 Then
                parmarray = {newparameter("@GOV_MOT_ID", SqlDbType.TinyInt, per.GOV_MOT_ID), newparameter("@PER_FIRST", SqlDbType.VarChar, per.PER_FIRST), newparameter("@PER_FATHER", SqlDbType.VarChar, per.PER_FATHER), newparameter("@PER_GRAND", SqlDbType.VarChar, per.PER_GRAND), newparameter("@PER_DOB", SqlDbType.Int, per.PER_DOB)}
            End If

            Using dr As SqlDataReader = ExecuteReader(CommandType.StoredProcedure, "S_Person", parmarray)

                While dr.Read()
                    per = New person
                    per.GOV_MOT_ID = IIf(IsDBNull(dr("GOV_MOT_ID")), 0, dr("GOV_MOT_ID"))
                    per.GOV_NAME_AR = IIf(IsDBNull(dr("GOV_NAME_AR")), "", dr("GOV_NAME_AR"))
                    per.PER_ID = IIf(IsDBNull(dr("PER_ID")), 0, dr("PER_ID"))
                    per.PER_FAMID = IIf(IsDBNull(dr("PER_FAMNO")), 0, dr("PER_FAMNO"))
                    per.PER_DOB = IIf(IsDBNull(dr("PER_DOB")), 0, dr("PER_DOB"))
                    per.PER_FIRST = IIf(IsDBNull(dr("PER_FIRST")), "", dr("PER_FIRST"))
                    per.PER_FATHER = IIf(IsDBNull(dr("PER_FATHER")), "", dr("PER_FATHER"))
                    per.PER_GRAND = IIf(IsDBNull(dr("PER_GRAND")), "", dr("PER_GRAND"))
                    per.PER_GENDER = IIf(IsDBNull(dr("PER_GENDER")), 0, dr("PER_GENDER"))
                    per.REGULAR = IIf(IsDBNull(dr("REGULAR")), 0, dr("REGULAR"))
                    per.SPECIAL = IIf(IsDBNull(dr("SPECIAL")), 0, dr("SPECIAL"))
                    per.ABS = IIf(IsDBNull(dr("ABS")), 0, dr("ABS"))
                    per.COR = IIf(IsDBNull(dr("COR")), 0, dr("COR"))
                    per.DEL = IIf(IsDBNull(dr("DEL")), 0, dr("DEL"))
                    per.CHG = IIf(IsDBNull(dr("CHG")), 0, dr("CHG"))
                    per.IDP = IIf(IsDBNull(dr("IDP")), 0, dr("IDP"))
                    per.ADD = IIf(IsDBNull(dr("ADD")), 0, dr("ADD"))
                    per.CHC = IIf(IsDBNull(dr("CHC")), 0, dr("CHC"))
                    per.PER_PUID = IIf(IsDBNull(dr("PER_PUID")), 0, dr("PER_PUID"))
                    per.PC_ID = IIf(IsDBNull(dr("PC_ID")), 0, dr("PC_ID"))
                    per.VRC_OID = IIf(IsDBNull(dr("VRC_OID")), 0, dr("VRC_OID"))
                    per.PC_NAME = IIf(IsDBNull(dr("PC_NAME")), "", dr("PC_NAME"))
                    per.VRC_NAME_AR = IIf(IsDBNull(dr("VRC_NAME_AR")), "", dr("VRC_NAME_AR"))
                    per.PER_FHFULLNAME = IIf(IsDBNull(dr("PER_FHFULLNAME")), " ", dr("PER_FHFULLNAME"))
                    per.PER_IDPDATE = IIf(IsDBNull(dr("PER_IDPDATE")), "", dr("PER_IDPDATE"))
                    per.PER_IDPVRC = IIf(IsDBNull(dr("PER_IDPVRCID")), 0, dr("PER_IDPVRCID"))
                    per.PER_IDPFRC = IIf(IsDBNull(dr("PER_IDPFRCID")), 0, dr("PER_IDPFRCID"))
                    per.PER_OLDVRC = IIf(IsDBNull(dr("PER_OLDVRC")), 0, dr("PER_OLDVRC"))
                    per.PER_OLDFRC = IIf(IsDBNull(dr("PER_OLDFRC")), 0, dr("PER_OLDFRC"))
                    per.PER_DELDOCTYPE = IIf(IsDBNull(dr("PER_DELETEDOCUMENTTYPE")), " ", dr("PER_DELETEDOCUMENTTYPE"))
                    per.PER_DELDOCNO = IIf(IsDBNull(dr("PER_DELETEDOCUMENTNO")), " ", dr("PER_DELETEDOCUMENTNO"))
                    per.PER_DELDOCPLACE = IIf(IsDBNull(dr("PER_DELETEDOCUMENTISSUEPALCE")), " ", dr("PER_DELETEDOCUMENTISSUEPALCE"))
                    per.PER_DELDOCDATE = IIf(IsDBNull(dr("PER_DELETEDOCUMENTISSUEDATE")), "", dr("PER_DELETEDOCUMENTISSUEDATE"))
                    per.PER_IDPDOCTYPE = IIf(IsDBNull(dr("PER_IDPDOCUMENTTYPE")), " ", dr("PER_IDPDOCUMENTTYPE"))
                    per.PER_IDPDOCNO = IIf(IsDBNull(dr("PER_IDPDOCUMENTNO")), " ", dr("PER_IDPDOCUMENTNO"))
                    per.PER_IDPDOCPLACE = IIf(IsDBNull(dr("PER_IDPDOCUMENTISSUEPLACE")), " ", dr("PER_IDPDOCUMENTISSUEPLACE"))
                    per.PER_IDPDOCDATE = IIf(IsDBNull(dr("PER_IDPDOCUMENTISSUEDATE")), "", dr("PER_IDPDOCUMENTISSUEDATE"))

                    per.PER_DOCTYPE = IIf(IsDBNull(dr("PER_DOCUMENTTYPE1")), " ", dr("PER_DOCUMENTTYPE1"))
                    per.PER_DOCNO = IIf(IsDBNull(dr("PER_DOCUMENTNO1")), " ", dr("PER_DOCUMENTNO1"))
                    per.PER_DOCPLACE = IIf(IsDBNull(dr("PER_DOCUMENTISSUEPLACE1")), " ", dr("PER_DOCUMENTISSUEPLACE1"))
                    per.PER_DOCDATE = IIf(IsDBNull(dr("PER_DOCUMENTISSUEDATE1")), "", dr("PER_DOCUMENTISSUEDATE1"))

                    per.PER_DOCTYPE1 = IIf(IsDBNull(dr("PER_DOCUMENTTYPE2")), " ", dr("PER_DOCUMENTTYPE2"))
                    per.PER_DOCNO1 = IIf(IsDBNull(dr("PER_DOCUMENTNO2")), " ", dr("PER_DOCUMENTNO2"))
                    per.PER_DOCPLACE1 = IIf(IsDBNull(dr("PER_DOCUMENTISSUEPLACE2")), " ", dr("PER_DOCUMENTISSUEPLACE2"))
                    per.PER_DOCDATE1 = IIf(IsDBNull(dr("PER_DOCUMENTISSUEDATE2")), "", dr("PER_DOCUMENTISSUEDATE2"))

                    per.PER_OLDGOVID = IIf(IsDBNull(dr("PER_ORGGOVID")), 0, dr("PER_ORGGOVID"))
                    per.PER_OLDGOVNAME = IIf(IsDBNull(dr("PER_ORGGOVNAME")), " ", dr("PER_ORGGOVNAME"))
                    per.PER_IMAGE = IIf(IsDBNull(dr("PER_IMAGE")), Nothing, dr("PER_IMAGE"))

                    per.PER_CHGDOCTYPE = IIf(IsDBNull(dr("CHGDocumentType")), " ", dr("CHGDocumentType"))
                    per.PER_CHGDOCNO = IIf(IsDBNull(dr("CHGDocumentNumber")), " ", dr("CHGDocumentNumber"))
                    per.PER_CHGDOCPLACE = IIf(IsDBNull(dr("CHGDocumentIssuePlace")), " ", dr("CHGDocumentIssuePlace"))
                    per.PER_CHGDOCDATE = IIf(IsDBNull(dr("CHGDocumentIssueDate")), "", dr("CHGDocumentIssueDate"))

                    perList.Add(per)

                End While

            End Using

            Return perList
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function check_duplicate(per As person) As Object
        Try
            Dim parmarray() As SqlParameter
            ReDim parmarray(-1)

            If Not check_PC_VRC_GOV(per) Then
                MsgBox("الرجاء التاكد من المحافظه ومركز التسجيل ومركز الاقتراع")
                Return 3
            End If

            Dim names(2) As String
            Dim full As String = per.PER_FHFULLNAME
            names = full.Split(" ")

            parmarray = {newparameter("@DOC_TYPE1", SqlDbType.NVarChar, per.PER_DOCTYPE),
                         newparameter("@PER_HFIRST", SqlDbType.VarChar, names(0)),
                         newparameter("@PER_HFATHER", SqlDbType.VarChar, names(1)),
                         newparameter("@PER_HGRAND", SqlDbType.VarChar, names(2)),
                         newparameter("@user_id", SqlDbType.Int, per.userId),
                         newparameter("@DOC_NUMBER1", SqlDbType.NVarChar, per.PER_DOCNO),
                         newparameter("@DOC_ISSUEPLACE1", SqlDbType.NVarChar, per.PER_DOCPLACE),
                         newparameter("@DOC_ISSUEDATE1", SqlDbType.NVarChar, per.PER_DOCDATE),
                         newparameter("@DOC_TYPE2", SqlDbType.NVarChar, per.PER_DOCTYPE1),
                         newparameter("@DOC_NUMBER2", SqlDbType.NVarChar, per.PER_DOCNO1),
                         newparameter("@DOC_ISSUEPLACE2", SqlDbType.NVarChar, per.PER_DOCPLACE1),
                         newparameter("@DOC_ISSUEDATE2", SqlDbType.NVarChar, per.PER_DOCDATE1),
                         newparameter("@PER_ID", SqlDbType.Int, per.PER_ID),
                         newparameter("@Rc", SqlDbType.SmallInt, per.VRC_OID),
                         newparameter("@PER_PUID", SqlDbType.Int, per.PER_PUID),
                         newparameter("@FamNo", SqlDbType.VarChar, per.PER_FAMID),
                         newparameter("@PER_GOV_ID", SqlDbType.TinyInt, per.GOV_MOT_ID),
                         newparameter("@PER_FIRST", SqlDbType.VarChar, per.PER_FIRST),
                         newparameter("@PER_FATHER", SqlDbType.VarChar, per.PER_FATHER),
                         newparameter("@PER_GRAND", SqlDbType.VarChar, per.PER_GRAND),
                         newparameter("@PER_DOB", SqlDbType.DateTime, per.PER_DOB),
                         newparameter("@PER_GENDER", SqlDbType.Bit, per.PER_GENDER),
                         newparameter("@PER_PCID", SqlDbType.Int, per.PC_ID),
                         newparameter("@per_image", SqlDbType.Image, per.PER_IMAGE),
                         newparameter("@PER_REGTYPE", SqlDbType.Int, per.PER_REGTYPE)}

            Dim dtResult As New DataTable
            Dim sqladapt As SqlDataAdapter
            Dim sqlcomm As New SqlCommand

            OpenConnection()

            sqlcomm.Connection = DB_CONN
            sqlcomm.CommandType = CommandType.StoredProcedure
            sqlcomm.CommandText = "Check_DUP"
            sqlcomm.Parameters.AddRange(parmarray)
            sqladapt = New SqlDataAdapter(sqlcomm)
            sqladapt.Fill(dtResult)

            CloseConnection()

            Return dtResult

        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function

    Public Function getGovernorates() As DataTable
        Try
            Dim dtResult As New DataTable
            Dim sqladapt As SqlDataAdapter
            Dim sqlcomm As New SqlCommand

            OpenConnection()

            sqlcomm.Connection = DB_CONN
            sqlcomm.CommandType = CommandType.StoredProcedure
            sqlcomm.CommandText = "get_governorates"
            sqladapt = New SqlDataAdapter(sqlcomm)
            sqladapt.Fill(dtResult)

            CloseConnection()

            Return dtResult
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function getLastPerId() As Integer
        Try
            
            Using dr As SqlDataReader = ExecuteReader(CommandType.StoredProcedure, "S_GetLastPersonId")
                While dr.Read()
                    If IsDBNull(dr("PER_ID")) Then
                        Return 1
                    End If
                    Return dr("PER_ID") + 1
                End While


            End Using

            Return 0
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try

    End Function
End Class

