﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearch
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblVoterId = New System.Windows.Forms.Label()
        Me.lblVrcId = New System.Windows.Forms.Label()
        Me.lblFamNo = New System.Windows.Forms.Label()
        Me.lblFirstName = New System.Windows.Forms.Label()
        Me.lblSecondName = New System.Windows.Forms.Label()
        Me.lblThirdName = New System.Windows.Forms.Label()
        Me.lblGovId = New System.Windows.Forms.Label()
        Me.lblYearOB = New System.Windows.Forms.Label()
        Me.cmbGov = New System.Windows.Forms.ComboBox()
        Me.txtVoterId = New System.Windows.Forms.TextBox()
        Me.txtVRC = New System.Windows.Forms.TextBox()
        Me.txtFamNo = New System.Windows.Forms.TextBox()
        Me.txtSecondName = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtThirdName = New System.Windows.Forms.TextBox()
        Me.txtYearOB = New System.Windows.Forms.TextBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.gbVoterId = New System.Windows.Forms.GroupBox()
        Me.gbVoterInfo = New System.Windows.Forms.GroupBox()
        Me.gbFamVRC = New System.Windows.Forms.GroupBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.gvResult = New System.Windows.Forms.DataGridView()
        Me.btnSelect = New System.Windows.Forms.Button()
        Me.btnPDF = New System.Windows.Forms.Button()
        Me.chkLock = New System.Windows.Forms.CheckBox()
        Me.gbVoterId.SuspendLayout()
        Me.gbVoterInfo.SuspendLayout()
        Me.gbFamVRC.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvResult, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblVoterId
        '
        Me.lblVoterId.AutoSize = True
        Me.lblVoterId.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoterId.Location = New System.Drawing.Point(153, 33)
        Me.lblVoterId.Name = "lblVoterId"
        Me.lblVoterId.Size = New System.Drawing.Size(46, 13)
        Me.lblVoterId.TabIndex = 0
        Me.lblVoterId.Text = "رقم الناخب"
        '
        'lblVrcId
        '
        Me.lblVrcId.AutoSize = True
        Me.lblVrcId.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVrcId.Location = New System.Drawing.Point(180, 69)
        Me.lblVrcId.Name = "lblVrcId"
        Me.lblVrcId.Size = New System.Drawing.Size(71, 13)
        Me.lblVrcId.TabIndex = 0
        Me.lblVrcId.Text = "رقم مركز التموين"
        '
        'lblFamNo
        '
        Me.lblFamNo.AutoSize = True
        Me.lblFamNo.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFamNo.Location = New System.Drawing.Point(163, 29)
        Me.lblFamNo.Name = "lblFamNo"
        Me.lblFamNo.Size = New System.Drawing.Size(82, 13)
        Me.lblFamNo.TabIndex = 0
        Me.lblFamNo.Text = "رقم البطاقة التموينية"
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(417, 29)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(47, 13)
        Me.lblFirstName.TabIndex = 0
        Me.lblFirstName.Text = "اسم الناخب"
        '
        'lblSecondName
        '
        Me.lblSecondName.AutoSize = True
        Me.lblSecondName.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSecondName.Location = New System.Drawing.Point(428, 61)
        Me.lblSecondName.Name = "lblSecondName"
        Me.lblSecondName.Size = New System.Drawing.Size(37, 13)
        Me.lblSecondName.TabIndex = 0
        Me.lblSecondName.Text = "اسم الاب"
        '
        'lblThirdName
        '
        Me.lblThirdName.AutoSize = True
        Me.lblThirdName.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblThirdName.Location = New System.Drawing.Point(428, 97)
        Me.lblThirdName.Name = "lblThirdName"
        Me.lblThirdName.Size = New System.Drawing.Size(38, 13)
        Me.lblThirdName.TabIndex = 0
        Me.lblThirdName.Text = "اسم الجد"
        '
        'lblGovId
        '
        Me.lblGovId.AutoSize = True
        Me.lblGovId.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGovId.Location = New System.Drawing.Point(187, 30)
        Me.lblGovId.Name = "lblGovId"
        Me.lblGovId.Size = New System.Drawing.Size(40, 13)
        Me.lblGovId.TabIndex = 0
        Me.lblGovId.Text = "المحافظة"
        '
        'lblYearOB
        '
        Me.lblYearOB.AutoSize = True
        Me.lblYearOB.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYearOB.Location = New System.Drawing.Point(181, 62)
        Me.lblYearOB.Name = "lblYearOB"
        Me.lblYearOB.Size = New System.Drawing.Size(45, 13)
        Me.lblYearOB.TabIndex = 0
        Me.lblYearOB.Text = "سنة التولد"
        '
        'cmbGov
        '
        Me.cmbGov.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbGov.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbGov.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGov.FormattingEnabled = True
        Me.cmbGov.Location = New System.Drawing.Point(6, 30)
        Me.cmbGov.Name = "cmbGov"
        Me.cmbGov.Size = New System.Drawing.Size(121, 21)
        Me.cmbGov.TabIndex = 7
        '
        'txtVoterId
        '
        Me.txtVoterId.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoterId.Location = New System.Drawing.Point(6, 30)
        Me.txtVoterId.MaxLength = 8
        Me.txtVoterId.Name = "txtVoterId"
        Me.txtVoterId.Size = New System.Drawing.Size(121, 20)
        Me.txtVoterId.TabIndex = 0
        '
        'txtVRC
        '
        Me.txtVRC.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVRC.Location = New System.Drawing.Point(34, 62)
        Me.txtVRC.MaxLength = 4
        Me.txtVRC.Name = "txtVRC"
        Me.txtVRC.Size = New System.Drawing.Size(121, 20)
        Me.txtVRC.TabIndex = 3
        '
        'txtFamNo
        '
        Me.txtFamNo.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFamNo.Location = New System.Drawing.Point(34, 29)
        Me.txtFamNo.MaxLength = 5
        Me.txtFamNo.Name = "txtFamNo"
        Me.txtFamNo.Size = New System.Drawing.Size(121, 20)
        Me.txtFamNo.TabIndex = 2
        '
        'txtSecondName
        '
        Me.txtSecondName.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSecondName.Location = New System.Drawing.Point(249, 61)
        Me.txtSecondName.MaxLength = 15
        Me.txtSecondName.Name = "txtSecondName"
        Me.txtSecondName.Size = New System.Drawing.Size(121, 20)
        Me.txtSecondName.TabIndex = 5
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(249, 29)
        Me.txtName.MaxLength = 15
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(121, 20)
        Me.txtName.TabIndex = 4
        '
        'txtThirdName
        '
        Me.txtThirdName.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtThirdName.Location = New System.Drawing.Point(249, 97)
        Me.txtThirdName.MaxLength = 15
        Me.txtThirdName.Name = "txtThirdName"
        Me.txtThirdName.Size = New System.Drawing.Size(121, 20)
        Me.txtThirdName.TabIndex = 6
        '
        'txtYearOB
        '
        Me.txtYearOB.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYearOB.Location = New System.Drawing.Point(6, 62)
        Me.txtYearOB.MaxLength = 10
        Me.txtYearOB.Name = "txtYearOB"
        Me.txtYearOB.Size = New System.Drawing.Size(121, 20)
        Me.txtYearOB.TabIndex = 8
        '
        'btnSearch
        '
        Me.btnSearch.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Location = New System.Drawing.Point(872, 578)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(110, 30)
        Me.btnSearch.TabIndex = 17
        Me.btnSearch.Text = "بحث"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'gbVoterId
        '
        Me.gbVoterId.Controls.Add(Me.txtVoterId)
        Me.gbVoterId.Controls.Add(Me.lblVoterId)
        Me.gbVoterId.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbVoterId.Location = New System.Drawing.Point(769, 141)
        Me.gbVoterId.Name = "gbVoterId"
        Me.gbVoterId.Size = New System.Drawing.Size(204, 127)
        Me.gbVoterId.TabIndex = 0
        Me.gbVoterId.TabStop = False
        Me.gbVoterId.Text = "البحث عن طريق رقم الناخب"
        '
        'gbVoterInfo
        '
        Me.gbVoterInfo.Controls.Add(Me.lblSecondName)
        Me.gbVoterInfo.Controls.Add(Me.lblFirstName)
        Me.gbVoterInfo.Controls.Add(Me.lblThirdName)
        Me.gbVoterInfo.Controls.Add(Me.lblGovId)
        Me.gbVoterInfo.Controls.Add(Me.lblYearOB)
        Me.gbVoterInfo.Controls.Add(Me.txtYearOB)
        Me.gbVoterInfo.Controls.Add(Me.txtName)
        Me.gbVoterInfo.Controls.Add(Me.txtSecondName)
        Me.gbVoterInfo.Controls.Add(Me.txtThirdName)
        Me.gbVoterInfo.Controls.Add(Me.cmbGov)
        Me.gbVoterInfo.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbVoterInfo.Location = New System.Drawing.Point(14, 141)
        Me.gbVoterInfo.Name = "gbVoterInfo"
        Me.gbVoterInfo.Size = New System.Drawing.Size(480, 127)
        Me.gbVoterInfo.TabIndex = 2
        Me.gbVoterInfo.TabStop = False
        Me.gbVoterInfo.Text = "البحث عن طريق المعلومات الشخصية"
        '
        'gbFamVRC
        '
        Me.gbFamVRC.Controls.Add(Me.txtFamNo)
        Me.gbFamVRC.Controls.Add(Me.lblVrcId)
        Me.gbFamVRC.Controls.Add(Me.lblFamNo)
        Me.gbFamVRC.Controls.Add(Me.txtVRC)
        Me.gbFamVRC.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFamVRC.Location = New System.Drawing.Point(500, 141)
        Me.gbFamVRC.Name = "gbFamVRC"
        Me.gbFamVRC.Size = New System.Drawing.Size(261, 127)
        Me.gbFamVRC.TabIndex = 1
        Me.gbFamVRC.TabStop = False
        Me.gbFamVRC.Text = "البحث عن طريق معلومات التموين"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(509, 578)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(109, 30)
        Me.btnCancel.TabIndex = 19
        Me.btnCancel.Text = "الغاء"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.VRU.My.Resources.Resources.VRU_Header
        Me.PictureBox1.Location = New System.Drawing.Point(14, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(970, 127)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 16
        Me.PictureBox1.TabStop = False
        '
        'gvResult
        '
        Me.gvResult.AllowUserToAddRows = False
        Me.gvResult.AllowUserToDeleteRows = False
        Me.gvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvResult.Location = New System.Drawing.Point(12, 325)
        Me.gvResult.MultiSelect = False
        Me.gvResult.Name = "gvResult"
        Me.gvResult.Size = New System.Drawing.Size(970, 247)
        Me.gvResult.TabIndex = 20
        '
        'btnSelect
        '
        Me.btnSelect.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSelect.Location = New System.Drawing.Point(753, 578)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(109, 30)
        Me.btnSelect.TabIndex = 21
        Me.btnSelect.Text = "عرض المعلومات"
        Me.btnSelect.UseVisualStyleBackColor = True
        '
        'btnPDF
        '
        Me.btnPDF.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPDF.Location = New System.Drawing.Point(630, 578)
        Me.btnPDF.Name = "btnPDF"
        Me.btnPDF.Size = New System.Drawing.Size(109, 30)
        Me.btnPDF.TabIndex = 22
        Me.btnPDF.Text = "ملف PDF"
        Me.btnPDF.UseVisualStyleBackColor = True
        '
        'chkLock
        '
        Me.chkLock.AutoSize = True
        Me.chkLock.Location = New System.Drawing.Point(898, 287)
        Me.chkLock.Name = "chkLock"
        Me.chkLock.Size = New System.Drawing.Size(75, 17)
        Me.chkLock.TabIndex = 100
        Me.chkLock.Text = "قفل الملف"
        Me.chkLock.UseVisualStyleBackColor = True
        '
        'frmSearch
        '
        Me.AcceptButton = Me.btnSearch
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lavender
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(985, 613)
        Me.Controls.Add(Me.chkLock)
        Me.Controls.Add(Me.btnPDF)
        Me.Controls.Add(Me.btnSelect)
        Me.Controls.Add(Me.gvResult)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gbVoterInfo)
        Me.Controls.Add(Me.gbFamVRC)
        Me.Controls.Add(Me.gbVoterId)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.PictureBox1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1001, 651)
        Me.MinimumSize = New System.Drawing.Size(1001, 651)
        Me.Name = "frmSearch"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "واجهة البحث"
        Me.gbVoterId.ResumeLayout(False)
        Me.gbVoterId.PerformLayout()
        Me.gbVoterInfo.ResumeLayout(False)
        Me.gbVoterInfo.PerformLayout()
        Me.gbFamVRC.ResumeLayout(False)
        Me.gbFamVRC.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvResult, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

End Sub
    Friend WithEvents lblVoterId As System.Windows.Forms.Label
    Friend WithEvents lblVrcId As System.Windows.Forms.Label
    Friend WithEvents lblFamNo As System.Windows.Forms.Label
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents lblSecondName As System.Windows.Forms.Label
    Friend WithEvents lblThirdName As System.Windows.Forms.Label
    Friend WithEvents lblGovId As System.Windows.Forms.Label
    Friend WithEvents lblYearOB As System.Windows.Forms.Label
    Friend WithEvents cmbGov As System.Windows.Forms.ComboBox
    Friend WithEvents txtVoterId As System.Windows.Forms.TextBox
    Friend WithEvents txtVRC As System.Windows.Forms.TextBox
    Friend WithEvents txtFamNo As System.Windows.Forms.TextBox
    Friend WithEvents txtSecondName As System.Windows.Forms.TextBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtThirdName As System.Windows.Forms.TextBox
    Friend WithEvents txtYearOB As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents gbVoterId As System.Windows.Forms.GroupBox
    Friend WithEvents gbVoterInfo As System.Windows.Forms.GroupBox
    Friend WithEvents gbFamVRC As System.Windows.Forms.GroupBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents gvResult As System.Windows.Forms.DataGridView
    Friend WithEvents btnSelect As System.Windows.Forms.Button
    Friend WithEvents btnPDF As System.Windows.Forms.Button
    Friend WithEvents chkLock As System.Windows.Forms.CheckBox
End Class
