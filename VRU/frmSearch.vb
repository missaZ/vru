﻿Imports iTextSharp.text.pdf
Imports System.IO
Imports System.Data.SqlClient


Public Class frmSearch
    Private index As Integer = -1
    Dim finder As PersonDAL
    Dim headers(6) As String
    Dim chkbox As New CheckBox
    Public isSearch As Boolean = False

    Public Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            Dim myform As New person
            Dim rect As Rectangle
            Dim dt As New DataTable
            Dim colCB As DataGridViewCheckBoxColumn
            Dim dr As DataRow
            Dim persons As List(Of person)

            finder = New PersonDAL

            If txtVoterId.Text <> "" Then
                myform.PER_ID = Integer.Parse(txtVoterId.Text)
            ElseIf txtFamNo.Text <> "" And txtVRC.Text <> "" Then
                myform.PER_FAMID = Integer.Parse(txtFamNo.Text)
                If txtVRC.Text.Length = 3 Then
                    myform.VRC_OID = Integer.Parse(txtVRC.Text)
                ElseIf txtVRC.Text.Length = 4 Then
                    myform.VRC_ID = Integer.Parse(txtVRC.Text)
                End If

            ElseIf txtName.Text <> "" And txtSecondName.Text <> "" And txtThirdName.Text <> "" And txtYearOB.Text <> "" Then
                myform.PER_FIRST = txtName.Text
                myform.PER_FATHER = txtSecondName.Text
                myform.PER_GRAND = txtThirdName.Text
                myform.PER_DOB = txtYearOB.Text
                myform.GOV_MOT_ID = cmbGov.SelectedValue
            End If

            finder.GetPerson(myform)

            gvResult.AllowUserToAddRows = False
            gvResult.Columns.Clear()

            colCB = New DataGridViewCheckBoxColumn()
            colCB.Name = "chkcol"
            colCB.HeaderText = ""
            colCB.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            gvResult.Columns.Add(colCB)
            rect = Me.gvResult.GetCellDisplayRectangle(0, -1, True)
            rect.Offset(40, 10)

            chkbox.Size = New Size(16, 16)
            chkbox.Location = rect.Location

            AddHandler chkbox.CheckedChanged, AddressOf chkBoxChange
            Me.gvResult.Controls.Add(chkbox)

            'Declare Column names
            dt.Columns.Add("رقم الناخب").ReadOnly = True
            dt.Columns.Add("أسم الناخب").ReadOnly = True
            dt.Columns.Add("أسم الاب").ReadOnly = True
            dt.Columns.Add("أسم الجد").ReadOnly = True
            dt.Columns.Add("التولد").ReadOnly = True
            dt.Columns.Add("رقم البطاقة التموينية").ReadOnly = True
            dt.Columns.Add("أسم المركز التمويني").ReadOnly = True
            dt.Columns.Add("رقم المركز التمويني").ReadOnly = True
            dt.Columns.Add("رقم مركز التسجيل").ReadOnly = True
            dt.Columns.Add("رقم مركز الاقتراع").ReadOnly = True
            dt.Columns.Add("أسم مركز الاقتراع").ReadOnly = True
            dt.Columns.Add("عنوان مركز الاقتراع").ReadOnly = True
            dt.Columns.Add("المحافظة").ReadOnly = True
            dt.Columns.Add("حالة الناخب").ReadOnly = True

            persons = finder.perList
            For Each p As person In persons
                dr = dt.NewRow()
                dr("رقم الناخب") = p.PER_ID
                dr("أسم الناخب") = p.PER_FIRST
                dr("أسم الاب") = p.PER_FATHER
                dr("أسم الجد") = p.PER_GRAND
                dr("التولد") = p.PER_DOB
                dr("رقم البطاقة التموينية") = p.PER_FAMID
                dr("أسم المركز التمويني") = p.VRC_NAME_AR
                dr("رقم المركز التمويني") = p.VRC_OID
                dr("رقم مركز التسجيل") = p.VRC_ID
                dr("رقم مركز الاقتراع") = p.PC_ID
                dr("أسم مركز الاقتراع") = p.PC_NAME
                dr("المحافظة") = p.GOV_NAME_AR
                dt.Rows.Add(dr)
            Next

            gvResult.DataSource = dt
            gvResult.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            gvResult.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            If dt.Rows.Count = 0 Then
                MsgBox("الرجاء التاكد من البيانات!")
            ElseIf isSearch Then
                Me.gvResult(0, 0).Value = True
            End If

            index = -1
        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

    Private Sub CHeKBOX(BOType As String)

        Dim i As Integer = 0
        Dim ChkedRow As List(Of Integer) = New List(Of Integer)

        For i = 0 To gvResult.RowCount - 1
            If Convert.ToBoolean(gvResult.Rows(i).Cells("chkcol").Value) = True Then
                ChkedRow.Add(i)
            End If
        Next

        If BOType = "PDF" Then
            If ChkedRow.Count = 0 Then
                MsgBox("الرجاء اختيار على الاقل شخص واحد")
                Exit Sub
            End If

            Cursor.Current = Cursors.AppStarting
            Dim res As Boolean = CreatePDF(ChkedRow, BOType)
            If (res) Then
                Cursor.Current = Cursors.Default
            End If
        Else
            If ChkedRow.Count <> 1 Then
                MsgBox("الرجاء اختيار شخص واحد")
                Exit Sub
            End If

            finder.Index = ChkedRow(0)
            frmAddUpdate.finder = finder
            frmAddUpdate.MdiParent = frmMain
            frmAddUpdate.Show()
        End If
    End Sub

    Function CreatePDF(ChkedRow As List(Of Integer), BOType2 As String) As Boolean
        Try
            Dim newfile As String = ""

            For Each k As Integer In ChkedRow
                newfile = PDFcreator(Integer.Parse(gvResult.Rows(k).Cells(1).Value))
            Next


            If (Not File.Exists(newfile)) Then
                Return ""
            End If
            If ChkedRow.Count = 1 Then
                frmPDF.filename = newfile
                frmPDF.ShowDialog()
            Else
                Dim argument As String = "/select, " + newfile

                System.Diagnostics.Process.Start("explorer.exe", argument)
            End If
            
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
        Return True

    End Function

    Private Sub chkBoxChange(sender As Object, e As EventArgs)
        For k As Integer = 0 To gvResult.RowCount - 1
            Me.gvResult(0, k).Value = Me.chkbox.Checked
            index = k
        Next
        Me.gvResult.EndEdit()
    End Sub

    Private Sub frmSearch_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim govs As New DataTable
            Dim gov As New Form
            Dim g As New PersonDAL

            govs = g.getGovernorates

            cmbGov.DataSource = govs
            cmbGov.DisplayMember = "GOV_NAME_AR"
            cmbGov.ValueMember = "GOV_MOT_ID"
            Me.ParentForm.TransparencyKey = System.Drawing.Color.FromArgb(121, 121, 121)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub txtVoterId_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtVoterId.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtFamNo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtFamNo.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtVRC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtVRC.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtYearOB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtYearOB.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If

        If Microsoft.VisualBasic.Asc(e.KeyChar) < 48 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 57 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtName.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtSecondName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSecondName.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtThirdName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtThirdName.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Asc(e.KeyChar) < 190 Or Microsoft.VisualBasic.Asc(e.KeyChar) > 237 Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub btnSelect_Click(sender As Object, e As EventArgs) Handles btnSelect.Click

        CHeKBOX("EDIT")
    End Sub

    Private Sub btnPDF_Click(sender As Object, e As EventArgs) Handles btnPDF.Click
        CHeKBOX("PDF")
    End Sub

End Class